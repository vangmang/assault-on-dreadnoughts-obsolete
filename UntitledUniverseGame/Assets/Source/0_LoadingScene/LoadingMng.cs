﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public sealed class LoadingMng : MonoBehaviour
{
    public enum SCENE
    {
        logo,
        loading,
        mainMenu,
        mainGame
    }

    [SerializeField]
    private SavedData savedData;
    public CanvasGroup LoadingDisplayer;
    public Transform LoadingImageTransform;
    public Transform LoadingImageTransform2;
    public GameObject LoadingObject;
    private static LoadingMng instance = null;

    public static LoadingMng Instance
    {
        get { return instance; }
    }

    public int n_TargetScene;
    public string s_TargetScene; 
    
    private void Awake()
    {
        instance = this;
        s_TargetScene = "";
        n_TargetScene = (int)SCENE.mainMenu; // default
        DontDestroyOnLoad(this);
    }

    public void LoadScene(int index)
    {
        LoadingObject.SetActive(true);
        IEnumerator scene = loadSceneInvoke(index);
        StartCoroutine(scene);   
    }

    public void LoadScene(string name)
    {
        LoadingObject.SetActive(true);
        IEnumerator scene = loadSceneInvoke(name);
        StartCoroutine(scene);
    }

    private IEnumerator loadSceneInvoke(int index)
    {
        //yield return new WaitUntil(() => savedData.dataLoadingComplete);
        AsyncOperation async = SceneManager.LoadSceneAsync(index);
        async.allowSceneActivation = false;
        float t = 0f;
        LoadingDisplayer.alpha = 1f;
        while (t < 0.9f)
        {
            t = async.progress;
            LoadingImageTransform.Rotate(Vector3.forward, 20f * Time.deltaTime);
            LoadingImageTransform2.Rotate(Vector3.forward, -10f * Time.deltaTime);
            yield return null;
        }
        //Debug.Log("vangmang");
        t = 1f;
        while(t >= 0f)
        {
            t -= Time.deltaTime;
            LoadingImageTransform.Rotate(Vector3.forward, 20f * Time.deltaTime);
            LoadingImageTransform2.Rotate(Vector3.forward, -10f * Time.deltaTime);
            LoadingDisplayer.alpha = t;
            yield return null;
        }
        LoadingDisplayer.alpha = 0f;
        yield return new WaitForSeconds(0.1f);
        LoadingObject.SetActive(false);
        async.allowSceneActivation = true;
    }

    private IEnumerator loadSceneInvoke(string name)
    {
        //yield return new WaitUntil(() => savedData.dataLoadingComplete);
        AsyncOperation async = SceneManager.LoadSceneAsync(name);
        async.allowSceneActivation = false;
        float t = 0f;
        LoadingDisplayer.alpha = 1f;
        while (t < 0.9f)
        {
            t = async.progress;
            LoadingImageTransform.Rotate(Vector3.forward, 20f * Time.deltaTime);
            yield return null;
        }
        t = 1f;
        while (t >= 0f)
        {
            t -= Time.deltaTime;
            LoadingImageTransform.Rotate(Vector3.forward, 20f * Time.deltaTime);
            LoadingDisplayer.alpha = t;
            yield return null;
        }
        LoadingDisplayer.alpha = 0f;
        yield return new WaitForSeconds(0.1f);
        LoadingObject.SetActive(false);
        async.allowSceneActivation = true;
    }

    private void OnDestroy()
    {
        Destroy(this);
    }
}
