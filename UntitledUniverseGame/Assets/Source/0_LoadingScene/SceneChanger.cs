﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(LoadingMng.Instance.s_TargetScene.Equals(""))
            LoadingMng.Instance.LoadScene(LoadingMng.Instance.n_TargetScene);   
        else
            LoadingMng.Instance.LoadScene(LoadingMng.Instance.s_TargetScene);
    }
}
