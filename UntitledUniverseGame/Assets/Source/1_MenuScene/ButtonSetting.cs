﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSetting : MonoBehaviour
{
    public Color pressedColor;
    public Color enterColor;
    public Color exitColor;

    public AudioSource enterAudio;
    public AudioSource pressedAudio;
}
