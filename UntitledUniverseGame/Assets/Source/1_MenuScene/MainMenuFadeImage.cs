﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuFadeImage : MonoBehaviour
{
    public Image image;
    public GraphicRaycaster UIgraphicRaycaster;
    public CanvasGroup menuGroup;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(playImageFade());
    }

    private IEnumerator playImageFade()
    {
        float t = 1f;
        var color = image.color;
        while (t > 0f)
        {
            t -= Time.deltaTime;
            color.a = t;
            image.color = color;
            yield return null;
        }
        color.a = 0f;
        image.color = color;
        t = 0f;
        while(t < 1f)
        {
            t += 1.5f * Time.deltaTime;
            menuGroup.alpha = t;
            yield return null;
        }
        menuGroup.alpha = 1f;
        UIgraphicRaycaster.enabled = true;
    }

    public void SetImageAlpha1F()
    {
        UIgraphicRaycaster.enabled = false;
        var color = image.color;
        color.a = 1f;
        image.color = color;
    }

}
