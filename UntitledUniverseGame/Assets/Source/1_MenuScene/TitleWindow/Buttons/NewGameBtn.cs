﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class NewGameBtn : ButtonManager
{
    public MainMenuFadeImage fadeImage;

    public override void OnPointerDown(PointerEventData e)
    {
        PlayPressButtonSound();
        PlayPressButtonSound();ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
        StartCoroutine(invokeLoadScene());
    }
    private IEnumerator invokeLoadScene()
    {
        fadeImage.SetImageAlpha1F();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene((int)LoadingMng.SCENE.loading);
        LoadingMng.Instance.n_TargetScene = (int)LoadingMng.SCENE.mainGame;
    }
}
