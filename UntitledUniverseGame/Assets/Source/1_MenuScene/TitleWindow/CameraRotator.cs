﻿using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    [SerializeField] private float rotSpeed;
    private void Update()
    {
        transform.Rotate(Vector3.up, rotSpeed * Time.deltaTime);
    }
}
