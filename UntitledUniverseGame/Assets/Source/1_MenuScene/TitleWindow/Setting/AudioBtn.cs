﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AudioBtn : MainMenuButtonManager
{
    public override void OnPointerDown(PointerEventData e)
    {
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
        UI_Manager.currentWindow.gameObject.SetActive(false);
        UI_Manager.recentWindows.Push(UI_Manager.currentWindow);
        UI_Manager.currentWindow = UI_Manager.audioWindow;
        UI_Manager.currentWindow.gameObject.SetActive(true);
    }
}
