﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject titleWindow;
    public GameObject settingWindow;
    public GameObject gameplayWindow;
    public GameObject graphicWindow;
    public GameObject audioWindow;

    public GameObject currentWindow;
    public Stack<GameObject> recentWindows = new Stack<GameObject>();
}

