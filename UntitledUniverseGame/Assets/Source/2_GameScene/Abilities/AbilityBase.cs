﻿using UnityEngine;
using FiniteStateMachine;
using System;

public abstract class AbilityBase : StateMng
{
    public ShipBase shipBase;
    public bool isCooldownExecuted;
    /// <summary>
    /// 스테이션에서 구매가능한 가격
    /// </summary>
    [Serializable]
    public struct _COST_
    {
        public int bounty;
        public int dreadnoughtSerialChip;
    }
    public _COST_ Cost;
    public int usableCount;
    protected abstract void Init();
    public interface IAciveFormAbility
    {
        bool Activation { get; }
        void EnableActivationForm(params object[] o_Params);
    }

    public interface ILauncherAbility
    {
        float Cooldown { get; }
        void Launch(params object[] o_Params);
        void AffectTarget(params object[] o_Params);
    }
}

public abstract class ActiveFormAbility : AbilityBase, AbilityBase.IAciveFormAbility
{
    public bool Activation { get; protected set; }
    public abstract void EnableActivationForm(params object[] o_Params);
}

public abstract class LauncherFormAbility : AbilityBase, AbilityBase.ILauncherAbility
{
    public enum LAUNCHER_TYPE
    {
        gravityTracingMissile,
        kineticExplosion
    }
    public abstract LAUNCHER_TYPE LauncherType { get; }
    public bool LaunchAble { get; protected set; }
    public Action<float> CooldownInvoker;

    public DateTimeOffset lastLaunched;
    public float Cooldown { get; protected set; }
    public abstract void Launch(params object[] o_Params);
    public abstract void AffectTarget(params object[] o_Params);
}
