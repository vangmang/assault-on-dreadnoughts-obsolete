﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GravityTracingMissileChaseState : State<GravityTracingMissileStateMng>
{
    public override Enum GetState
    {
        get { return GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.chase; }
    }

    private float currentVelocity;
    private TargetingModuleStateMng.ITargetable currentTarget;
    private float time;

    public override void Enter(params object[] o_Params)
    {
        currentVelocity = 0f;
        time = 0f;
        currentTarget = instance.launcherStateMng.currentTarget;
        instance.missileTransform.rotation = instance.launcherStateMng.shipBase.Info.ShipTransform.rotation;
        instance.missileTransform.position = instance.launcherStateMng.shipBase.Info.ShipTransform.position - instance.launcherStateMng.shipBase.Info.ShipTransform.up * 30f;
        instance.missileCollider.gameObject.SetActive(true);
        instance.missileTrail.gameObject.SetActive(true);
        StartCoroutine(enableCollider());
    }

    private IEnumerator enableCollider()
    {
        yield return new WaitForSeconds(0.5f);
        instance.missileCollider.enabled = true;
    }

    public override void Execute()
    {
        if (currentTarget == null)
            return;
        try
        {
            var targetRot = Quaternion.LookRotation(currentTarget.TargetableTransform.position - instance.missileTransform.position, instance.missileTransform.up);
            instance.missileTransform.rotation = Quaternion.Slerp(instance.missileTransform.rotation, targetRot, instance.CurrentInfo.mobility * Time.deltaTime);
            currentVelocity = Mathf.Lerp(currentVelocity, instance.CurrentInfo.velocity, instance.CurrentInfo.acceleration * Time.deltaTime);
            instance.missileTransform.position += instance.missileTransform.forward * currentVelocity * Time.deltaTime;
        }
        catch
        {
            instance.StateTransition(GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.idle);
            return;
        }

        time += Time.deltaTime;
        var distance = (instance.missileTransform.position - currentTarget.TargetableTransform.position).magnitude;
        if(distance <= 100f)
        {
            instance.launcherStateMng.AffectTarget(currentTarget);
            instance.StateTransition(GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.hit);
            return;
        }
        else if(time >= instance.launcherStateMng.flightTime)
        {
            instance.StateTransition(GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.idle);
            return;
        }
    }

    public override void Exit()
    {
    }
}
