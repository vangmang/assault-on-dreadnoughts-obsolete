﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityTracingMissileCollision : MonoBehaviour
{
    public GravityTracingMissileStateMng gravityTracingMissileStateMng;

    public void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.tag.Equals("Solid"))
        {
            object[] arr =
            {
                gravityTracingMissileStateMng.CurrentInfo.damage,
                gravityTracingMissileStateMng.launcherStateMng.shipBase
            };
            collision.gameObject.SendMessage("TakeDamage", arr, SendMessageOptions.DontRequireReceiver);
        }
        gravityTracingMissileStateMng.StateTransition(GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.hit);
    }
}
