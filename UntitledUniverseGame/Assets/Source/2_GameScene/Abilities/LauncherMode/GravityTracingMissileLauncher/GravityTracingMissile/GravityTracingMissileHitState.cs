﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GravityTracingMissileHitState : State<GravityTracingMissileStateMng>
{
    public override Enum GetState
    {
        get { return GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.hit; }
    }
    public override void Enter(params object[] o_Params)
    {
        instance.missileCollider.gameObject.SetActive(false);
        try
        {
            instance.missileExplosionParticle.Play();
            instance.missileExplosionAudio.Play();
        }
        catch { }
        StartCoroutine(invokeStateTransition());
    }

    private IEnumerator invokeStateTransition()
    {
        yield return new WaitForSeconds(instance.missileTrail.time);
        instance.StateTransition(GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
