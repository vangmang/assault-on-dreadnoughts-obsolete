﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GravityTracingMissileIdleState : State<GravityTracingMissileStateMng>
{
    public override Enum GetState
    {
        get { return GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.idle; }
    }
    public override void Enter(params object[] o_Params)
    {
        instance.missileCollider.enabled = false;
        instance.missileCollider.gameObject.SetActive(false);
        StartCoroutine(waitForTrail());
    }

    private IEnumerator waitForTrail()
    {
        yield return new WaitForSeconds(instance.missileTrail.time);
        instance.missileTrail.gameObject.SetActive(false);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
