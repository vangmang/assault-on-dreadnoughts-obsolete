﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;

public class GravityTracingMissileStateMng : StateMng
{
    public enum GRAVITY_TRACING_MISSILE_STATES
    {
        idle,
        chase,
        hit
    }

    public GravityTracingMissileLauncherStateMng launcherStateMng;
    public Collider missileCollider;
    public TrailRenderer missileTrail;
    public ParticleSystem missileExplosionParticle;
    public AudioSource missileExplosionAudio;
    public Transform missileTransform;

    public struct _CURRENT_INFO_
    {
        public float damage;
        public float velocity;
        public float acceleration;
        public float mobility;
    }
    public _CURRENT_INFO_ CurrentInfo;

    private void Awake()
    {
        InitializeState();
    }

    // Start is called before the first frame update
    void Start()
    {
        missileTrail.gameObject.SetActive(false);
        StateEnter(GRAVITY_TRACING_MISSILE_STATES.idle);
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
    }

    private void OnDestroy()
    {
        
    }
}
