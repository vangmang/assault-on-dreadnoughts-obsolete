﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GravityTracingMissileLauncherIdleState : State<GravityTracingMissileLauncherStateMng>
{
    public override Enum GetState
    {
        get { return GravityTracingMissileLauncherStateMng.GRAVITY_TRACING_MISSILE_LAUNCHER_STATES.idle; }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
