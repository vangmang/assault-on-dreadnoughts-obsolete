﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GravityTracingMissileLauncherLaunchState : State<GravityTracingMissileLauncherStateMng>
{
    public override Enum GetState
    {
        get { return GravityTracingMissileLauncherStateMng.GRAVITY_TRACING_MISSILE_LAUNCHER_STATES.launch; }
    }

    public override void Enter(params object[] o_Params)
    {
        try
        {
            instance.launchingAudio.transform.position = instance.shipBase.Info.ShipTransform.position;
            instance.launchingAudio.Play();
        }
        catch { }
        instance.currentCharge = --instance.currentCharge % instance.MaxCharge;
        int index = -instance.currentCharge;
        var missile = instance.GravityTracingMissileStateMngs[index];
        missile.CurrentInfo.velocity = instance.Velocity;
        missile.CurrentInfo.acceleration = instance.Acceleration;
        missile.CurrentInfo.mobility = instance.Mobility;
        missile.CurrentInfo.damage = instance.Damage;
        missile.StateTransition(GravityTracingMissileStateMng.GRAVITY_TRACING_MISSILE_STATES.chase);
        instance.StateTransition(GravityTracingMissileLauncherStateMng.GRAVITY_TRACING_MISSILE_LAUNCHER_STATES.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
