﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class GravityTracingMissileLauncherStateMng : LauncherFormAbility
{
    public enum GRAVITY_TRACING_MISSILE_LAUNCHER_STATES
    {
        idle,
        launch
    }

    public override LAUNCHER_TYPE LauncherType => LAUNCHER_TYPE.gravityTracingMissile;


    [SerializeField] protected float cooldown;
    [SerializeField] private float damage;
    [SerializeField] private float velocity;
    [SerializeField] private float acceleration;
    [SerializeField] private float mobility;
    [SerializeField] private GravityTracingMissileStateMng[] gravityTracingMissiles;

    public GravityTracingMissileStateMng[] GravityTracingMissileStateMngs { get { return gravityTracingMissiles; } }
    public TargetingModuleStateMng.ITargetable currentTarget;
    public AudioSource launchingAudio;

    public float flightTime;
    public float Damage { get; private set; }
    public float Velocity { get; private set; }
    public float Acceleration { get; private set; }
    public float Mobility { get; private set; }
    public int MaxCharge { get { return gravityTracingMissiles.Length - 1; } }
    public int currentCharge;

    // Start is called before the first frame update
    void Awake()
    {
        Init();
        InitializeState();
    }

    private void Start()
    {
        this.ObserveEveryValueChanged(_ => shipBase.Info.TargetingModuleStateMng.isOptimalZoneAvailable).
            Subscribe(_ =>
            {
                LaunchAble = shipBase.Info.TargetingModuleStateMng.isOptimalZoneAvailable;
            });
        StateEnter(GRAVITY_TRACING_MISSILE_LAUNCHER_STATES.idle);
    }

    protected override void Init()
    {
        Cooldown = cooldown;
        Damage = damage;
        Velocity = velocity;
        Acceleration = acceleration;
        Mobility = mobility;
    }

    public override void Launch(params object[] o_Params)
    {
        usableCount--;
        CooldownInvoker(cooldown);
        currentTarget = shipBase.Info.TargetingModuleStateMng.currentTarget;
        StateTransition(GRAVITY_TRACING_MISSILE_LAUNCHER_STATES.launch);
    }

    public override void AffectTarget(params object[] o_Params)
    {
        TargetingModuleStateMng.ITargetable target = o_Params[0] as TargetingModuleStateMng.ITargetable;
        object[] arr = {
            damage,
            shipBase
        };
        target.TargetableTransform.gameObject.SendMessage("TakeDamage", arr, SendMessageOptions.DontRequireReceiver);
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
    }

    private void OnDestroy()
    {
    }
}
