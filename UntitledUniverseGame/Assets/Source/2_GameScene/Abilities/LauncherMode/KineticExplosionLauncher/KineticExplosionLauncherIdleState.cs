﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class KineticExplosionLauncherIdleState : State<KineticExplosionLauncherStateMng>
{
    public override Enum GetState => KineticExplosionLauncherStateMng.KINETIC_EXPLOSION_LAUNCHER_STATES.idle;

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
