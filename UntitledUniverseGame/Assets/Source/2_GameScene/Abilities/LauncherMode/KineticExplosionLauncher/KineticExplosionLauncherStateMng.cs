﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UniRx;

public class KineticExplosionLauncherStateMng : LauncherFormAbility
{
    public enum KINETIC_EXPLOSION_LAUNCHER_STATES
    {
        idle,
        launch
    }

    public override LAUNCHER_TYPE LauncherType => LAUNCHER_TYPE.kineticExplosion;
    [SerializeField] protected float cooldown;
    [SerializeField] private float affectableDistance;
    private IEnumerable<TargetingModuleStateMng.ITargetable> enemies;
    private void Awake()
    {
        Init();
        InitializeState();
    }

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => shipBase.Info.TargetingModuleStateMng.EnemyList.Count).
            Subscribe(count =>
            {
                LaunchAble = count > 0;
            });
        StateEnter(KINETIC_EXPLOSION_LAUNCHER_STATES.idle);
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
    }

    protected override void Init()
    {
        Cooldown = cooldown;
    }

    public override void AffectTarget(params object[] o_Params)
    {
        foreach (var ship in enemies)
        {
            if (ship.Identify.Equals(ShipBase.IDENTIFY.ally))
                return;
            var dir = (ship.TargetableTransform.position - shipBase.Info.ShipTransform.position).normalized;
            var distance = (shipBase.Info.ShipTransform.position - ship.TargetableTransform.position).magnitude;
            var affectDistance = 1500 * (distance / affectableDistance);
            var target = ship as ShipBase;
            target.Info.ShipRigidbody.AddForce(dir * affectDistance, ForceMode.Impulse);
        }
    }

    public override void Launch(params object[] o_Params)
    {
        enemies = shipBase.Info.TargetingModuleStateMng.EnemyList.
                Where(ship => ship.TargetType.Equals(TargetingModuleStateMng.TARGET_TYPE.ship)).
                Where(ship =>
                {
                    var distance = (shipBase.Info.ShipTransform.position - ship.TargetableTransform.position).magnitude;
                    return distance <= affectableDistance;
                });
        if (enemies.Count() == 0)
            return;
        CooldownInvoker(cooldown);
        usableCount--;
        StateTransition(KINETIC_EXPLOSION_LAUNCHER_STATES.launch);
    }
}
