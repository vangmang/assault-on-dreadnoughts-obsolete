﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycastChecker : MonoBehaviour
{
    public MouseFollowingCamera followingCamera;


    // Update is called once per frame
    void Update()
    {
        transform.position = followingCamera.WantedPos;
        var r = followingCamera.MaxRadian;
        var dist = followingCamera.MaxDistance;
        var h = followingCamera.MaxHeight;
        RaycastHit hit;
        if (Physics.Raycast(followingCamera.target.position, (transform.position - followingCamera.target.position).normalized, out hit, r))
        {
            if (!hit.collider.gameObject.tag.Equals("Player"))
            {
                r = (followingCamera.target.position - hit.point).magnitude;
                dist *= (r / followingCamera.MaxRadian);
                h *= (r / followingCamera.MaxRadian);
            }
        }
        followingCamera.Radian = r + followingCamera.additionalRadian;
        followingCamera.Distance = dist;
        followingCamera.Height = h;
    }
}
