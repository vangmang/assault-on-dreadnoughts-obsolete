﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class CameraShaker : MonoBehaviour
{
    public float shakes = 0f;
    public float shakeAmount = 0.5f;
    public float decreaseFactor = 1f;

    private Vector3 originPos;
    public bool isCameraShaking;

    private void Awake()
    {
        isCameraShaking = false;
        originPos = transform.position;
    }

    public void ExecuteCameraShaking(Transform target)
    {
        if (isCameraShaking)
        {
            if (shakes > 0)
            {
                var randomPos = Random.insideUnitSphere * shakeAmount;
                target.position += randomPos;
                shakes -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                shakes = 0f;
                target.position += originPos;
                isCameraShaking = false;
            }
        }
    }

    public void ShakeCamera(float shake)
    {
        if (!isCameraShaking)
        {
            originPos = gameObject.transform.position;
        }
        shakes = shake;
        isCameraShaking = true;
    }
}
