﻿using UnityEngine;
using UniRx;

public class CurrentPlanetMapDisplayer : MonoBehaviour
{
    public PlanetInfo planetInfo;
    public GameStateMng gameStateMng;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => gameStateMng.GetCurrentState).
            Subscribe(_ =>
            {
                gameObject.SetActive(planetInfo.planet.Equals(CharStateMng.Instance.CurrentInfo.cur_Stage.planet) &&
                    gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pauseAndShowMap));
            });
    }
}
