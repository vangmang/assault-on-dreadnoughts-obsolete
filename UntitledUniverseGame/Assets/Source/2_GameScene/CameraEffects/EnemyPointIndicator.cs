﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyPointIndicator : Indicator
{
    public Indicator targetIndicator;
    //public bool rotatable;
    public Image SubImage;
    [SerializeField] private Animation targetingAlertAnim;
    public Animation TargetingAlertAnim => targetingAlertAnim;

    // Start is called before the first frame update
    void Start()
    {
        base.initIndicator();
    }

    protected override void displayIndicator()
    {
        Vector3 screenPoint = mainCamera.WorldToScreenPoint(target.position) - halfScreenSize;

        screenPoint.x = Mathf.Clamp(screenPoint.x, -borderScreenSize.x, borderScreenSize.x);
        screenPoint.y = Mathf.Clamp(screenPoint.y, -borderScreenSize.y, borderScreenSize.y);
        screenPoint.z = 0f;

        //var modifiedPosition = transform.right * modifiedVector.x + transform.up * modifiedVector.y;
        transform.position = screenPoint;// + modifiedPosition;

        var pos = targetIndicator.transform.position - transform.position;
        var angle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg - 90f;
        transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }
}
