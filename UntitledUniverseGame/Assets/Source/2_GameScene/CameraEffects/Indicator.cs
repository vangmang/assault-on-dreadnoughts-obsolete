﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Indicator : MonoBehaviour
{
    public Camera mainCamera; 
    public Transform target;
    public Image normalImage;
    public Image outOfImage;
    public List<Text> textList;

    protected Vector3 halfScreenSize;
    protected Vector3 borderScreenSize;
    protected new Renderer renderer;
    [SerializeField] protected bool DisableOnStart;
    public Renderer OriginRenderer { get; protected set; }
    protected float originalNormalImageAlpha;

    // Start is called before the first frame update
    void Start()
    {
        initIndicator();
    }
    void LateUpdate()
    {
        displayIndicator();
    }

    protected virtual void initIndicator()
    {
        if(normalImage)
            originalNormalImageAlpha = normalImage.color.a;
        gameObject.SetActive(!DisableOnStart);
        renderer = target.gameObject.GetComponentInChildren<Renderer>();
        OriginRenderer = renderer;
        halfScreenSize = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f);
        borderScreenSize = halfScreenSize - halfScreenSize * 0.02f;
    }
    // Update is called once per frame

    protected virtual void displayIndicator()
    {
        Vector3 screenPoint = mainCamera.WorldToScreenPoint(target.position) - halfScreenSize;
        bool checkXpos = Mathf.Abs(screenPoint.x) > borderScreenSize.x;
        bool checkYpos = Mathf.Abs(screenPoint.y) > borderScreenSize.y;
        bool checkPos = checkXpos || checkYpos;

        screenPoint.x = Mathf.Clamp(screenPoint.x, -borderScreenSize.x, borderScreenSize.x);
        screenPoint.y = Mathf.Clamp(screenPoint.y, -borderScreenSize.y, borderScreenSize.y);
        screenPoint.z = 0f;

        if (!checkPos && !renderer.isVisible) // 스크린 화면을 넘어갈 경우
        {
            // 스크린 화면을 넘어갔는데 오브젝트가 비치지 않는 경우
            // 2020.01.27 참 골때리는 상황 발생. 
            // WorldToScreenPoint가 좀 골때린다. 오브젝트가 카메라에 비치지 않는상태임에도 불구하고
            // 카메라를 회전시키면 카메라 뷰포트 기준 오브젝트의 역방향에 포지션이 똑같이 잡힌다.
            // 무슨 스텐실이냐 ㅅㅂ;
            // 이브온라인 offscreen 타겟 인디케이터 참고해서 구현했다.
            if (Mathf.Abs(screenPoint.x) < borderScreenSize.x)
                screenPoint.x = screenPoint.x > 0 ? borderScreenSize.x : -borderScreenSize.x;
            if (Mathf.Abs(screenPoint.y) > borderScreenSize.y)
                screenPoint.y = screenPoint.y > 0 ? borderScreenSize.y : -borderScreenSize.y;
            checkPos = true;
        }
        if (outOfImage)
        {
            Vector3 dir = transform.localPosition.normalized;
            float rot = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90f;
            outOfImage.transform.localRotation = Quaternion.Euler(0f, 0f, rot);
            var color = outOfImage.color;
            color.a = checkPos ? 1f : 0f;
            outOfImage.color = color; //(checkPos) ? new Color(1f, 1f, 1f, 1f) : new Color(1f, 1f, 1f, 0f);// ;
        }
        transform.localPosition = screenPoint;
        if (normalImage)
        {
            var color = normalImage.color;
            color.a = checkPos ? 0f : originalNormalImageAlpha;
            normalImage.color = color;//(checkPos) ? new Color(1f, 1f, 1f, 0f) : new Color(1f, 1f, 1f, 1f); //.gameObject.SetActive(!checkPos);
        }
    }
}
