﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Text;

public class JumpPortalIndicator : Indicator
{
    [SerializeField] private Color idleColor;
    [SerializeField] private Color selectColor;

    private bool onMousePoint;
    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => mainCamera.transform.rotation).
            Where(_ => normalImage.transform.position.magnitude <= 64f).
            Subscribe(_ =>
            {
                if (!onMousePoint)
                {
                    onMousePoint = true;
                    StartCoroutine(playNameTextEffect());
                    StartCoroutine(playImageEffects());
                }
            });
        Observable.EveryUpdate(). 
            Where(_ => Input.GetMouseButtonDown(0)).
            Where(_ => onMousePoint).
            Subscribe(_ =>
            {
                SavedData.SavedDataContainer.Level.currentLevel++;
                GameMng.Instance.LoadScene(LoadingMng.SCENE.mainGame);   
            }).AddTo(this);
        base.initIndicator();
    }

    private IEnumerator playNameTextEffect()
    {
        var name = "Jump To Next System";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < name.Length && normalImage.transform.position.magnitude <= 64; i++)
        {
            sb.Append(name[i]);
            textList[0].text = sb.ToString();
            yield return new WaitForSeconds(0.02f);
        }
        yield return new WaitUntil(() => !onMousePoint);
        textList[0].text = "";
    }

    private IEnumerator playImageEffects()
    {
        normalImage.color = selectColor;
        normalImage.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        yield return new WaitWhile(() => normalImage.transform.position.magnitude <= 64f);
        normalImage.color = idleColor;
        normalImage.transform.localScale = Vector3.one;
        onMousePoint = false;
    }
}
