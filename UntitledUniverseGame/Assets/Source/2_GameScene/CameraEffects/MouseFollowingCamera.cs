﻿using System;
using UnityEngine;
using UniRx;

[RequireComponent(typeof(Camera))]
public class MouseFollowingCamera : MonoBehaviour
{
    [SerializeField]private GameMng gameMng;
    public Transform target;
    [SerializeField] private float radian;
    [SerializeField] private float sensitivity;
    [SerializeField] private float followingSpeed;
    [SerializeField] private float rotatingSpeed;
    [SerializeField] private float height;
    [SerializeField] private float distance;
    [SerializeField] private bool checkFollowTarget;
    [SerializeField] private bool x_reverse;
    [SerializeField] private bool y_reverse;
    [SerializeField] private CameraShaker cameraShaker;
    public bool interpolation;

    public Vector3 WantedPos { get; private set; }
    private float rotX;
    private float rotY;
    private Quaternion localRotation;
    public float Radian { get { return radian; } set { radian = value; } }
    public float Distance { get { return distance; } set { distance = value; } }
    public float Height { get { return height; } set { height = value; } }
    public float MaxRadian { get; private set; }
    public float MaxDistance { get; private set; }
    public float MaxHeight { get; private set; }
    public float Sensitivity { get { return sensitivity; } }
    public bool Xreverse { get { return x_reverse; } }
    public bool Yreverse { get { return y_reverse; } }
    [NonSerialized] public float additionalRadian;
    private float originRadian;
    private float radianDecreaseFactor = 10f;

    // Start is called before the first frame update
    void Awake()
    {        
        MaxRadian = radian;
        MaxDistance = distance;
        MaxHeight = height;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        FollowMouse();
        enabled = false;
    }

    private void Start()
    {
        this.ObserveEveryValueChanged(_ => gameMng.PauseInfo.GamePlaySetting.mouseSensitivity.value).
            Subscribe(_ =>
            {
                sensitivity = gameMng.PauseInfo.GamePlaySetting.mouseSensitivity.value * 100f;
            });
    }

    // Update is called once per frame
    void Update()
    {
        FollowMouse();
        additionalRadian = Mathf.Lerp(additionalRadian, 0f, radianDecreaseFactor * Time.deltaTime);
    }

    public void FollowMouse()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * sensitivity * Time.deltaTime;
        rotX += mouseY * sensitivity * Time.deltaTime;
        rotX = Mathf.Clamp(rotX, -90f, 90f);
        localRotation = Quaternion.Euler(!x_reverse ? rotX : -rotX, !y_reverse ? rotY : -rotY, 0f);

        transform.rotation = Quaternion.Slerp(transform.rotation, localRotation, rotatingSpeed * Time.deltaTime);
        if (checkFollowTarget)
        {
            var target = this.target.position + transform.forward * -radian + transform.up * height + transform.forward * distance;
            WantedPos = this.target.position + transform.forward * -MaxRadian + transform.up * MaxHeight + transform.forward * MaxDistance;
            transform.position = interpolation ? Vector3.Lerp(transform.position, target, followingSpeed * Time.deltaTime) : target;
        }
        if (cameraShaker)
            cameraShaker.ExecuteCameraShaking(transform);
    }
}
