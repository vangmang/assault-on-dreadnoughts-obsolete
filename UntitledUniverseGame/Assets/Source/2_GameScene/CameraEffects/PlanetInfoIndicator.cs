﻿using UnityEngine.EventSystems;
using System.Text;

public class PlanetInfoIndicator : Indicator, IPointerClickHandler
{
    public StarSystemDisplayer starSystemDisplayer;
    public PlanetInfo planetInfo;
    private string planetNameText;
    private string warpPointsText;

    // Start is called before the first frame update
    void Start()
    {
        base.initIndicator();
        planetNameText = planetInfo.planet.placetNameText.text;

        warpPointsText = getWarpPointsText();
    }

    public void OnPointerClick(PointerEventData e)
    {
        starSystemDisplayer.planetNameText.text = planetNameText;
        starSystemDisplayer.warpPointsListText.text = getWarpPointsText();
    }

    private string getWarpPointsText()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < planetInfo.planet.planetInfo.possessedWarpPoints.Count; i++)
        {
            var warpPoint = planetInfo.planet.planetInfo.possessedWarpPoints[i];
            sb.Append((i + 1).ToString()).Append(") ").Append(warpPoint.placetNameText.text).Append("\n").Append("탐사 여부: ").
                Append(warpPoint.explorationCheck ? "탐사 완료" : "미확인");
            if (warpPoint.currentPointCheck)
                sb.Append(" <현재 위치>");
            sb.Append("\n");
        }
        return sb.ToString();
    }
}
