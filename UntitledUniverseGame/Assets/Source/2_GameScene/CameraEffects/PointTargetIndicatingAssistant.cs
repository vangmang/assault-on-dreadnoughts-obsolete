﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using System.Text;

public class PointTargetIndicatingAssistant : MonoBehaviour
{
    [SerializeField] private WarpTargetIndicator indicator;
    [SerializeField] private Text distanceText;
    //public WarpPoint getWarpPoint { get; private set; }
    private CharStateMng charStateMng;
    private WarpPoint warpPoint;
    private IEnumerator Start()
    {
        yield return new WaitUntil(() => indicator.Assistant != null);
        this.ObserveEveryValueChanged(_ => CharStateMng.Instance.CurrentInfo.cur_Stage).
            Subscribe(_ =>
            {
                //Debug.Log(CharStateMng.Instance.CurrentInfo.cur_Stage);
                //Debug.Log(CharStateMng.Instance.CurrentInfo.cur_Stage + " " + ((WarpPoint)indicator.targetPlace).currentPlanet.stage);
                indicator.displayCheck = CharStateMng.Instance.CurrentInfo.cur_Stage.Equals(((WarpPoint)indicator.targetPlace).currentPlanet.stage);
                // 오브젝트 자체를 껐다 켰다 하니까 각 인디케이터의 start에서 초기화가 되기 전 비활성화 되어버려서 이상하게 작동하게 된다.
                // 워프 타겟 인디케이터는 특별히 따로 디스플레이 체크라는 불리언 변수를 만들어서 해결했다.
                //indicator.gameObject.SetActive(enable);
            });
        warpPoint = (WarpPoint)indicator.Assistant;
        charStateMng = CharStateMng.Instance;
        yield return new WaitUntil(() => charStateMng.CurrentInfo.cur_WarpPoint);
        Observable.EveryUpdate().Subscribe(_ => { Display(); }).AddTo(this);
    }

    private void Display()
    {
        var unit = (charStateMng.CurrentInfo.cur_WarpPoint.Equals(warpPoint));
        var target = warpPoint.transform.position;
        var dist = (charStateMng.Info.ShipTransform.position - target).magnitude;
        if (!unit)
            dist = (charStateMng.Info.ShipTransform.position - target * 100f).magnitude / 512f;

        int intDist = (int)dist;
        //StringBuilder sb = new StringBuilder(intDist.ToString());
        var sb = intDist.ToString() + (unit ? "m" : "km");
        //sb.Append(unit ? "m" : "km");
        distanceText.text = charStateMng.CurrentInfo.cur_Stage.planet.Equals(((WarpPoint)indicator.targetPlace).currentPlanet) ?
                            sb : "";

    }
}
