﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UniRx;

public class StationIndicator : Indicator
{
    [SerializeField] private Color idleColor;
    [SerializeField] private Color selectColor;
    public Station station;
    private bool onMousePoint;
    private bool checkPoint;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => mainCamera.transform.rotation).
            Where(_ => checkPoint).
            Where(_ => normalImage.transform.position.magnitude <= 32f).
                Subscribe(_ =>
                {
                    if (!onMousePoint)
                    {
                        onMousePoint = true;
                        StartCoroutine(playImageEffects());
                    }
                });
        this.ObserveEveryValueChanged(_ => onMousePoint).
            Subscribe(_ =>
            {
                textList[0].gameObject.SetActive(onMousePoint && checkPoint);
            });
        this.ObserveEveryValueChanged(_ => CharStateMng.Instance.CurrentInfo.cur_WarpPoint).
            Subscribe(_ =>
            {
                checkPoint = station.currentWarpPoint.Equals(CharStateMng.Instance.CurrentInfo.cur_WarpPoint);
            });
        Observable.EveryUpdate().
            Where(_ => !CharStateMng.Instance.CurrentInfo.isWarpin).
            Where(_ => !station.gameMng.gameStateMng.isShowingInfoState).
            Where(_ => Input.GetKeyDown(KeyCode.G)).
            Where(_ => onMousePoint).
            Subscribe(_ =>
            {
                station.gameMng.gameStateMng.StateTransition(GameStateMng.GAME_STATES.pauseAndShowStation);
            }).AddTo(this);
        base.initIndicator();
    }

    private IEnumerator playImageEffects()
    {
        normalImage.color = selectColor;
        normalImage.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        yield return new WaitWhile(() => normalImage.transform.position.magnitude <= 32f);
        normalImage.color = idleColor;
        normalImage.transform.localScale = Vector3.one;
        onMousePoint = false;
        CharStateMng.Instance.Warp.currentWarpTarget = null;
    }

    protected override void displayIndicator()
    {
        Vector3 screenPoint = mainCamera.WorldToScreenPoint(target.position) - halfScreenSize;
        bool checkXpos = Mathf.Abs(screenPoint.x) > borderScreenSize.x;
        bool checkYpos = Mathf.Abs(screenPoint.y) > borderScreenSize.y;
        bool checkPos = checkXpos || checkYpos;

        screenPoint.x = Mathf.Clamp(screenPoint.x, -borderScreenSize.x, borderScreenSize.x);
        screenPoint.y = Mathf.Clamp(screenPoint.y, -borderScreenSize.y, borderScreenSize.y);
        screenPoint.z = 0f;

        if (!checkPos && !renderer.isVisible) // 스크린 화면을 넘어갈 경우
        {
            if (Mathf.Abs(screenPoint.x) < borderScreenSize.x)
                screenPoint.x = screenPoint.x > 0 ? borderScreenSize.x : -borderScreenSize.x;
            if (Mathf.Abs(screenPoint.y) > borderScreenSize.y)
                screenPoint.y = screenPoint.y > 0 ? borderScreenSize.y : -borderScreenSize.y;
            checkPos = true;
        }
        transform.localPosition = screenPoint;
        if (normalImage)
            normalImage.gameObject.SetActive(!checkPos && this.checkPoint);
    }
}