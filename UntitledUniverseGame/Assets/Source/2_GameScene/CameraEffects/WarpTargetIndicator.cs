﻿using System.Collections;
using UnityEngine;
using UniRx;
using System.Text;

public class WarpTargetIndicator : Indicator
{
    [SerializeField] private Color idleColor;
    [SerializeField] private Color selectColor;
    [System.NonSerialized] public bool displayCheck = true;
    private bool onMousePoint;
    public WarpablePlace targetPlace;
    public Renderer SetRenderer { set { renderer = value; } }
    public object Assistant;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => mainCamera.transform.rotation).
            Where(_ => normalImage.transform.position.magnitude <= 32f).
            Subscribe(_ =>
            {
                if (!onMousePoint)
                {
                    CharStateMng.Instance.Warp.currentWarpTarget = targetPlace;
                    onMousePoint = true;
                    StartCoroutine(playImageEffects());
                    StartCoroutine(playTextEffect());
                    StartCoroutine(playDistanceTextEffect());
                }
            });
        this.ObserveEveryValueChanged(_ => GameMng.Instance.enableJumpPortalGeneration).
            Where(x => x).
            Subscribe(_ =>
            {
                try
                {
                    Destroy(gameObject);
                }
                catch { }
            });
        base.initIndicator();
        renderer = target.gameObject.GetComponent<Renderer>();
    }

    private IEnumerator playTextEffect()
    {
        var name = targetPlace.placetNameText.text;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < name.Length && normalImage.transform.position.magnitude <= 32f; i++)
        {
            sb.Append(name[i]);
            textList[0].text = sb.ToString();
            yield return new WaitForSeconds(0.02f);
        }
        yield return new WaitUntil(() => !onMousePoint);
        textList[0].text = "";
    }

    private IEnumerator playDistanceTextEffect()
    {
        var distance = targetPlace.placetDistanceText.text;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < distance.Length && normalImage.transform.position.magnitude <= 32f; i++)
        {
            sb.Append(distance[i]);
            textList[1].text = sb.ToString();
            yield return new WaitForSeconds(0.02f);
        }
        yield return new WaitUntil(() => !onMousePoint);
        textList[1].text = "";
    }

    private IEnumerator playImageEffects()
    {
        normalImage.color = selectColor;
        normalImage.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        yield return new WaitWhile(() => normalImage.transform.position.magnitude <= 32f);
        normalImage.color = idleColor;
        normalImage.transform.localScale = Vector3.one;
        onMousePoint = false;
        CharStateMng.Instance.Warp.currentWarpTarget = null;
    }

    protected override void displayIndicator()
    {
        Vector3 screenPoint = mainCamera.WorldToScreenPoint(target.position) - halfScreenSize;
        bool checkXpos = Mathf.Abs(screenPoint.x) > borderScreenSize.x;
        bool checkYpos = Mathf.Abs(screenPoint.y) > borderScreenSize.y;
        bool checkPos = checkXpos || checkYpos;

        screenPoint.x = Mathf.Clamp(screenPoint.x, -borderScreenSize.x, borderScreenSize.x);
        screenPoint.y = Mathf.Clamp(screenPoint.y, -borderScreenSize.y, borderScreenSize.y);
        screenPoint.z = 0f;

        if (!checkPos && !renderer.isVisible) // 스크린 화면을 넘어갈 경우
        {
            if (Mathf.Abs(screenPoint.x) < borderScreenSize.x)
                screenPoint.x = screenPoint.x > 0 ? borderScreenSize.x : -borderScreenSize.x;
            if (Mathf.Abs(screenPoint.y) > borderScreenSize.y)
                screenPoint.y = screenPoint.y > 0 ? borderScreenSize.y : -borderScreenSize.y;
            checkPos = true;
        }
        if (outOfImage)
        {
            Vector3 dir = transform.localPosition.normalized;
            float rot = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90f;
            outOfImage.transform.localRotation = Quaternion.Euler(0f, 0f, rot);
            outOfImage.gameObject.SetActive(checkPos && displayCheck); // 다른 행성에 있을 때 워프 포인트 표시를 방지
        }
        transform.localPosition = screenPoint;
        if (normalImage)
        {
            var color = normalImage.color;
            color.a = checkPos ? 0f : originalNormalImageAlpha;
            normalImage.color = color;//(checkPos) ? new Color(1f, 1f, 1f, 0f) : new Color(1f, 1f, 1f, 1f); //.gameObject.SetActive(!checkPos);
        }
    }

    private void OnDestroy()
    {
        
    }
}
