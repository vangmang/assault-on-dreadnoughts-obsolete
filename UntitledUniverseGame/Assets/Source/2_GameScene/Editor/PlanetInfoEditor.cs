﻿#if UNITY_EDITOR
using System.Text;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(PlanetInfo))]
public class PlanetInfoEditor : Editor
{
    private SerializedProperty planet;
    private SerializedProperty distanceWithStar;
    private SerializedProperty direction;
    private SerializedProperty possessedWarpPoints;
    private PlanetInfo planetInfo;

    private void OnEnable()
    {
        planetInfo = (PlanetInfo)target;
        planet = serializedObject.FindProperty("planet");
        distanceWithStar = serializedObject.FindProperty("distanceWithStar");
        direction = serializedObject.FindProperty("direction");
        possessedWarpPoints = serializedObject.FindProperty("possessedWarpPoints");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        distanceWithStar.floatValue = planetInfo.transform.localPosition.magnitude / StarMap.editableDistance;
        direction.vector3Value = planetInfo.transform.localPosition.normalized;
        EditorGUILayout.PropertyField(planet);
        EditorGUILayout.PropertyField(distanceWithStar, new GUIContent("Distance with star"));
        EditorGUILayout.PropertyField(direction);
        EditorGUILayout.PropertyField(possessedWarpPoints, true);
        StringBuilder sb = new StringBuilder(distanceWithStar.floatValue.ToString("N2"));
        sb.Append("AU").ToString();
        planetInfo.planet.placetDistanceText.text = sb.ToString();

        serializedObject.ApplyModifiedProperties();
    }
}
#endif