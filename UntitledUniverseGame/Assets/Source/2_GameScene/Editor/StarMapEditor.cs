﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UniRx;

[CustomEditor(typeof(StarMap))]
[CanEditMultipleObjects]
public class StarMapEditor : Editor
{
    private SerializedProperty starMapList;
    private SerializedProperty planetInfo;
    private SerializedProperty stationPrefab;
    private SerializedProperty planetCount;
    private SerializedProperty mapEditor;
    private SerializedProperty bossPointDisplayer;
    private SerializedProperty stationPointDisplayer;
    private StarMap starMap;

    private void OnEnable()
    {
        EditorApplication.update = DrawLines;
        starMapList = serializedObject.FindProperty("starMapList");
        planetInfo = serializedObject.FindProperty("planetInfoPrefab");
        planetCount = serializedObject.FindProperty("planetCount");
        mapEditor = serializedObject.FindProperty("mapEditor");
        stationPrefab = serializedObject.FindProperty("stationPrefab");
        bossPointDisplayer = serializedObject.FindProperty("bossPointDisplayer");
        stationPointDisplayer = serializedObject.FindProperty("stationPointDisplayer");
        if (!starMap)
            starMap = (StarMap)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(starMapList, true);
        EditorGUILayout.PropertyField(planetInfo);
        EditorGUILayout.PropertyField(mapEditor);
        EditorGUILayout.PropertyField(stationPrefab);
        EditorGUILayout.PropertyField(bossPointDisplayer);
        EditorGUILayout.PropertyField(stationPointDisplayer);
        planetCount.intValue = EditorGUILayout.IntField(new GUIContent("planetCount"), Mathf.Clamp(planetCount.intValue, 0, 15));

        if (GUILayout.Button("BuildMap"))
            instantiatePlanets();
        if (GUILayout.Button("ClearMap"))
            clearPlanets();
        DrawLines();
        serializedObject.ApplyModifiedProperties();
    }

    [ExecuteInEditMode]
    public void DrawLines()
    {
        try
        {
            if (starMap.starMapList.Count > 0)
            {
                foreach (var planet in starMap.starMapList)
                {
                    Debug.DrawLine(planet.transform.position, Vector3.zero);
                }
            }
        }
        catch { }
    }

    private void instantiatePlanets()
    {
        if (starMap.starMapList.Count == 0 && starMap.recentArrayLength == planetCount.intValue)
            starMap.recentArrayLength = 0;
        else if (starMap.starMapList.Count == planetCount.intValue || planetCount.intValue == 0)
            return;
        if (planetCount.intValue < starMap.starMapList.Count)
        {
            for (int i = starMap.starMapList.Count - 1; i >= planetCount.intValue; i--)
            {
                DestroyImmediate(starMap.starMapList[i].gameObject);
                starMap.starMapList.RemoveAt(i);
            }
        }
        else
        {
            for (int i = starMap.starMapList.Count; i < planetCount.intValue; i++)
            {
                var pos = new Vector3(Mathf.Cos(i) * ((i + 1) * StarMap.editableDistance), 0f, Mathf.Sin(i) * ((i + 1) * StarMap.editableDistance));
                var planetInfo = Instantiate(starMap.planetInfoPrefab, starMap.mapEditor).GetComponent<PlanetInfo>();
                planetInfo.transform.localPosition = pos;
                starMap.starMapList.Add(planetInfo);
            }
        }
        starMap.recentArrayLength = starMap.starMapList.Count;
    }

    private void OnDisable()
    {
        //EditorApplication.update -= DrawLines;
    }

    private void clearPlanets()
    {
        for (int i = starMap.starMapList.Count - 1; i >= 0; i--)
        {
            DestroyImmediate(starMap.starMapList[i].gameObject);
            starMap.starMapList.Remove(starMap.starMapList[i]);
        }
        starMapList.arraySize = 0;
        planetCount.intValue = 0;
        starMap.recentArrayLength = 0;
    }
}
#endif