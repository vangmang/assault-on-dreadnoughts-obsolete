﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAudioFadeIn : MonoBehaviour
{
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(audioFadin());    
    }

    private IEnumerator audioFadin()
    {
        float t = 0f;
        float rev = 0f;
        while(t <= 0.997f)
        {
            rev = Mathf.Lerp(1f, 0f, t);
            t += rev * 0.5f * Time.deltaTime;
            audioSource.volume = t;
            yield return null;
        }
        audioSource.volume = 1f;
    }
}
