﻿using System.Collections;
using UnityEngine;
using System;
using UniRx;
using System.Collections.Generic;

public class CreateEnemies : MonoBehaviour
{
    [Serializable]
    public struct _ENEMY_FRIGATE_
    {
        public GameObject enemyFrigate;
        public int enemyFrigateCount;
        public Transform enemyFrigateParent;
    }
    [Serializable]
    public struct _ENEMY_DREADNOUGHT_
    {
        public GameObject enemyDreadnought;
        public int enemyDreadnoughtCount;
        public Transform enemyDreadnoughtParent;
    }
    public _ENEMY_FRIGATE_ EnemyFrigate;
    [Header("Boss")]
    public _ENEMY_DREADNOUGHT_ EnemyDreadnought;
    [SerializeField] private WarpPoint currentWarpPoint;
    public List<Action> Updates { get; private set; }
    public bool enableEnemyInstantiation;

    private void Awake()
    {
        enableEnemyInstantiation = true;
        Updates = new List<Action>();
    }

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => CharStateMng.Instance.CurrentInfo.cur_WarpPoint != null);
        if (currentWarpPoint == CharStateMng.Instance.CurrentInfo.cur_WarpPoint || currentWarpPoint.WarpPointType.Equals(WarpPoint._WARP_POINT_TYPE_.stationPoint))
            yield break;
        this.ObserveEveryValueChanged(_ => CharStateMng.Instance.CurrentInfo.cur_WarpPoint).
            Where(_ => enableEnemyInstantiation).
            Subscribe(_ =>
            {
                bool samePoint = CharStateMng.Instance.CurrentInfo.cur_WarpPoint.Equals(currentWarpPoint);
                gameObject.SetActive(samePoint);
                if (samePoint)
                {
                    CreateFrigates();
                    CreateDreadnoughts();
                }
            });
    }

    public void CreateFrigates()
    {
        for (int i = 0; i < EnemyFrigate.enemyFrigateCount; i++)
        {
            EnemyFrigateStateMng enemyFrigate = Instantiate(EnemyFrigate.enemyFrigate, EnemyFrigate.enemyFrigateParent).GetComponentInChildren<EnemyFrigateStateMng>();
            enemyFrigate.transform.parent.gameObject.name = "Frigate[" + i + "]";
            enemyFrigate.createEnemies = this;
            enemyFrigate.gameMng = GameMng.Instance;
            enemyFrigate.Identify = /*ShipBase.IDENTIFY.hostile;*/ i < 30 ? ShipBase.IDENTIFY.hostile : ShipBase.IDENTIFY.ally;
            enemyFrigate.TargetingAlert.transform.parent = CharStateMng.Instance.CharacterFeatureInfo.targetingAlertTransform;
            enemyFrigate.Info.ShipTransform.localPosition = Vector3.zero;
            enemyFrigate.originalPos =
                new Vector3(Mathf.Cos(i * 36f) * 1200f, Mathf.Sin(i * 36f) * 1200f, -5500 - i * 200f);
            enemyFrigate.CurrentInfo.cur_WarpPoint = currentWarpPoint;
        }
    }

    private void Update()
    {
        for (int i = 0; i < Updates.Count; i++)
            Updates[i]();
    }

    public void CreateDreadnoughts()
    {
        if (currentWarpPoint.WarpPointType.Equals(WarpPoint._WARP_POINT_TYPE_.bossPoint))
        {
            for (int i = 0; i < EnemyDreadnought.enemyDreadnoughtCount; i++)
            {
                EnemyDreadnoughtStateMng enemyDreadnought =
                    Instantiate(EnemyDreadnought.enemyDreadnought, EnemyDreadnought.enemyDreadnoughtParent).GetComponentInChildren<EnemyDreadnoughtStateMng>();
                enemyDreadnought.transform.parent.gameObject.name = "Boss_Dreadnought[" + i + "]";
                enemyDreadnought.createEnemies = this;
                enemyDreadnought.gameMng = GameMng.Instance;
                enemyDreadnought.Identify = ShipBase.IDENTIFY.hostile;
                enemyDreadnought.Info.ShipTransform.localPosition = Vector3.zero;
                enemyDreadnought.originalPos =
                    (currentWarpPoint.currentPlanet.planetInfo.transform.position - currentWarpPoint.targetPlanet.planetInfo.transform.position).normalized * 7000f;
                enemyDreadnought.CurrentInfo.cur_WarpPoint = currentWarpPoint;
            }
        }
    }

    public void AddEnmiesIntoLocalArea(TargetingModuleStateMng.ITargetable target)
    {
        EnemyManager.Instance.targets.Add(target);
    }

    private void OnDestroy()
    {

    }
}
