﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Unity.Jobs;
using Unity.Collections;

public class EnemyManager : MonoBehaviour
{
    private EnemyManager() { }
    private static EnemyManager instance = null;
    public static EnemyManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(EnemyManager)) as EnemyManager;
            return instance;
        }
    }


    public GameObject EnemyIndicatorParent;
    public GameObject AlertIndicatorParent;

    /// <summary>
    /// 로컬에 도착하면 로컬 적들이 할당 된다.
    /// </summary>
    public readonly List<TargetingModuleStateMng.ITargetable> targets = new List<TargetingModuleStateMng.ITargetable>();

    public void ClearTargets()
    {
        targets.Clear();
    }

    // Start is called before the first frame update
    void OnDestroy()
    {

    }
}
