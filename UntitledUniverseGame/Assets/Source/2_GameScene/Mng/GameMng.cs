﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public sealed class GameMng : MonoBehaviour
{
    private GameMng() { }
    private static GameMng instance = null;
    public static GameMng Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(GameMng)) as GameMng;
            return instance;
        }
    }
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Camera _UICamera;
    [SerializeField] private Transform warpTargetIndicatorTransform;
    /// <summary>
    /// 태양
    /// </summary>
    [SerializeField] private Light directionalLight;
    public Camera MainCamera { get { return mainCamera; } }
    public Camera UICamera { get { return _UICamera; } }
    public Light DirectionalLight { get { return directionalLight; } }
    public Transform WarpTargetIndicatorTransform { get { return warpTargetIndicatorTransform; } }
    public CanvasGroup UI_IndicatorsCanvasGroup;
    public Image MicroWormholeFadeImage;
    public GameSceneFadeImage fadeImage;
    public MouseFollowingCamera mouseFollowingCamera;
    public MouseFollowingCamera effectCamera;
    public CameraShaker cameraShaker;


    [Serializable] 
    public struct _PAUSE_INFO_ 
    {
        public GameObject pauseParent;
        public GameObject pauseWindow;
        public GameObject keyboardExplainWindow;
        public GameObject settingWindow;
        public GameObject gameplayWindow;
        public GameObject audioWindow;
        public GameObject graphicWindow;      // 3.10 진짜 씨발 존나 골때리는 상황 발생
                                            // 진짜 너무 어이가 없다. 세팅에서 그래픽버튼 누르면 오디오가 튀어나오고 오디오버튼 누르면 그래픽이 튀어나온다
                                            // 처음엔 단순 인스펙터에 잘못 할당한 줄 알았는데 ㅅㅂ; 코드도 이상한데 없다. 인스펙터 상에서 오디오와 그래픽을 바꾸는걸로 일단 임시 대처 시발 뭔버그냐
        public GameObject buildWindow;
        public GameObject infoWindow;
        public GameObject systemMapWindow;
        public GameObject stationWindow;
        public GameObject mainMenuPopup;
        public GameObject quitPopup;
        public GameObject currentWindow;
        public Stack<GameObject> recentWindows;

        [Serializable]
        public struct _GAMEPLAY_SETTING_
        {
            public Slider mouseSensitivity;
        }

        [Serializable]
        public struct _GRAPHIC_SETTING_
        {

        }

        [Serializable]
        public struct _AUDIO_SETTING_
        {
            public Slider masterVolume;
            public Slider backgroundVolume;
        }

        public _GAMEPLAY_SETTING_ GamePlaySetting;
        public _GRAPHIC_SETTING_ GraphicSetting;
        public _AUDIO_SETTING_ AudioSetting;
    }
    [Serializable]
    public struct _GOODS_
    {
        public int bountyAmount;
        public int dreadnoughtSerialChip;

        public Text bountyAmountText;
        public Text dreadoughtSerialChipText;
    }


    public _PAUSE_INFO_ PauseInfo;
    public _GOODS_ Goods;

    public CharStateMng charStateMng;
    public GameStateMng gameStateMng;
    public SoundManager soundMng;
    public StarSystemDisplayer starSystemDisplayer;
    public GameObject jumpPortalPrefab;
    public Transform IndicatorTransform;
    public WarpPoint bossPoint;
    public WarpPoint stationPoint;
    public GameObject QRFEnableDisplayer;

    [Serializable]
    private struct _QRF_ORGANIZATION_
    {
        public GameObject frigatePrefab;
        public GameObject assaultFrigatePrefab;
        public GameObject fighterPrefab;
        public GameObject cruiserPrefab;
    }

    public readonly List<ShipBase.SHIP_TYPE> QRFMembers = new List<ShipBase.SHIP_TYPE>();
    public bool QRFMemberOrganizationCheck;
    public bool IsQRFLaunched { get; private set; }
    [SerializeField] private _QRF_ORGANIZATION_ qrfOrganization;

    [NonSerialized] public bool enableJumpPortalGeneration;

    [Serializable]
    public struct _LEVELS_
    {
        public Transform LevelParent;
        public List<GameObject> LevelPrefabs;
        public int currentLevel;
    }
    public _LEVELS_ Levels;

    private IEnumerator Start()
    {
        this.ObserveEveryValueChanged(_ => Goods.bountyAmount).
            Subscribe(_ =>
            {
                Goods.bountyAmountText.text = Goods.bountyAmount.ToString();
            });
        this.ObserveEveryValueChanged(_ => Goods.dreadnoughtSerialChip).
            Subscribe(_ =>
            {
                Goods.dreadoughtSerialChipText.text = Goods.dreadnoughtSerialChip.ToString();
            });

        yield return new WaitForFixedUpdate();
        Levels.currentLevel = SavedData.SavedDataContainer.Level.currentLevel;
        LevelDataContainer levelDataContainer = Instantiate(Levels.LevelPrefabs[Levels.currentLevel], Levels.LevelParent).GetComponent<LevelDataContainer>();
        starSystemDisplayer.starMap = levelDataContainer.starMap;
        directionalLight = levelDataContainer.directionalLight;
        PauseInfo.recentWindows = new Stack<GameObject>();
        this.ObserveEveryValueChanged(_ => charStateMng.Warp.isWarpHighlight).
            Where(_ => charStateMng.GetCurrentState.Equals(CharStateMng.CHARACTER_STATES.warp)).
            Subscribe(_ =>
            {
                IEnumerator playEffect = playIndicatorFadeEffect(charStateMng.Warp.isWarpHighlight);
                StartCoroutine(playEffect);      
            });
        this.ObserveEveryValueChanged(_ => enableJumpPortalGeneration).
            Where(x => x).
            Subscribe(_ =>
            {
                JumpPortal jumpPortal = Instantiate(jumpPortalPrefab).GetComponent<JumpPortal>();
                jumpPortal.gameMng = this;
            });
        this.ObserveEveryValueChanged(_ => QRFMemberOrganizationCheck).
            Subscribe(_ =>
            {
                QRFEnableDisplayer.SetActive(QRFMemberOrganizationCheck);
            });
    }

    public void SetDirectionalLightRotation(Vector3 eulerAngle)
    {
        var rot = directionalLight.transform.rotation;
        rot.eulerAngles = eulerAngle;
        directionalLight.transform.rotation = rot;
    }

    public void SetDirectionalLightRotation(Quaternion rot)
    {
        directionalLight.transform.rotation = rot;
            //Quaternion.Slerp(directionalLight.transform.rotation, rot, Time.deltaTime) : rot;
    }

    // inOut: in => true out => false
    private IEnumerator playIndicatorFadeEffect(bool inOut)
    {
        float t = inOut ? 1f : 0f;
        float end = inOut ? 0f : 1f;
        while(inOut ? t >= 0f : t <= 1f)
        {
            if (inOut)
                t -= Time.deltaTime;
            else
                t += Time.deltaTime;
            UI_IndicatorsCanvasGroup.alpha = t;
            yield return null;
        }
        UI_IndicatorsCanvasGroup.alpha = end;
    }

    public void LoadScene(LoadingMng.SCENE scene)
    {
        IEnumerator loadScene = invokeLoadScene(scene);
        StartCoroutine(loadScene);
    }

    /// <summary>
    /// 기동타격대는 해당 성계에서 딱 한번 고용, 호출할 수 있다.
    /// </summary>
    public void CallQRF()
    {
        if (charStateMng.CurrentInfo.cur_WarpPoint.currentPlanet.Equals(stationPoint.currentPlanet))
        {
            charStateMng.CharacterFeatureInfo.MessageText.text = "Can't call Quick Reaction Force in Station planet.";
            charStateMng.CharacterFeatureInfo.hitEffectTextAnim.Play();
            return;
        }
        IsQRFLaunched = true;
        QRFMemberOrganizationCheck = false;
        StartCoroutine(directQRFWarpArrival());
    }

    // 아군 기동타격대가 워프해서 온 것 처럼 연출
    private IEnumerator directQRFWarpArrival()
    {
        foreach (var memberType in QRFMembers)
        {
            GameObject instantiation = null;
            switch (memberType)
            {
                case ShipBase.SHIP_TYPE.frigate:
                    instantiation = qrfOrganization.frigatePrefab;
                    break;
                case ShipBase.SHIP_TYPE.assaultFrigate:
                    instantiation = qrfOrganization.assaultFrigatePrefab;
                    break;
                case ShipBase.SHIP_TYPE.fighter:
                    instantiation = qrfOrganization.fighterPrefab;
                    break;
                case ShipBase.SHIP_TYPE.crusier:
                    instantiation = qrfOrganization.cruiserPrefab;
                    break;
                default: break;
            }
            if (instantiation)
            {
                ShipBase shipBase = Instantiate(instantiation, charStateMng.CurrentInfo.cur_WarpPoint.createEnemies.transform).GetComponentInChildren<ShipBase>();
                var overrideWarpTargetPos = (UnityEngine.Random.insideUnitSphere * 4500f);
                var pos = stationPoint.currentPlanet.transform.position + overrideWarpTargetPos;
                shipBase.overrideWarpTargetPos = overrideWarpTargetPos;
                shipBase.Info.ShipTransform.localPosition = pos;
                shipBase.Identify = ShipBase.IDENTIFY.ally;
                shipBase.gameMng = this;
                shipBase.CurrentInfo.cur_WarpPoint = charStateMng.CurrentInfo.cur_WarpPoint;
                shipBase.OverrideIdentify();
                shipBase.DirectWarpArrival();
            }
            // 워프 드라이브 연출
            yield return new WaitForSeconds(0.25f);
        }
    }    

    private IEnumerator invokeLoadScene(LoadingMng.SCENE scene)
    {
        fadeImage.SetImageAlpha1F();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene((int)LoadingMng.SCENE.loading);
        LoadingMng.Instance.n_TargetScene = (int)scene;
    }

    public static _GOODS_ operator + (GameMng gameMng, SavedData.SavedDataContainer._GOODS_ goods)
    {
        gameMng.Goods.bountyAmount = goods.bountyAmount;
        gameMng.Goods.dreadnoughtSerialChip = goods.dreadnoughtSerialChip;
        return gameMng.Goods;
    }

    private void OnDestroy()
    {
        
    }
}
