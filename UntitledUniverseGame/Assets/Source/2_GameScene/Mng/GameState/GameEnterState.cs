﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GameEnterState : State<GameStateMng>
{
    public override Enum GetState
    {
        get { return GameStateMng.GAME_STATES.enter; }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
