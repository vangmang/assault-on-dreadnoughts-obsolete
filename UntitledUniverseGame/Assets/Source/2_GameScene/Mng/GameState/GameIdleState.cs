﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GameIdleState : State<GameStateMng>
{
    public override Enum GetState
    {
        get { return GameStateMng.GAME_STATES.idle; }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
