﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class GamePauseAndShowBuildState : State<GameStateMng>
{
    public override Enum GetState
    {
        get { return GameStateMng.GAME_STATES.pauseAndShowBuild; }
    }

    public override void Enter(params object[] o_Params)
    {
        try // 2020.03.11: 씬 전환하고 자꾸 MissingGameObject뜬다. 싱글톤 문제인듯. 나중에 확인바람 
        {
            Time.timeScale = 0f;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            instance.gameMng.PauseInfo.pauseParent.SetActive(true);
            instance.gameMng.PauseInfo.currentWindow.SetActive(false);
        }
        catch { }
        instance.gameMng.PauseInfo.pauseParent.SetActive(true);

        instance.gameMng.PauseInfo.currentWindow = instance.gameMng.PauseInfo.buildWindow;
        instance.isShowingInfoState = true;
        instance.gameMng.PauseInfo.currentWindow.SetActive(true);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        instance.isShowingInfoState = false;
        instance.gameMng.PauseInfo.currentWindow.SetActive(false);
    }
}
