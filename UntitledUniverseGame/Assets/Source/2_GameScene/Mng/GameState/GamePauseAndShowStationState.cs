﻿using FiniteStateMachine;
using System;
using UnityEngine;

public class GamePauseAndShowStationState : State<GameStateMng>
{
    public override Enum GetState
    {
        get { return GameStateMng.GAME_STATES.pauseAndShowStation; }
    }

    public override void Enter(params object[] o_Params)
    {
        Time.timeScale = 0f;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        instance.isShowingInfoState = true;
        instance.gameMng.PauseInfo.pauseParent.SetActive(true);
        instance.gameMng.PauseInfo.stationWindow.SetActive(true);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        Time.timeScale = 1f;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        instance.isShowingInfoState = false;
        instance.gameMng.PauseInfo.pauseParent.SetActive(false);
        instance.gameMng.PauseInfo.stationWindow.SetActive(false);
    }
}
