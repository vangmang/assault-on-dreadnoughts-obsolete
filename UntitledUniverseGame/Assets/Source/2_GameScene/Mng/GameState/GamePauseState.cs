﻿using System.Collections;
using System.Collections.Generic;
using FiniteStateMachine;
using UnityEngine;
using System;

public class GamePauseState : State<GameStateMng>
{
    public override Enum GetState
    {
        get { return GameStateMng.GAME_STATES.pause; }
    }

    public override void Enter(params object[] o_Params)
    {
        try // 2020.03.11: 씬 전환하고 자꾸 MissingGameObject뜬다. 싱글톤 문제인듯. 나중에 확인바람 
        { 
            Time.timeScale = 0f;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            instance.gameMng.PauseInfo.pauseParent.SetActive(true);
            instance.gameMng.PauseInfo.pauseWindow.SetActive(true);
            instance.gameMng.PauseInfo.currentWindow = instance.gameMng.PauseInfo.pauseWindow;
            instance.Escape = instance.escapePause;
        }
        catch { }
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        if (!instance.isShowingInfoState)
        {
            try // 2020.03.11: 씬 전환하고 자꾸 MissingGameObject뜬다. 싱글톤 문제인듯. 나중에 확인바람 
            {
                instance.gameMng.PauseInfo.pauseParent.SetActive(false);
                instance.gameMng.PauseInfo.pauseWindow.SetActive(true);
                //instance.gameMng.PauseInfo.currentWindow = null;
                Time.timeScale = 1f;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            catch { }
        }
    }
}
