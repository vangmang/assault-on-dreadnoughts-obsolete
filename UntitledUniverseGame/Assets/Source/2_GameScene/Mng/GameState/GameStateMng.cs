﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UniRx;

public class GameStateMng : SingletonStateMng<GameStateMng>
{
    public enum GAME_STATES
    {
        loading,
        enter,
        idle,
        cinematicPlay,
        pause,
        pauseAndShowInfo,
        pauseAndShowMap,
        pauseAndShowBuild,
        pauseAndShowStation,
        over,
        exit
    }

    public GameMng gameMng;
    public delegate void _Escape_();
    public _Escape_ Escape;
    public bool isShowingInfoState; // 빌드, 맵, 인벤토리, 스테이션 오픈 여부

    private void Awake()
    {
        Escape = escapePause;
        InitializeState();
        StateEnter(GAME_STATES.loading);
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitUntil(() => gameMng.fadeImage.Init);
        this.ObserveEveryValueChanged(_ => GetCurrentState).
            Subscribe(state =>
            {
                switch(state)
                {
                    case GAME_STATES.idle:
                        if (GetRecentState.Equals(GAME_STATES.loading))
                            break;
                        gameMng.soundMng.ResumeAudios(); 
                        break;
                    case GAME_STATES.pause:
                            gameMng.soundMng.PauseAudios();
                        break;
                    case GAME_STATES.pauseAndShowStation:
                        gameMng.soundMng.PauseAudios();
                        break;
                    default: break;
                }
            });
        Observable.EveryUpdate().
            Where(_ => Input.GetKeyDown(KeyCode.Escape)).
            Where(_ => !GetCurrentState.Equals(GAME_STATES.cinematicPlay)).
            Subscribe(_ =>
            {
                // 게임씬에서 메인메뉴로 돌아와서 ESC누르면 에러 발생한다. 그래서 예외처리함
                try
                {
                    Escape();
                }
                catch { }
            }).AddTo(this);
        Observable.EveryUpdate(). // 지도
            Where(_ => Input.GetKeyDown(KeyCode.M)).
            Where(_ => !GetCurrentState.Equals(GAME_STATES.cinematicPlay)).
            Where(_ => !GetCurrentState.Equals(GAME_STATES.pauseAndShowMap)).
            Subscribe(_ =>
            {
                gameMng.soundMng.PauseAudios();
                isShowingInfoState = true;
                gameMng.PauseInfo.recentWindows.Push(gameMng.PauseInfo.currentWindow); // 일시정지 창 푸쉬
                StateTransition(GAME_STATES.pauseAndShowMap);
            }).AddTo(this);
        Observable.EveryUpdate(). // 빌드
            Where(_ => Input.GetKeyDown(KeyCode.B)).
            Where(_ => !GetCurrentState.Equals(GAME_STATES.cinematicPlay)).
            Where(_ => !GetCurrentState.Equals(GAME_STATES.pauseAndShowBuild)).
            Subscribe(_ =>
            {
                gameMng.soundMng.PauseAudios();
                isShowingInfoState = true;
                gameMng.PauseInfo.recentWindows.Push(gameMng.PauseInfo.currentWindow); // 일시정지 창 푸쉬
                StateTransition(GAME_STATES.pauseAndShowBuild);
            }).AddTo(this);
        Observable.EveryUpdate(). // 인벤토리
            Where(_ => Input.GetKeyDown(KeyCode.I)).
            Where(_ => !GetCurrentState.Equals(GAME_STATES.cinematicPlay)).
            Where(_ => !GetCurrentState.Equals(GAME_STATES.pauseAndShowInfo)).
            Subscribe(_ =>
            {
                gameMng.soundMng.PauseAudios();
                isShowingInfoState = true;
                gameMng.PauseInfo.recentWindows.Push(gameMng.PauseInfo.currentWindow); // 일시정지 창 푸쉬
                StateTransition(GAME_STATES.pauseAndShowInfo);
            }).AddTo(this);
    }

    public void escapePause()
    {
        GAME_STATES state = !GetCurrentState.Equals(GAME_STATES.pause) ? GAME_STATES.pause : GAME_STATES.idle;
        StateTransition(state);
    }

    private void OnDestroy()
    {
        Escape = null;
    }
}
