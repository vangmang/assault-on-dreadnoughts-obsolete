﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPortal : MonoBehaviour
{
    public GameMng gameMng;
    public Indicator indicatorPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Indicator indicator = Instantiate(indicatorPrefab, gameMng.IndicatorTransform).GetComponent<Indicator>();
        indicator.target = transform;
        indicator.mainCamera = gameMng.MainCamera;
    }
}
