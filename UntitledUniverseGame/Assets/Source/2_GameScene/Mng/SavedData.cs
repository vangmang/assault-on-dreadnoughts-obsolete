﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;

public class SavedData : MonoBehaviour
{
    [SerializeField]
    private TextAsset processXml;
    private static SavedData instance = null;
    public static SavedData Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
        InitData();
    }

    public void InitData()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(processXml.text);
        XmlElement savedData = xmlDoc["SavedData"];
        XmlElement playerInfo = savedData["PlayerInfo"];
        XmlElement level = savedData["Level"];
        XmlElement setting = savedData["Setting"];
        XmlElement upgradTimes = playerInfo["UpgradeTimes"];
        XmlElement goods = playerInfo["Goods"];
        XmlElement levelInfo = level["Info"];
        XmlElement gamePlay = setting["Gameplay"];
        XmlElement audio = setting["Audio"];

        SavedDataContainer.UpgradeTimesStat.velocity = int.Parse(upgradTimes.GetAttribute("velocity"));
        SavedDataContainer.UpgradeTimesStat.acceleration = int.Parse(upgradTimes.GetAttribute("acceleration"));
        SavedDataContainer.UpgradeTimesStat.mobility = int.Parse(upgradTimes.GetAttribute("mobility"));
        SavedDataContainer.UpgradeTimesStat.shieldCapacity = int.Parse(upgradTimes.GetAttribute("shieldCapacity"));
        SavedDataContainer.UpgradeTimesStat.power = int.Parse(upgradTimes.GetAttribute("power"));
        SavedDataContainer.UpgradeTimesStat.weaponRange = int.Parse(upgradTimes.GetAttribute("weaponRange"));
        SavedDataContainer.UpgradeTimesStat.weaponDamage = int.Parse(upgradTimes.GetAttribute("weaponDamage"));
        SavedDataContainer.UpgradeTimesStat.weaponFireRate = int.Parse(upgradTimes.GetAttribute("weaponFireRate"));

        SavedDataContainer.Goods.bountyAmount = int.Parse(goods.GetAttribute("bounty"));
        SavedDataContainer.Goods.dreadnoughtSerialChip = int.Parse(goods.GetAttribute("dreadnoughtSerialChip"));

        SavedDataContainer.Level.currentLevel = int.Parse(levelInfo.GetAttribute("currentLevel"));
        SavedDataContainer.Level.currentWarpPoint = levelInfo.GetAttribute("currentWarpPoint");

        SavedDataContainer.Setting.mouseSensitivity = float.Parse(gamePlay.GetAttribute("mouseSensitivity"));
        SavedDataContainer.Setting.backgroundAudio = float.Parse(audio.GetAttribute("backgroundAudio"));
    }

    public void SetGameProcess(int level)
    {
        SavedDataContainer.Level.currentLevel = level;
    }

    public void SetGameProcess(string warpPoint)
    {
        SavedDataContainer.Level.currentWarpPoint = warpPoint;
    }

    public void SetGameProcess(int level, string warpPoint)
    {
        SavedDataContainer.Level.currentLevel = level;
        SavedDataContainer.Level.currentWarpPoint = warpPoint;
    }

//#if UNITY_EDITOR
//    // xml 초기화용 개발용 메소드
//    private void InitXml()
//    {
//        if (processXml.text.Equals(""))
//        {
//            XmlDocument xmlDoc = new XmlDocument();
//            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null));
//            XmlElement savedData = xmlDoc.CreateElement("SavedData");
//            xmlDoc.AppendChild(savedData);
//            XmlElement playerInfo = xmlDoc.CreateElement("playerInfo");
//            savedData.AppendChild(playerInfo);
//            XmlElement upgradeTimes = xmlDoc.CreateElement("upgradeTimes");
//            playerInfo.AppendChild(upgradeTimes);
//            upgradeTimes.SetAttribute("velocity", "10");
//            upgradeTimes.SetAttribute("acceleration", "10");
//            upgradeTimes.SetAttribute("mobility", "10");
//            upgradeTimes.SetAttribute("shieldCapacity", "10");
//            upgradeTimes.SetAttribute("power", "10");
//            upgradeTimes.SetAttribute("weaponRange", "10");
//            upgradeTimes.SetAttribute("weaponDamage", "10");
//            upgradeTimes.SetAttribute("weaponFireRate", "10");
//            XmlElement goods = xmlDoc.CreateElement("goods");
//            playerInfo.AppendChild(goods);
//            goods.SetAttribute("bounty", "0");
//            goods.SetAttribute("dreadnoughtSerialChip", "0");

//            File.WriteAllText(UnityEditor.AssetDatabase.GetAssetPath(processXml), xmlDoc.OuterXml);
//            UnityEditor.EditorUtility.SetDirty(processXml);
//        }
//    }
//#endif

    public static class SavedDataContainer
    {
        public struct _UPGRADE_TIMES_STAT_
        {
            public int velocity;
            public int acceleration;
            public int mobility;
            public int shieldCapacity;
            public int power;
            public int weaponRange;
            public int weaponDamage;
            public int weaponFireRate;
        }

        public struct _GOODS_
        {
            public int bountyAmount;
            public int dreadnoughtSerialChip;
        }

        public struct _LEVEL_
        {
            public int currentLevel;
            public string currentWarpPoint;
        }

        public struct _SETTING_
        {
            public float mouseSensitivity;
            public float backgroundAudio;
        }

        public static _UPGRADE_TIMES_STAT_ UpgradeTimesStat;
        public static _GOODS_ Goods;
        public static _LEVEL_ Level;
        public static _SETTING_ Setting;
    }

    private void OnDestroy()
    {
        
    }
}



