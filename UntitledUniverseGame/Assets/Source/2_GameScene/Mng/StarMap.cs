﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

public sealed class StarMap : MonoBehaviour
{
    private static StarMap instance = null;
    public static StarMap Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(StarMap)) as StarMap;
            return instance;
        }
    }

    public const float editableDistance = 36f;
    public List<PlanetInfo> starMapList;
    public GameObject planetInfoPrefab;
    public GameObject stationPrefab;
    public GameObject bossPointDisplayer;
    public GameObject stationPointDisplayer;
    public Transform mapEditor;
    public int planetCount;
    public int recentArrayLength;
    public readonly List<WarpPoint> WarpPoints = new List<WarpPoint>();
    public int randomStartPoint;

    private void Start()
    {
        randomStartPoint = Random.Range(0, WarpPoints.Count - 1);
        CharStateMng.Instance.CurrentInfo.cur_WarpPoint = WarpPoints[randomStartPoint];
        CharStateMng.Instance.CurrentInfo.cur_Stage = CharStateMng.Instance.CurrentInfo.cur_WarpPoint.stage;
        var current = WarpPoints.First(point => point.Equals(CharStateMng.Instance.CurrentInfo.cur_WarpPoint));
        WarpPoints.Remove(current);

        // 현재 포인트의 행성에는 보스 포인트가 존재하지 않는다.
        while (WarpPoints.Any(point => point.currentPlanet.Equals(current.currentPlanet)))
        {
            var samePlanetPoint = WarpPoints.First(planet => planet.currentPlanet.Equals(current.currentPlanet));
            WarpPoints.Remove(samePlanetPoint);
        }

        SetBossPoint();
        SetStationPoint();
    }

    private void SetBossPoint()
    {
        int randBossCount = WarpPoints.Count;
        // 보스 포인트 지정
        for (int i = 0; i < WarpPoints.Count; i++)
        {
            int rand = Random.Range(1, randBossCount);
            if (rand == 1)
            {
                var current = WarpPoints[i];
                current.WarpPointType = WarpPoint._WARP_POINT_TYPE_.bossPoint;
                GameMng.Instance.bossPoint = current;
                WarpPoints.Remove(current);
                IEnumerator invoke = waitAllocationOfBossPointIndicator(current);
                StartCoroutine(invoke);
                while (WarpPoints.Any(point => point.currentPlanet.Equals(current.currentPlanet)))
                {
                    var samePlanetPoint = WarpPoints.First(planet => planet.currentPlanet.Equals(current.currentPlanet));
                    WarpPoints.Remove(samePlanetPoint);
                }
                break;
            }
            randBossCount--;
        }
    }

    private IEnumerator waitAllocationOfBossPointIndicator(WarpPoint current)
    {
        yield return new WaitUntil(() => current.currentPlanet.Indictaor);
        Instantiate(bossPointDisplayer, current.currentPlanet.Indictaor.transform);
    }

    private void SetStationPoint()
    {
        int randStationCount = WarpPoints.Count;
        // 스테이션 포인트 지정
        for(int i = 0; i < WarpPoints.Count; i++)
        {
            int rand = Random.Range(1, randStationCount);
            if (rand == 1)
            {
                var current = WarpPoints[i];
                current.WarpPointType = WarpPoint._WARP_POINT_TYPE_.stationPoint;
                IEnumerator invoke = waitAllocationOfStationPointIndicator(current);
                StartCoroutine(invoke);
                var station = Instantiate(stationPrefab, current.transform).GetComponent<Station>();                
                GameMng.Instance.stationPoint = current;
                station.currentWarpPoint = current;
                break;
            }
            randStationCount--;
        }
    }

    private IEnumerator waitAllocationOfStationPointIndicator(WarpPoint current)
    {
        yield return new WaitUntil(() => current.WarpIndicator);
        StationPointDisplayer displayer = Instantiate(stationPointDisplayer, current.WarpIndicator.transform).GetComponent<StationPointDisplayer>();
        displayer.warpPoint = current;
    }

    /// <summary>
    /// 함선 워프 후 워프 한 함선을 기준으로 행성 좌표를 재설정해준다.
    /// </summary>
    public void SetStarMap()
    {
        var list = new List<PlanetInfo>(starMapList);

        var currentStage = starMapList.First(p => p.planet.stage.isCurrentStage);
        list.Remove(currentStage);
        foreach (var planet in list)
        {
            var pos = currentStage.planet.planetInfo.transform.position;
            var wantedPos = planet.transform.position - pos;
            planet.transform.position -= pos;
            var dist = wantedPos.magnitude / 100000f * 0.25f;
            //StringBuilder sb = new StringBuilder(dist.ToString("N2"));
            var sb = dist.ToString("N2") + "AU";
            //sb.Append("AU");
            planet.planet.Indictaor.SetRenderer = planet.planet.Indictaor.OriginRenderer;
            planet.planet.distance = dist;
            planet.planet.placetDistanceText.text = sb;
        }
        IEnumerator disable = disablePlanets(list);
        StartCoroutine(disable);
        currentStage.planet.planetInfo.transform.position = Vector3.zero;
    }

    private IEnumerator disablePlanets(List<PlanetInfo> list)
    {
        yield return new WaitForFixedUpdate();
        foreach (var planet in list)
            planet.planet.stage.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {

    }
}
