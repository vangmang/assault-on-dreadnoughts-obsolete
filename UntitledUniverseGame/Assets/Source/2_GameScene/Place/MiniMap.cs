﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    //[SerializeField]
    //private List<WarpablePlace> warpablePlaces;
    //[SerializeField]
    [SerializeField] private List<Transform> warpablePlacesInEditor;
    [SerializeField] private float navigatingDistance;
    //public static readonly Vector3 MovableRange = new Vector3(5000f, 5000f, 5000f);    

    // Start is called before the first frame update
    void Start()
    {
        warpablePlacesInEditor.ForEach(place =>
        {
            var dir = place.localPosition.normalized;
            //Debug.Log(place.localPosition + " " + dir);
            place.localPosition = dir * navigatingDistance;

        });
    }
}
