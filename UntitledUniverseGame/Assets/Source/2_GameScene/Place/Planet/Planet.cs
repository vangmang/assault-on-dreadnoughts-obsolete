﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Planet : WarpablePlace
{
    public PlanetInfo planetInfo;
    public Transform planet; // 실질적인 행성
    public WarpTargetIndicator Indictaor { get; private set; }

    private void Start()
    {
        InitIndicator();
    }

    protected override void InitIndicator()
    {
        Indictaor = Instantiate(warpTargetIndicator, GameMng.Instance.WarpTargetIndicatorTransform).GetComponent<WarpTargetIndicator>();
        Indictaor.mainCamera = GameMng.Instance.MainCamera;
        Indictaor.target = transform;
        Indictaor.targetPlace = this;
    }
}

