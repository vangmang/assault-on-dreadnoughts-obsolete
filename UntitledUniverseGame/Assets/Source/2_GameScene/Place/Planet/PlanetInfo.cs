﻿using System.Collections.Generic;
using UnityEngine;

public class PlanetInfo : MonoBehaviour
{
    public Planet planet;
    /// <summary>
    /// 항성과의 거리 [단위 => AU]
    /// </summary>
    public float distanceWithStar;
    /// <summary>
    /// 함선과의 거리 [단위 => AU]
    /// </summary>
    public float distance;
    public Vector3 direction;
    public Vector3 OriginalPos { get; private set; }
    public Vector3 OriginalDireciton { get; private set; }

    // 행성이 보유한 워프 포인트
    public List<WarpPoint> possessedWarpPoints;

    private void Awake()
    {
        transform.position = direction * (distanceWithStar * 100000f);
        OriginalPos = transform.position;
        OriginalDireciton = direction;
    }
}