﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public Transform stage;
    public Transform planetTransform;
    public PlanetIndicatorAnchor IndicatorAnchor;
    public Planet planet;
    /// <summary>
    /// 현재 캐릭터 스테이지 여부
    /// </summary>
    public bool isCurrentStage;
}
