﻿using System.Collections;
using UnityEngine;
using UniRx;

public class Station : MonoBehaviour
{
    public Transform Core;
    public Transform Arrays;
    public WarpPoint currentWarpPoint;
    public GameObject stationIndicatorPrefab;
    public GameMng gameMng;
    private StationIndicator stationIndicator;
    [SerializeField] private Transform stationEntrance;
    
    IEnumerator Start()
    {
        yield return new WaitForFixedUpdate();
        transform.localPosition = (currentWarpPoint.currentPlanet.transform.position - currentWarpPoint.targetPlanet.transform.position).normalized * 70000f;
        gameMng = GameMng.Instance;
        stationIndicator = Instantiate(stationIndicatorPrefab, gameMng.IndicatorTransform).GetComponent<StationIndicator>();
        stationIndicator.target = stationEntrance;
        stationIndicator.mainCamera = gameMng.MainCamera;
        stationIndicator.station = this;

        this.ObserveEveryValueChanged(_ => gameMng.charStateMng.CurrentInfo.preLoadAfterWarp).
            Where(x => x == true).
            Subscribe(warpPoint =>
            {
                try
                {
                    if (((Planet)gameMng.charStateMng.Warp.targetedWarpTarget).Equals(currentWarpPoint.currentPlanet))
                    {
                        var sb = gameMng.charStateMng.Warp.targetedWarpTarget.stage.planet.ToString() + gameMng.charStateMng.CurrentInfo.cur_Stage.planet.ToString();
                        var checkCurrent = WarpablePlace.WarpPointTable[sb].Equals(currentWarpPoint);
                        gameObject.SetActive(checkCurrent);
                    }
                    else
                    {
                        gameObject.SetActive(true);
                    }
                }
                catch {
                    gameObject.SetActive(false);
                }
            });
    }     

    // Update is called once per frame
    void Update()
    {
        Core.Rotate(Core.forward, -0.5f * Time.deltaTime);
        Arrays.Rotate(Arrays.forward, 1f * Time.deltaTime);
    }
}
