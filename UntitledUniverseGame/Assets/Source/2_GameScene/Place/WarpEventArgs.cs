﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WarpEventArgs : EventArgs
{
    public WarpablePlace TargetPlace { get; private set; }

    public WarpEventArgs(WarpablePlace place)
    {
        TargetPlace = place;
    }
}
