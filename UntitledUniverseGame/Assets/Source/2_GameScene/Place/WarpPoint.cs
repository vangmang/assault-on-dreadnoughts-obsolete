﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using UniRx;


/// <summary>
/// 각 행성간 워프 포인트
/// </summary>
public class WarpPoint : WarpablePlace
{
    public enum _WARP_POINT_TYPE_
    {
        normalPoint,    // 일반 포인트 => 일반적인 전투가 이루어지는 포인트
        devilPoint,     // 악마 포인트 => 능력을 희생해서 패시브 하나를 영구히 지속시킬 수 있다. 
        /* NPC 개리 오쏜 드니코프 (Garry Othon Dnicov)가 나타난다. 그가 나타난 곳에 있는 모든 적들은 이미 전부 격파된 상태이다. 게임 도중 총 3번 나타난다. */
        deathPoint,     // 죽음 포인트 => 매우 어려운 보스를 잡고 희귀 패시브를 얻을 수 있다.
        stationPoint,   // 스테이션 포인트 => 상점, 정비 등 여러 활동을 할 수 있다.
        bossPoint,      // 보스 포인트 => 다음 성계로 가기 위해 무조건 들러야 하는, 보스가 존재하는 포인트
    }

    public Planet currentPlanet;
    public Planet targetPlanet;
    public bool explorationCheck { get; private set; }
    public bool currentPointCheck { get; private set; }

    public Transform planetTransform;
    private CharStateMng charStateMng;

    public _WARP_POINT_TYPE_ WarpPointType;
    public CreateEnemies createEnemies;
    public WarpTargetIndicator WarpIndicator { get; private set; }
    public Vector3 originalDirectionWithTarget { get; private set; }
    public Vector3 originalPosition { get; private set; }

    private void Awake()
    {
        charStateMng = CharStateMng.Instance;
        StarMap.Instance.WarpPoints.Add(this);
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForFixedUpdate();
        StringBuilder sb = new StringBuilder("[");
        sb.Append(currentPlanet.placetNameText.text).Append(" - ").Append(targetPlanet.placetNameText.text).Append("] warp point");
        placetNameText.text = sb.ToString();
        // 1 - 4 // 4 - 1 이런식으로 워프 포인트가 겹칠 수 있으므로 두 게임 오브젝트 아이디를 결합하는 것으로 결정
        StringBuilder id = new StringBuilder(currentPlanet.ToString());
        id.Append(targetPlanet.ToString());
        WarpPointTable.Add(id.ToString(), this);
        //yield return new WaitForFixedUpdate(); // 행성의 실질적인 포지션 할당 완료를 기다리기 위해 좀만 기다리자.
        originalDirectionWithTarget = (currentPlanet.planetInfo.transform.position - targetPlanet.planetInfo.transform.position).normalized;
        transform.localPosition = originalDirectionWithTarget * -ShipBase._WARP_.warpConstant;
        originalPosition = transform.localPosition;
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_WarpPoint).
            Subscribe(point =>
            {
                currentPointCheck = point.Equals(this);
                if (currentPointCheck)
                {
                    explorationCheck = true;
                    createEnemies.enableEnemyInstantiation = false;
                }
            });
        Observable.EveryUpdate().Subscribe(_ =>
        {
            display();
        }).AddTo(this);
        InitIndicator();
    }
    protected override void InitIndicator()
    {
        WarpIndicator = Instantiate(warpTargetIndicator, GameMng.Instance.WarpTargetIndicatorTransform).GetComponent<WarpTargetIndicator>();
        WarpIndicator.mainCamera = GameMng.Instance.MainCamera;
        WarpIndicator.target = transform;
        WarpIndicator.targetPlace = this;
        WarpIndicator.Assistant = this;        
    }

    private void display()
    {
        if(charStateMng.CurrentInfo.cur_WarpPoint.Equals(this))
        {
            var dist = (charStateMng.Info.ShipTransform.position - charStateMng.CurrentInfo.cur_WarpPoint.transform.position).magnitude;
            charStateMng.Warp.enableMicroWormholePenetration = dist <= 250f;
        }
    }

    private void OnDestroy()
    {

    }
}
