﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class WarpablePlace : MonoBehaviour
{
    [SerializeField] protected WarpTargetIndicator warpTargetIndicator;
    [SerializeField] protected ShipBase._WARP_.WARP_TYPE warpType;
    // 인스턴스 아이디의 빼기로 워프 포인트를 지정한다 
    /// <summary>
    /// 행성 인스턴스 아이디의 빼기로 현재 워프 포인트를 지정한다
    /// </summary>
    public static Dictionary<string, WarpPoint> WarpPointTable { get; private set; }
 
    public Text placetNameText;
    public Text placetDistanceText;
    public float distance; // 단위 AU 
    public Stage stage;
    public ShipBase._WARP_.WARP_TYPE WarpType { get { return warpType; } }

    private void Awake()
    {
        WarpPointTable = new Dictionary<string, WarpPoint>();
    }

    protected abstract void InitIndicator();

    private void OnDestroy()
    {
        WarpPointTable = null;
    }
}
