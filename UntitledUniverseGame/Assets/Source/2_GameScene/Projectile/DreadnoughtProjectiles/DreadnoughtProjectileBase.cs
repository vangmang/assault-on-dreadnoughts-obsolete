﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using UniRx;

public abstract class DreadnoughtProjectileBase : StateMng
{

    [Serializable]
    public struct _STAT_
    {
        [SerializeField] private float maxFlightTime;
        public float MaxFlightTime { get { return maxFlightTime; } }
    }

    [Serializable]
    public struct _CURRENT_INFO_
    {
        [NonSerialized] public float currentVelocity;
        public float currentFlightTime;

        public float damage;
    }
    [Serializable]
    public struct _INFO_
    {
        [SerializeField] private Transform projectileTransform;
        [SerializeField] private TrailRenderer projectileTrail;
        [SerializeField] private BoxCollider projectileCollider;
        [SerializeField] private MeshRenderer projectileMeshRenderer;
        [SerializeField] private ParticleSystem projectileParticle;
        public Transform ProjectileTransform { get { return projectileTransform; } }
        public TrailRenderer ProjectileTrail { get { return projectileTrail; } }
        public BoxCollider ProjectileCollider { get { return projectileCollider; } }
        public MeshRenderer ProjectileMeshRenderer { get { return projectileMeshRenderer; } }
        public ParticleSystem ProjectilePartilce { get { return projectileParticle; } }
    }

    public _STAT_ Stat;
    public _CURRENT_INFO_ CurrentInfo;
    public _INFO_ Info;
    public DreadnoughtWeaponBase DreadnoughtWeaponStateMng;
    public Vector3 additionalPosition;
    public Transform weaponLaunchTransform;
}
