﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IonCannonProjectileCollision : MonoBehaviour
{
    [SerializeField] private IonCannonProjectileStateMng projectileStateMng;
    private void OnCollisionEnter(Collision collision)
    {
        //if (!collision.gameObject.tag.Equals("Player"))
        //    Debug.Log(collision);
        //Debug.Log(collision.gameObject);
        projectileStateMng.StateTransition(IonCannonProjectileStateMng.ION_CANNON_PROJECTILE_STATES.hit, collision.gameObject);
    }
}
