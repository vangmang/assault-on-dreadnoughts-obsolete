﻿using System.Collections;
using UnityEngine;
using FiniteStateMachine;
using System;
using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;

public class IonCannonProjectileFiredState : State<IonCannonProjectileStateMng>
{
    public override Enum GetState
    {
        get { return IonCannonProjectileStateMng.ION_CANNON_PROJECTILE_STATES.fired; }
    }

    [Unity.Burst.BurstCompile]
    private struct StateExecutionJob : IJobParallelForTransform
    {
        public Vector3 direction;
        public float velocity;
        [ReadOnly]
        public float deltaTime;
        public void Execute(int index, TransformAccess transformAccess)
        {
            transformAccess.position += direction * velocity * deltaTime;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        var weaponTransform = o_Params.Length > 0 ? (Transform)o_Params[0] : instance.DreadnoughtWeaponStateMng.Info.WeaponTransform;
        var target = weaponTransform.position;
        if (o_Params.Length > 1)
            target += (Vector3)o_Params[1];
        instance.Info.ProjectileTransform.position = target + weaponTransform.forward * 25f;
        instance.Info.ProjectileTransform.rotation = weaponTransform.rotation;
        StartCoroutine(enableCollision());
        instance.Info.ProjectileTrail.gameObject.SetActive(true);
    }

    private IEnumerator enableCollision()
    {
        yield return new WaitForSeconds(0.025f);
        instance.Info.ProjectileCollider.enabled = true;
    }

    public override void Execute()
    {
        instance.CurrentInfo.currentFlightTime += Time.deltaTime;
        if (instance.CurrentInfo.currentFlightTime >= instance.Stat.MaxFlightTime)
            instance.StateTransition(IonCannonProjectileStateMng.ION_CANNON_PROJECTILE_STATES.idle);

        StateExecutionJob stateExecutionJob = new StateExecutionJob()
        {
            direction = instance.Info.ProjectileTransform.forward,
            velocity = instance.CurrentInfo.currentVelocity,
            deltaTime = Time.deltaTime
        };
        JobHandle jobHandle = stateExecutionJob.Schedule(instance.TransformAccessArray);
        jobHandle.Complete();
    }

    public override void Exit()
    {
    }
}
