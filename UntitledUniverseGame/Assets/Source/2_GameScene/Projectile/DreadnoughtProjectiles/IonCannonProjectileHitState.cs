﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class IonCannonProjectileHitState : State<IonCannonProjectileStateMng>
{
    public override Enum GetState
    {
        get { return IonCannonProjectileStateMng.ION_CANNON_PROJECTILE_STATES.hit; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.Info.ProjectileCollider.enabled = false;
        instance.StateTransition(IonCannonProjectileStateMng.ION_CANNON_PROJECTILE_STATES.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
