﻿using System.Collections;
using System.Collections.Generic;
using FiniteStateMachine;
using UnityEngine;
using System;

public class IonCannonProjectileIdleState : State<IonCannonProjectileStateMng>
{
    public override Enum GetState
    {
        get { return IonCannonProjectileStateMng.ION_CANNON_PROJECTILE_STATES.idle; }
    }
    public override void Enter(params object[] o_Params)
    {
        try
        {
            instance.CurrentInfo.currentFlightTime = 0f;
        }
        catch { }
        //instance.Info.ProjectileMeshRenderer.enabled = false;
        instance.Info.ProjectileCollider.enabled = false;
        instance.Info.ProjectileTrail.gameObject.SetActive(false);
        instance.Info.ProjectileTransform.gameObject.SetActive(false);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
