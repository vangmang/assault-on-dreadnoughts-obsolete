﻿using UnityEngine;
using UniRx;
using UnityEngine.Jobs;

public class IonCannonProjectileStateMng : DreadnoughtProjectileBase
{
    public enum ION_CANNON_PROJECTILE_STATES
    {
        idle,
        fired,
        hit
    }

    public Transform[] ParallelForTransform { get; private set; }
    public TransformAccessArray TransformAccessArray { get; private set; }


    private void Awake()
    {
        InitializeState();
        ParallelForTransform = new Transform[]
{
            Info.ProjectileTransform
};
        TransformAccessArray = new TransformAccessArray(ParallelForTransform);
        CurrentInfo.currentVelocity = DreadnoughtWeaponStateMng.Stat.maxVelocity;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => DreadnoughtWeaponStateMng.Stat.damage).
            Subscribe(_ =>
            {
                CurrentInfo.damage = DreadnoughtWeaponStateMng.Stat.damage;
            });
        StateEnter(ION_CANNON_PROJECTILE_STATES.fired, weaponLaunchTransform, additionalPosition);
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
    }

    private void OnDestroy()
    {
        TransformAccessArray.Dispose();
    }
}
