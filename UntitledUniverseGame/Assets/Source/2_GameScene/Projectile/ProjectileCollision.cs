﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileCollision : MonoBehaviour
{
    [SerializeField] private ProjectileStateMng projectileStateMng;

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(projectileStateMng.Info.ProjectileTransform.position, projectileStateMng.Info.ProjectileTransform.forward, out hit, 30f))
        {
            if(hit.collider.gameObject.tag.Equals("Solid"))
                ricochet(hit.point, hit.normal);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Solid"))
        {
            ricochet(collision.GetContact(0).point, collision.GetContact(0).normal);
            return;
        }
        else if (collision.gameObject.tag.Equals("Projectile"))
            return;
        projectileStateMng.StateTransition(ProjectileStateMng.PROJECTILE_STATES.hit, collision.gameObject);
        collision.gameObject.SendMessage("TakeDamage", projectileStateMng, SendMessageOptions.DontRequireReceiver);
    }

    private void ricochet(Vector3 _point, Vector3 _normal)
    {
        var incoming = _point - projectileStateMng.CurrentInfo.firedPosition;
        var normal = _normal;
        var target = Vector3.Reflect(incoming, normal);
        var lookRot = Quaternion.LookRotation(target);
        projectileStateMng.Info.ProjectileTransform.rotation = lookRot;
        if (projectileStateMng.WeaponStateMng.getShipBase.Equals(CharStateMng.Instance))
            ((CharStateMng)projectileStateMng.WeaponStateMng.getShipBase).CharacterFeatureInfo.ricochetEffectPlayer.PlayRicochetEffectAudio();
    }
}