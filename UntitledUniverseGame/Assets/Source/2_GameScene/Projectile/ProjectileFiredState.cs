﻿using System.Collections;
using UnityEngine;
using FiniteStateMachine;
using System;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Jobs;

public class ProjectileFiredState : State<ProjectileStateMng>
{
    public override Enum GetState
    {
        get { return ProjectileStateMng.PROJECTILE_STATES.fired; }
    }

    [Unity.Burst.BurstCompile]
    private struct StateExecutionJob : IJobParallelForTransform
    {
        public Vector3 direction;
        public float velocity;
        [ReadOnly]
        public float deltaTime;
        public void Execute(int index, TransformAccess transformAccess)
        {
            transformAccess.position += direction * velocity * deltaTime;
        }
    }

    //private TransformAccessArray transformAccessArray;
    //private Transform[] transforms;

    public override void Enter(params object[] o_Params)
    {
        instance.Info.ProjectileTransform.position = instance.WeaponStateMng.Info.WeaponTransform.position + instance.WeaponStateMng.Info.WeaponTransform.forward * 25f;
        instance.CurrentInfo.firedPosition = instance.Info.ProjectileTransform.position;
        instance.Info.ProjectileTransform.rotation = instance.WeaponStateMng.Info.WeaponTransform.rotation;
        instance.Info.ProjectileMeshRenderer.enabled = true;
        //StartCoroutine(enableCollision());
        //Invoke("enableCollision", 0.025f);
        instance.Info.ProjectileCollider.enabled = true;
        instance.Info.ProjectileTrail.gameObject.SetActive(true);
        var color = instance.Info.ProjectileTrail.startColor;
        color.a = 1f;
        instance.Info.ProjectileTrail.startColor = color;
    }

    public override void Execute()
    {
        instance.CurrentInfo.currentFlightTime += Time.deltaTime;
        if (instance.CurrentInfo.currentFlightTime >= 0.025f)
            instance.Info.ProjectileTrail.time = 0.1f;

        if (instance.CurrentInfo.currentFlightTime >= instance.Stat.MaxFlightTime)
            instance.StateTransition(ProjectileStateMng.PROJECTILE_STATES.idle);

        StateExecutionJob stateExecutionJob = new StateExecutionJob()
        {
            direction = instance.Info.ProjectileTransform.forward,
            velocity = instance.CurrentInfo.currentVelocity,
            deltaTime = Time.deltaTime
        };
        JobHandle jobHandle = stateExecutionJob.Schedule(instance.TransformAccessArray);
        jobHandle.Complete();
        //transformAccessArray.Dispose();
    }

    public override void Exit()
    {
    }
}
