﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class ProjectileHitState : State<ProjectileStateMng>
{
    public override Enum GetState
    {
        get { return ProjectileStateMng.PROJECTILE_STATES.hit; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.Info.ProjectileMeshRenderer.enabled = false;
        instance.Info.ProjectileCollider.enabled = false;
        instance.Info.ProjectilePartilce.Play();
        instance.SpecialAttackHitEffect?.Invoke(o_Params);
        StartCoroutine(waitUntilParticlePlayingIsDone());
    }

    public override void Execute()
    {
    }

    private IEnumerator waitUntilParticlePlayingIsDone()
    {
        yield return new WaitUntil(() => !instance.Info.ProjectilePartilce.isPlaying);
        instance.StateTransition(ProjectileStateMng.PROJECTILE_STATES.idle);
    }

    public override void Exit()
    {
    }
}
