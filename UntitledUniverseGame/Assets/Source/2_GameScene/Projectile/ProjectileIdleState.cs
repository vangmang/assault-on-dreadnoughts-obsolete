﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class ProjectileIdleState : State<ProjectileStateMng>
{
    public override Enum GetState
    {
        get
        {
            return ProjectileStateMng.PROJECTILE_STATES.idle;
        }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.CurrentInfo.currentFlightTime = 0f;
        instance.CurrentInfo.damage = instance.WeaponStateMng.Stat.damage;
        instance.Info.ProjectileMeshRenderer.enabled = false;
        instance.Info.ProjectileCollider.enabled = false;
        instance.Info.ProjectileTrail.startColor = new Color(1f, 0.2381402f, 0f, 1f);
        instance.Info.ProjectileTrail.endColor = new Color(1f, 0.2381402f, 0f, 1f);
        instance.Info.ProjectileTrail.time = 0f; //0.1f
        instance.Info.ProjectileTrail.startWidth = 5f;
        instance.Info.ProjectileTrail.endWidth = 5f;
        var color = instance.Info.ProjectileTrail.startColor;
        color.a = 1f;
        instance.Info.ProjectileTrail.startColor = color; 
        if(instance.Info.ProjectilieSubTrail.gameObject.activeSelf)
            instance.Info.ProjectilieSubTrail.gameObject.SetActive(false);
        instance.Info.ProjectileTransform.gameObject.SetActive(false);
        //instance.Info.ProjectileTrail.emitting = false;
        instance.SpecialAttackHitEffect = null;
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
