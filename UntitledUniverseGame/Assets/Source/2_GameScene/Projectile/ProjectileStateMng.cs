﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using UniRx;
using UnityEngine.Jobs;

public class ProjectileStateMng : StateMng
{
    public enum PROJECTILE_STATES
    {
        idle,
        fired,
        hit
    }

    [Serializable]
    public struct _STAT_
    {
        [SerializeField] private float maxFlightTime;
        public float MaxFlightTime { get { return maxFlightTime; } }
    }

    [Serializable]
    public struct _CURRENT_INFO_
    {
        [NonSerialized] public float currentVelocity;
        public float currentFlightTime;
        public Vector3 firedPosition;
        public float damage;
    }
    [Serializable]
    public struct _INFO_
    {
        [SerializeField] private Transform projectileTransform;
        [SerializeField] private TrailRenderer projectileTrail;
        [SerializeField] private TrailRenderer projectileSubTrail; 
        [SerializeField] private BoxCollider projectileCollider;
        [SerializeField] private MeshRenderer projectileMeshRenderer;
        [SerializeField] private ParticleSystem projectileParticle;
        public Transform ProjectileTransform { get { return projectileTransform; } }
        public TrailRenderer ProjectileTrail { get { return projectileTrail; } }
        public TrailRenderer ProjectilieSubTrail { get { return projectileSubTrail; } }
        public BoxCollider ProjectileCollider { get { return projectileCollider; } }
        public MeshRenderer ProjectileMeshRenderer { get { return projectileMeshRenderer; } }
        public ParticleSystem ProjectilePartilce { get { return projectileParticle; } }
    }

    public _STAT_ Stat;
    public _CURRENT_INFO_ CurrentInfo;
    public _INFO_ Info;
    public WeaponBase WeaponStateMng;
    public delegate void _SpecialAttackHitEffect_(params object[] o_Params);
    public _SpecialAttackHitEffect_ SpecialAttackHitEffect;
    public Transform[] ParallelForTransform { get; private set; }
    public TransformAccessArray TransformAccessArray { get; private set; }

    private void Awake()
    {
        InitializeState();
        ParallelForTransform = new Transform[]
        {
            Info.ProjectileTransform
        };
        TransformAccessArray = new TransformAccessArray(ParallelForTransform);
        CurrentInfo.damage = WeaponStateMng.Stat.damage;
    }

    // Start is called before the first frame update
    private void Start()
    {
        this.ObserveEveryValueChanged(_ => WeaponStateMng.Stat.damage).
            Subscribe(_ =>
            {
                CurrentInfo.damage = WeaponStateMng.Stat.damage;
            });

        CurrentInfo.currentVelocity = WeaponStateMng.Stat.velocity;
        StateEnter(PROJECTILE_STATES.fired);
    }

    private void Update()
    {
        Execute();
    }

    private void OnDestroy()
    {
        TransformAccessArray.Dispose();
    }
}
