﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class CharAfterburnerState : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.afterburner; }
    }

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
        instance.CurrentInfo.isPowerRecoveryEnable = false;
    }

    public override void Execute()
    {
        instance.MainCamera.fieldOfView = Mathf.Lerp(instance.MainCamera.fieldOfView, 75f, instance.Stat.acceleration * Time.deltaTime);
        var wantedRot = instance.MainCamera.transform.rotation.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
        instance.Info.ShipTransform.rotation =
            Quaternion.Lerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
        instance.CurrentInfo.cur_rotation = instance.MainCamera.transform.rotation;
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.AfterburnerVelocity, instance.Stat.acceleration * Time.deltaTime);
        // 이동
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;

        instance.PlayThrusterEffects(1.2f, 17f, 0.75f, 1f);
        instance.PlaySailEffect();

        instance.CurrentInfo.cur_power -= instance.ConsumableStat.AfterburnerConsumption * 10f * Time.deltaTime;
        instance.ResetPowerRecovery();
        if (instance.CurrentInfo.cur_power <= 0f)
            instance.StateTransition(Input.GetKey(KeyCode.W) ? CharStateMng.CHARACTER_STATES.move : CharStateMng.CHARACTER_STATES.idle);
    }

    public override void Exit()
    {
    }
}
