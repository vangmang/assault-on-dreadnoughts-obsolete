﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class CharDownState : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.down; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.Info.ShipCollider.enabled = false;
        StartCoroutine(waitForParticlePlaying());
    }

    private IEnumerator waitForParticlePlaying()
    {
        var particleHalfLength = instance.Info.ShipExplosionParticle.main.duration * 0.05f;
        yield return new WaitForSeconds(particleHalfLength);
        instance.Info.ShipExplosionParticle.transform.SetParent(null);
        instance.Info.ShipTransform.parent.gameObject.SetActive(false);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
