﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

// 일반 회피
public class CharEvacuationState : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.evacuation; }
    }

    private bool dirCheck;
    private Vector3 dir;

    public override void Enter(params object[] o_Params)
    {
        //instance.StopThrusterParticles();
        dirCheck = (int)o_Params[0] > 0;
        dir = dirCheck ? instance.Info.ShipTransform.right : -instance.Info.ShipTransform.right;
        instance.CurrentInfo.cur_power -= instance.ConsumableStat.EvasionConsumpion;
        instance.CurrentInfo.isPowerRecoveryEnable = false;
        instance.ResetPowerRecovery();
        var evade = invokeEvade(dir);
        StartCoroutine(evade);
    }

    private IEnumerator invokeEvade(Vector3 dir)
    {
        float t = 0f;
        float rev = 1f;
        var rot = instance.Info.ShipRigidbody.rotation.eulerAngles;
        var rotDir = dirCheck ? -1f : 1f;
        var _dir = rotDir < 0 ? instance.Info.ShipTransform.right : -instance.Info.ShipTransform.right;
        var start = instance.Info.ShipTransform.position;
        var end = instance.Info.ShipTransform.position + _dir * instance.Stat.evacutationDistance;

        // 구르는 동안 무적
        while (t <= 0.9f)
        {
            rev = Mathf.Lerp(1f, 0f, t);
            t += 3f * rev * Time.deltaTime;
            rot.z = rotDir * 360f * t;
            instance.Info.ShipTransform.rotation = Quaternion.Euler(rot);
            instance.Info.ShipTransform.position = Vector3.Lerp(start, end, t);
            yield return null;
        }
        instance.StateTransition(Input.GetKey(KeyCode.W) ? CharStateMng.CHARACTER_STATES.move : CharStateMng.CHARACTER_STATES.idle);
    }

    public override void Execute()
    {
        instance.MainCamera.fieldOfView = Mathf.Lerp(instance.MainCamera.fieldOfView, 60f, instance.Stat.acceleration * Time.deltaTime);
        instance.CurrentInfo.cur_velocity = Mathf.Lerp(instance.CurrentInfo.cur_velocity, 0f, instance.Stat.acceleration * Time.deltaTime);
        // 이동
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
        instance.PlayThrusterEffects(0f, 0f, 0f, 0f);
        instance.PlaySailEffect();
    }

    public override void Exit()
    {
    }
}
