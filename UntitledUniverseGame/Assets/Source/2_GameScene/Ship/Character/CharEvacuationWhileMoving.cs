﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using UniRx;

// 이동 중에 회피
public class CharEvacuationWhileMoving : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.evacuationWhileMoving; }
    }

    private bool dirCheck;
    private Vector3 dir;
    private Quaternion currentRot;
    
    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
        dirCheck = (int)o_Params[0] > 0;
        dir = dirCheck ? instance.Info.ShipTransform.right : -instance.Info.ShipTransform.right;
        instance.CurrentInfo.cur_power -= instance.ConsumableStat.EvasionConsumpion;
        instance.CurrentInfo.isPowerRecoveryEnable = false;
        instance.ResetPowerRecovery();
        var evade = invokeEvade(dir);
        StartCoroutine(evade);
    }

    private IEnumerator invokeEvade(Vector3 dir)
    {
        float t = 0f;
        float rev = 1f;
        var rot = instance.Info.ShipRigidbody.rotation.eulerAngles;
        var rotDir = dirCheck ? -1f : 1f;
        var _dir = rotDir < 0 ? instance.Info.ShipTransform.right : -instance.Info.ShipTransform.right;
        var start = instance.Info.ShipTransform.position;
        var end = instance.Info.ShipTransform.position + _dir * instance.Stat.evacutationDistance;

        while (t <= 0.9f)
        {
            rev = Mathf.Lerp(1f, 0f, t);
            t += 3f * rev * Time.deltaTime;
            rot.z = rotDir * 360 * t;
            instance.Info.ShipTransform.rotation = Quaternion.Euler(rot);
            end += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
            instance.Info.ShipTransform.position = Vector3.Lerp(start, end, t);
            yield return null;
        }
        instance.StateTransition(Input.GetKey(KeyCode.W) ? CharStateMng.CHARACTER_STATES.move : CharStateMng.CHARACTER_STATES.idle);
    }

    public override void Execute()
    {
        instance.MainCamera.fieldOfView = Mathf.Lerp(instance.MainCamera.fieldOfView, 70f, instance.Stat.acceleration * Time.deltaTime);
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity * 0.5f, instance.Stat.acceleration * Time.deltaTime);
        // 이동
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;

        var wantedRot = instance.MainCamera.transform.rotation.eulerAngles;
        currentRot = Quaternion.Slerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);

        instance.PlayThrusterEffects(1f, 13f, 0.25f, 1f);
        instance.PlaySailEffect();
    }

    public override void Exit()
    {
    }
}
