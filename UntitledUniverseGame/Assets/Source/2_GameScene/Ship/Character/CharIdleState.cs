﻿using FiniteStateMachine;
using System;
using UnityEngine;

public class CharIdleState : State<CharStateMng>
{
    public override Enum GetState => CharStateMng.CHARACTER_STATES.idle; 

    public override void Enter(params object[] o_Params)
    {
        //instance.StopThrusterParticles();
    }

    public override void Execute()
    {
        instance.MainCamera.fieldOfView = Mathf.Lerp(instance.MainCamera.fieldOfView, 60f, instance.Stat.acceleration * Time.deltaTime);
        var wantedRot = instance.CurrentInfo.cur_rotation.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
        instance.Info.ShipTransform.rotation =
            Quaternion.Slerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, 0f, instance.Stat.acceleration * Time.deltaTime);
        // 이동
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
        instance.PlayThrusterEffects(0f, 0f, 0f, 0f);
        instance.PlaySailEffect();
    }


    public override void Exit()
    {
    }
}
