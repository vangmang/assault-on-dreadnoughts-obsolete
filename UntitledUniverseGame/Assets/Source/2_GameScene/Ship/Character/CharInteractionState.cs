﻿using FiniteStateMachine;
using System;
using UnityEngine;

public class CharInteractionState : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.interact; }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }

}
