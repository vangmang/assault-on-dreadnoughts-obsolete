﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using System.Text;

public class CharMicroWormholePenetrateState : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.microWormholePenetate; }
    }

    private WarpPoint target;
    private Quaternion recentRot;
    private bool wormholeEnterCheck;

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();

        target = (WarpPoint)instance.Warp.currentWarpTarget;
        instance.Warp.targetedWarpTarget = target;
        instance.Warp.MicroWormholePenetrationBeginAudio.Play();
        wormholeEnterCheck = false;
        instance.CharacterFeatureInfo.StatusText.text = "MICRO WORMHOLE PENETRATION";
        //StringBuilder sb = new StringBuilder("Destination: ");
        //sb.Append(target.placetNameText.text);
        var sb = "Destination" + target.placetNameText.text;
        instance.CharacterFeatureInfo.WarpTargetText.text = sb;
        instance.CurrentInfo.isWarpin = true;
        instance.CurrentInfo.preLoadAfterWarp = false;
        StartCoroutine(aligning());
        EnemyManager.Instance.ClearTargets();
    }

    private IEnumerator aligning()
    {
        float t = 0f;
        float fov = instance.MainCamera.fieldOfView;
        float wormholeEnter = instance.Warp.MicroWormholePenetrationAlignTime - 2.2f;
        while (t <= instance.Warp.MicroWormholePenetrationAlignTime)
        {
            t += Time.deltaTime;
            instance.MainCamera.fieldOfView = Mathf.Lerp(fov, 70f, t * 0.2f);
            if (t >= wormholeEnter && !wormholeEnterCheck)
            {
                StartCoroutine(playCanvasImageFadeEffect());
                wormholeEnterCheck = true;
                instance.Warp.MicroWormholePenetrationEnterAudio.Play();
            }
            var yRot = instance.Info.ShipRigidbody.rotation.eulerAngles.y - recentRot.eulerAngles.y;
            var targetRot = target.transform.position - instance.Info.ShipRigidbody.position;
            var lookRot = Quaternion.LookRotation(targetRot, instance.Info.ShipTransform.up);
            var wantedRot = lookRot.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
            if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(instance.Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
                wantedRot.z = 0f;
            instance.Info.ShipTransform.rotation =
                Quaternion.Lerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
            instance.CurrentInfo.cur_rotation = instance.MainCamera.transform.rotation;
            instance.CurrentInfo.cur_velocity =
                Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity, instance.Stat.acceleration * Time.deltaTime);
            // 이동
            instance.Info.ShipRigidbody.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
            instance.PlayThrusterEffects(1f, 13f, 0.75f, 1f);
            instance.PlaySailEffect();
            recentRot = instance.Info.ShipRigidbody.rotation;
            yield return null;
        }
        GC.Collect();
        instance.CurrentInfo.cur_velocity = 0f;
        var dir =
            (instance.CurrentInfo.cur_Stage.planet.planetInfo.transform.position - target.targetPlanet.planetInfo.transform.position).normalized;
        instance.CurrentInfo.cur_Stage.planetTransform.position = dir * ShipBase._WARP_.warpConstant;
        instance.CurrentInfo.cur_rotation = instance.Info.ShipRigidbody.rotation;
        instance.CurrentInfo.cur_WarpPoint = target;
        instance.StateTransition(CharStateMng.CHARACTER_STATES.idle);
    }

    private IEnumerator playCanvasImageFadeEffect()
    {
        float t = 0f;
        yield return new WaitForSeconds(1.2f);
        while(t <= 1f)
        {
            t += Time.deltaTime;
            GameMng.Instance.MicroWormholeFadeImage.color = new Color(1f, 1f, 1f, t);
            yield return null;
        }
        t = 1f;
        while(t >= 0f)
        {
            t -= 3.33f * Time.deltaTime;
            GameMng.Instance.MicroWormholeFadeImage.color = new Color(1f, 1f, 1f, t);
            yield return null;
        }
        GameMng.Instance.MicroWormholeFadeImage.color = new Color(1f, 1f, 1f, 0f);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        instance.CurrentInfo.preLoadAfterWarp = true;
        instance.CurrentInfo.isWarpin = false;
        instance.CharacterFeatureInfo.StatusText.text = "";
        instance.CharacterFeatureInfo.WarpTargetText.text = "";
        EnemyManager.Instance.targets.Add(instance);
    }
}
