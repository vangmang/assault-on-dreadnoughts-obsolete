﻿using FiniteStateMachine;
using System;
using UnityEngine;

public class CharMoveState : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.move; }
    }

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
    }

    public override void Execute()
    {
        instance.MainCamera.fieldOfView = Mathf.Lerp(instance.MainCamera.fieldOfView, 70f, instance.Stat.acceleration * Time.deltaTime);
        var wantedRot = instance.MainCamera.transform.rotation.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
        instance.Info.ShipTransform.rotation =
            Quaternion.Slerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
        instance.CurrentInfo.cur_rotation = instance.MainCamera.transform.rotation;
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity, instance.Stat.acceleration * Time.deltaTime);
        // 이동
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
        instance.PlayThrusterEffects(1f, 13f, 0.75f, 1f);
        instance.PlaySailEffect();
    }

    public override void Exit()
    {
    }
}
