﻿using System;
using System.Collections;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using System.Linq;

public class CharStateMng : ShipBase
{
    private CharStateMng() { }

    private static CharStateMng instance = null;
    public static CharStateMng Instance
    {
        get
        {
            return instance;
        }
    }

    public enum CHARACTER_STATES
    {
        idle,
        move,
        warp,
        microWormholePenetate,
        interact,
        evacuation,
        evacuationWhileMoving,
        afterburner,
        down
    }

    [Serializable]
    public struct _CHARACTER_FEATURE_INFO_
    {
        public Transform targetingAlertTransform;
        public HitEffectPlayer hitEffectPlayer;
        public RicochetEffectPlayer ricochetEffectPlayer;
        public Animation hitEffectTextAnim;
        public CharTargetingModuleDisplayer charTargetingModuleDisplayer;
        public AudioSource targetingAlertAudio;
        public AudioSource warpDeniedAudio;
        public AudioSource shieldRecoveryChargingAudio;
        public Image optimalRangeImage;
        public Image fallOffRangeImage;
        public Image optimalZoneImage;
        public Image Display500mImage;
        public Transform optimalRangeTransform;
        public Transform fallOffRangeTransform;
        public Transform optimalZoneTransform;
        public Transform Display500mTransform;
        public Text StatusText;
        public Text WarpTargetText;
        public Text WarpTimeLeftText;
        public Text MessageText;
        public Transform LauncherParentTransform;
        public Transform ActiveFormParentTransform;
        public CooldownDisplayer cooldownDisplayer;
        public Indicator characterPointIndicator; // 실제로 표시되는 인디케이터는 아니고 적들 어디있는지 구분해주는 숨겨진 인디케이터
    }

    [Serializable]
    public struct _CHARACTER_UPGRADE_
    {
        public const float velocity = 20f;                 // 400
        /// <summary>
        /// 0.5+ 해줘야함
        /// </summary>
        public const float acceleration = 0.05f;           // 0.5
        /// <summary>
        /// 0.5+ 해줘야함
        /// </summary>
        public const float mobility = 0.1f;         // 1.5
        public const float power = 10f;                    // 100
        public const float shieldCapacity = 20f;           // 200
        /// <summary>
        /// 3000+ 해줘야함
        /// </summary>
        public const float weaponRange = 100f;     // 4000
        /// <summary>
        /// 5+ 해줘야함
        /// </summary>
        public const float weaponDamage = 1f;         // 15
        /// <summary>
        /// 0.6- 해줘야함
        /// </summary>
        public const float weaponFireRate = 0.02f;   // 0.4

        public StatDisplayer velocityDisplayer;
        public StatDisplayer accelerationDisplayer;
        public StatDisplayer mobilityDisplayer;
        public StatDisplayer powerDisplayer;
        public StatDisplayer shieldCapacityDisplayer;
        public StatDisplayer weaponRangeDisplayer;
        public StatDisplayer weaponDamageDisplayer;
        public StatDisplayer weaponFireRateDisplayer;
    }
    public _CHARACTER_UPGRADE_ CharacterUpgradeConstant;

    public Camera MainCamera;
    public CharacterBuildMng charBuildMng;
    public int currentShieldRecoveryCount;

    [NonSerialized] public float zDirection;
    [SerializeField] private float weaponLimitElevation;
    private Quaternion recentRot;
    private int mapLimitCheck; // 0: 노멀 1: 위험 2: 매우 위험 3: GG
    private bool isAboutToWarpDriveActive;
    private DateTimeOffset lastRecovery;

    public _CHARACTER_FEATURE_INFO_ CharacterFeatureInfo;
    public const float PlayerMapLimit = 59000f;

    protected override void InitOnAwake()
    {
        InitializeState();
        instance = this;
        TargetableTransform = Info.ShipTransform;
        // 공격 가능 각도 초기화
        var dist = Targeting.fallOffRange;
        WeaponLimitElevation = weaponLimitElevation;
        setRangeDisplayerImage(CharacterFeatureInfo.fallOffRangeImage, CharacterFeatureInfo.fallOffRangeTransform, WeaponLimitElevation, dist);
        setRangeDisplayerImage(CharacterFeatureInfo.optimalRangeImage, CharacterFeatureInfo.optimalRangeTransform, WeaponLimitElevation, dist);
        setRangeDisplayerImage(CharacterFeatureInfo.optimalZoneImage, CharacterFeatureInfo.optimalZoneTransform, WeaponLimitElevation, dist);
        setRangeDisplayerImage(CharacterFeatureInfo.Display500mImage, CharacterFeatureInfo.Display500mTransform, WeaponLimitElevation, dist);

        // velocity
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.velocity).
            Subscribe(_ =>
            {
                Stat.velocity = CharacterUpgradeConstant.velocityDisplayer.stat = _CHARACTER_UPGRADE_.velocity * UpgradeTimesStat.velocity + 200f;
            });
        // acceleration
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.acceleration).
            Subscribe(_ =>
            {
                Stat.acceleration = CharacterUpgradeConstant.accelerationDisplayer.stat = _CHARACTER_UPGRADE_.acceleration * UpgradeTimesStat.acceleration + 0.5f;
            });
        // mobility
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.mobility).
            Subscribe(_ =>
            {
                Stat.mobility = CharacterUpgradeConstant.mobilityDisplayer.stat = _CHARACTER_UPGRADE_.mobility * UpgradeTimesStat.mobility + 0.5f;
            });
        // power
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.power).
            Subscribe(_ =>
            {
                CurrentInfo.cur_power =
                Stat.power = CharacterUpgradeConstant.powerDisplayer.stat = _CHARACTER_UPGRADE_.power * UpgradeTimesStat.power;
            });
        // shieldCapacity
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.shieldCapacity).
            Subscribe(_ =>
            {
                CurrentInfo.cur_ShieldCapacity =
                Stat.shieldCapacity = CharacterUpgradeConstant.shieldCapacityDisplayer.stat = _CHARACTER_UPGRADE_.shieldCapacity * UpgradeTimesStat.shieldCapacity;
            });
        // weaponRange
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.weaponRange).
            Subscribe(_ =>
            {
                Stat.weaponRange = CharacterUpgradeConstant.weaponRangeDisplayer.stat = _CHARACTER_UPGRADE_.weaponRange * UpgradeTimesStat.weaponRange + 3000f;
            });
        // weaponDamage
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.weaponDamage).
            Subscribe(_ =>
            {
                Stat.weaponDamage = CharacterUpgradeConstant.weaponDamageDisplayer.stat = _CHARACTER_UPGRADE_.weaponDamage * UpgradeTimesStat.weaponDamage + 5f;
            });
        // weaponFireRate
        this.ObserveEveryValueChanged(_ => UpgradeTimesStat.weaponFireRate).
            Subscribe(_ =>
            {
                Stat.weaponFireRate = CharacterUpgradeConstant.weaponFireRateDisplayer.stat = 0.6f - _CHARACTER_UPGRADE_.weaponFireRate * UpgradeTimesStat.weaponFireRate;
            });
        Stat = this + SavedData.SavedDataContainer.UpgradeTimesStat;
        UpgradeTimesStat += SavedData.SavedDataContainer.UpgradeTimesStat;
    }

    private void setRangeDisplayerImage(Image image, Transform imageTransform, float theta, float maxDistance)
    {
        var dist = imageTransform.localPosition.z;
        var r = Mathf.Max(theta * (dist / maxDistance) * Mathf.Rad2Deg / 2f, 50f);
        image.rectTransform.sizeDelta = new Vector2(r, r);
    }

    public void SetCurrentStage()
    {
        StartCoroutine(invokeSetCurrentStage());
    }

    private IEnumerator invokeSetCurrentStage()
    {
        CurrentInfo.cur_Stage.isCurrentStage = true;
        yield return new WaitUntil(() => CurrentInfo.cur_Stage.planet.Indictaor != null);
        CurrentInfo.cur_Stage.planet.Indictaor.target = CurrentInfo.cur_Stage.IndicatorAnchor.transform;
        CurrentInfo.cur_Stage.planet.Indictaor.SetRenderer = CurrentInfo.cur_Stage.IndicatorAnchor.renderer; // 인디케이터 앵커의 렌더러
        CurrentInfo.cur_Stage.planet.placetDistanceText.text = "Current Location";
        var sunRot = Quaternion.LookRotation(CurrentInfo.cur_Stage.planet.planetInfo.OriginalDireciton, CurrentInfo.cur_Stage.planet.planetInfo.transform.up);
        GameMng.Instance.SetDirectionalLightRotation(sunRot);
        StarMap.Instance.SetStarMap();
    }

    private IEnumerator enableMiscStuff()
    {
        yield return new WaitUntil(() => gameMng.fadeImage.Init);
        Info.ShipCollider.enabled = true;
        CharacterFeatureInfo.targetingAlertAudio.volume = 1f;
    }

    /// <summary>
    /// 기본형 회복
    /// </summary>
    public void RecoveryShield()
    {
        currentShieldRecoveryCount--;
        IEnumerator recovery = invokeRecoveryShield(Stat.shieldCapacity * 0.33f);
        StartCoroutine(recovery);
    }

    public void RecoveryShield(float amount)
    {
        IEnumerator recovery = invokeRecoveryShield(amount);
        StartCoroutine(recovery);
    }

    private IEnumerator invokeRecoveryShield(float amount)
    {
        CharacterFeatureInfo.shieldRecoveryChargingAudio.Play();
        float recoveryAmount = amount;
        for (int i = 0; i < 30f; i++)
        {
            if (CurrentInfo.cur_ShieldCapacity >= Stat.shieldCapacity)
            {
                CurrentInfo.cur_ShieldCapacity = Stat.shieldCapacity;
                yield break;
            }
            CurrentInfo.cur_ShieldCapacity += recoveryAmount / 30f;
            yield return new WaitForSeconds(0.0025f);
        }
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        StartCoroutine(enableMiscStuff());
        currentShieldRecoveryCount = 3; // 초기는 3으로
        StateEnter(CHARACTER_STATES.idle);
        yield return new WaitUntil(() => CurrentInfo.cur_WarpPoint);
        var planetDir = (CurrentInfo.cur_WarpPoint.currentPlanet.planetInfo.OriginalPos - CurrentInfo.cur_WarpPoint.targetPlanet.planetInfo.OriginalPos).normalized;
        CurrentInfo.cur_Stage.planet.planet.position = planetDir * _WARP_.warpConstant;

        recentRot = Info.ShipRigidbody.rotation;
        SetCurrentStage();
        CurrentInfo.cur_rotation = MainCamera.transform.rotation;
        // 런처 
        Observable.EveryUpdate().
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => Input.GetMouseButton(1)).
            Where(_ => CurrentInfo.launcherFormAbility).
            Where(_ => CurrentInfo.launcherFormAbility.usableCount > 0).
            Where(_ => CurrentInfo.launcherFormAbility.LaunchAble).
            Timestamp().
            Where(x => x.Timestamp > CurrentInfo.launcherFormAbility.lastLaunched.AddSeconds(CurrentInfo.launcherFormAbility.Cooldown)).
            Subscribe(x =>
            {
                CurrentInfo.launcherFormAbility.CooldownInvoker = CharacterFeatureInfo.cooldownDisplayer.InvokeCooldown;
                CurrentInfo.launcherFormAbility.Launch();
                CurrentInfo.launcherFormAbility.lastLaunched = x.Timestamp;
            }).AddTo(this);
        // 활성형모듈
        Observable.EveryUpdate().
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => Input.GetKey(KeyCode.LeftShift)).
            Where(_ => Input.GetMouseButton(1)).
            Where(_ => CurrentInfo.activeFormAbility).
            Where(_ => !CurrentInfo.activeFormAbility.Activation).
            Subscribe(_ =>
            {
                CurrentInfo.activeFormAbility.EnableActivationForm(this);
            }).AddTo(this);
        Observable.EveryUpdate().
            Where(_ => Input.GetKeyDown(KeyCode.R)). //회복
            Where(_ => CurrentInfo.cur_ShieldCapacity < Stat.shieldCapacity).
            Where(_ => currentShieldRecoveryCount > 0).
            Timestamp().
            Where(x => x.Timestamp > lastRecovery.AddSeconds(1f)).
            Subscribe(x =>
            {
                RecoveryShield();
                lastRecovery = x.Timestamp;
            }).AddTo(this);
        Observable.EveryUpdate().   // 이동
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.warp)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.microWormholePenetate)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.evacuation)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.evacuationWhileMoving)).
            Where(_ => Input.GetKeyDown(KeyCode.W)).
            Subscribe(_ => StateTransition(CHARACTER_STATES.move)).AddTo(this);
        Observable.EveryUpdate().
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.warp)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.microWormholePenetate)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.evacuation)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.evacuationWhileMoving)).
            Where(_ => Input.GetKeyUp(KeyCode.W)).
            Subscribe(_ => StateTransition(CHARACTER_STATES.idle)).AddTo(this);
        Observable.EveryUpdate().   // 애프터버너
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => GetCurrentState.Equals(CHARACTER_STATES.move)).
            Where(_ => Input.GetKeyDown(KeyCode.F)).
            Subscribe(_ =>
            {
                if (CurrentInfo.cur_power > 0f &&
                !GetCurrentState.Equals(CHARACTER_STATES.evacuation) &&
                !GetCurrentState.Equals(CHARACTER_STATES.evacuationWhileMoving))
                    StateTransition(CHARACTER_STATES.afterburner);
            }).AddTo(this);
        Observable.EveryUpdate().
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.warp)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.microWormholePenetate)).
            Where(_ => Input.GetKeyUp(KeyCode.F)).
            Subscribe(_ => StateTransition(Input.GetKey(KeyCode.W) ? CHARACTER_STATES.move : CHARACTER_STATES.idle)).AddTo(this);
        Observable.EveryUpdate().   // 워프
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => Input.GetKeyDown(KeyCode.G)).
            Where(_ => mapLimitCheck == 0).
            Where(_ => Warp.currentWarpTarget != null &&
                       !Warp.currentWarpTarget.Equals(CurrentInfo.cur_Stage.planet) &&
                       !Warp.currentWarpTarget.Equals(CurrentInfo.cur_WarpPoint)).
            Where(_ => GetCurrentState.Equals(CHARACTER_STATES.idle)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.warp)).
            Subscribe(_ =>
            {

                if (CharacterFeatureInfo.charTargetingModuleDisplayer.Enemies.Count > 0) // 적이 있어서 워프 못 함
                {
                    IEnumerator denyWarpDrive = denyWarpDriveActive("Warp denied, enemies must be destroyed first.");
                    StartCoroutine(denyWarpDrive);
                    isAboutToWarpDriveActive = true;
                    return;
                }
                bool check = CheckWarpDriveActivation();
                if (!check)    // 전방에 장애물이 있어서 워프 못 함
                {
                    return;
                }
                CHARACTER_STATES state = Warp.currentWarpTarget.WarpType == _WARP_.WARP_TYPE.longRangeWarp ?
                    CHARACTER_STATES.warp : CHARACTER_STATES.microWormholePenetate;
                if (state == CHARACTER_STATES.microWormholePenetate && !Warp.enableMicroWormholePenetration) // 마이크로 웜홀 준비 
                    return;
                StateTransition(state);
            }).AddTo(this);
        Observable.EveryUpdate().
            Where(_ => Input.GetKeyDown(KeyCode.LeftAlt)).
            Where(_ => !CurrentInfo.isWarpin).
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => gameMng.QRFMemberOrganizationCheck).
            Where(_ => !gameMng.IsQRFLaunched).
            Where(_ => EnemyManager.Instance.targets.Count > 1). // 적이 없을 때는 호출을 막는다.
            Subscribe(_ =>
            {
                gameMng.CallQRF(); // 기동타격대 호출
            }).AddTo(this);

        Observable.EveryUpdate().   // 회피 
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.warp)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.microWormholePenetate)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.evacuationWhileMoving)).
            Where(_ => !GetCurrentState.Equals(CHARACTER_STATES.evacuation)).
            Where(_ => Input.GetKeyDown(KeyCode.Space)).
            Subscribe(_ =>
            {
                if (GetCurrentState.Equals(CHARACTER_STATES.evacuation) ||
                    GetCurrentState.Equals(CHARACTER_STATES.afterburner) ||
                    GetCurrentState.Equals(CHARACTER_STATES.evacuationWhileMoving) ||
                    CurrentInfo.cur_power <= 0f)
                    return;
                // -1 left : 1 right
                // 이동중일때
                if (Input.GetKey(KeyCode.W))
                {
                    if (Input.GetKey(KeyCode.A))
                        StateTransition(CHARACTER_STATES.evacuationWhileMoving, -1);
                    else if (Input.GetKey(KeyCode.D))
                        StateTransition(CHARACTER_STATES.evacuationWhileMoving, 1);
                }
                // 이동중이 아닐 때
                else
                {
                    if (Input.GetKey(KeyCode.A))
                        StateTransition(CHARACTER_STATES.evacuation, -1);
                    else if (Input.GetKey(KeyCode.D))
                        StateTransition(CHARACTER_STATES.evacuation, 1);
                }
            }).AddTo(this);
        Observable.EveryUpdate().
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => CurrentInfo.isPowerRecoveryEnable).
            Subscribe(_ =>
            {
                CurrentInfo.cur_power += Stat.powerRecoveryRate * Time.deltaTime;
                CurrentInfo.cur_power = Mathf.Min(CurrentInfo.cur_power, Stat.power);
            }).AddTo(this);
        Observable.EveryUpdate().
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Where(_ => !CurrentInfo.isPowerRecoveryEnable).
            Subscribe(_ =>
            {
                if (!CurrentInfo.isPowerRecovering)
                {
                    CurrentInfo.isPowerRecovering = true;
                    try
                    {
                        StartCoroutine(activateRecovery());
                    }
                    catch { }
                }
            }).AddTo(this);
        this.ObserveEveryValueChanged(_ => Info.ShipTransform.position).
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Subscribe(_ =>
            {
                Info.Identity.rotation = Quaternion.identity;
            });
        this.ObserveEveryValueChanged(_ => mapLimitCheck).
            Where(_ => !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !gameMng.gameStateMng.isShowingInfoState).
            Subscribe(_ =>
            {
                var message = "";
                switch (mapLimitCheck)
                {
                    case 1:
                        message = "You are approaching Gravity Tracing Missile Field! Get back!";
                        CharacterFeatureInfo.MessageText.fontStyle = FontStyle.Normal;
                        break;
                    case 2:
                        message = "YOU ARE APPROACHING GRAVITY TRACING MISSILE FIELD!! GET BACK!!";
                        CharacterFeatureInfo.MessageText.fontStyle = FontStyle.Bold;
                        break;
                    case 3:
                        break;
                    default:
                        message = "";
                        break;
                }
                CharacterFeatureInfo.MessageText.text = message;
            });
    }

    private void Update()
    {
        var yRot = Info.ShipRigidbody.rotation.eulerAngles.y - recentRot.eulerAngles.y;
        // 0 : forward / 1 : left / -1 : right
        var dir = yRot < 0 ? 1f : -1f;
        if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
            zDirection = 0f;
        var rot = Mathf.Min(Mathf.Abs(yRot) * 0.5f, 1f);
        dir *= rot * 90f;
        zDirection = Mathf.Lerp(zDirection, dir, Stat.mobility * Time.deltaTime);
        recentRot = Info.ShipRigidbody.rotation;

        if (!CurrentInfo.isWarpin)
        {
            var dist = Info.ShipTransform.position.magnitude;
            if (dist >= PlayerMapLimit + 2000f) // 경고 무시하고 맵 밖으로 가면 바로 사망
                mapLimitCheck = 3;
            else if (dist >= PlayerMapLimit + 1000f) // 경고 더 심해짐
                mapLimitCheck = 2;
            else if (dist >= PlayerMapLimit)
                mapLimitCheck = 1;
            else
                mapLimitCheck = 0;
        }
        Execute();
    }

    private IEnumerator denyWarpDriveActive(string message)
    {
        CharacterFeatureInfo.warpDeniedAudio.Play();
        CharacterFeatureInfo.MessageText.text = message;
        CharacterFeatureInfo.hitEffectTextAnim.Play();
        while (CharacterFeatureInfo.hitEffectTextAnim.isPlaying)
        {
            yield return null;
        }
        isAboutToWarpDriveActive = false;
        CharacterFeatureInfo.MessageText.text = "";
    }

    public void PlayEnemyDestroyedTextAnim()
    {
        if (!CharacterFeatureInfo.hitEffectTextAnim.isPlaying)
        {
            var recentText = CharacterFeatureInfo.MessageText.text;
            CharacterFeatureInfo.MessageText.text = "ENEMY DESTROYED";
            CharacterFeatureInfo.hitEffectTextAnim.Play();
            IEnumerator back = backToText(recentText);
            StartCoroutine(back);
        }
    }

    public bool CheckWarpDriveActivation()
    {
        RaycastHit hit;
        var direction = (Warp.currentWarpTarget.transform.position - Info.ShipTransform.position).normalized;
        bool check = !Physics.Raycast(Info.ShipTransform.position + (direction * 75f), direction, out hit, Mathf.Infinity);
        try
        {
            if (hit.collider != null)
            {
                IEnumerator denyWarpDrive = denyWarpDriveActive("Warp denied, obstacle is ahead");
                StartCoroutine(denyWarpDrive);
            }
        }
        catch { }
        return check;
    }

    private IEnumerator backToText(string text)
    {
        yield return new WaitUntil(() => !CharacterFeatureInfo.hitEffectTextAnim.isPlaying);
        CharacterFeatureInfo.MessageText.text = text;
        var color = CharacterFeatureInfo.MessageText.color;
        color.a = 1f;
        CharacterFeatureInfo.MessageText.color = color;
    }

    public override void ShipDown()
    {
        StateTransition(CHARACTER_STATES.down);
        RemoveTarget();
        PlayShipExplosionEffect();
        EnemyManager.Instance.targets.Remove(this);
    }

    public override void TakeDamage(ProjectileStateMng projectile)
    {
        CurrentInfo.cur_ShieldCapacity -= projectile.CurrentInfo.damage;
        //var projectileCaliber = projectile.WeaponStateMng.armament;
        //switch (projectileCaliber)
        //{
        //    case WeaponBase.ARMAMENT._155mm:
        //        gameMng.cameraShaker.shakeAmount = 3f;
        //        gameMng.cameraShaker.ShakeCamera(0.33f);
        //        break;
        //    case WeaponBase.ARMAMENT._300mm:
        //        gameMng.cameraShaker.shakeAmount = 6f;
        //        gameMng.cameraShaker.ShakeCamera(0.33f);
        //        break;
        //    case WeaponBase.ARMAMENT._1200mm:
        //        gameMng.cameraShaker.shakeAmount = 10f;
        //        gameMng.cameraShaker.ShakeCamera(0.33f);
        //        break;
        //}
    }

    public void PlaySailEffect()
    {
        var effectDirection = Info.SailEffect.velocityOverLifetime;
        var dir = -Info.ShipTransform.forward * CurrentInfo.cur_velocity;
        var colorMain = Info.SailEffect.main;
        var a = Mathf.Min(CurrentInfo.cur_velocity / Stat.velocity, 1f);

        colorMain.startColor = new Color(1f, 1f, 1f, a);
        effectDirection.x = dir.x * 0.25f;
        effectDirection.y = dir.y * 0.25f;
        effectDirection.z = dir.z * 0.25f;
    }

    public void PlayWarpSailEffect()
    {
        var trail = Info.SailEffect.trails;
        trail.enabled = true;
        var effectDirection = Info.SailEffect.velocityOverLifetime;
        var dir = -Info.ShipTransform.forward * CurrentInfo.cur_warpVelocity * 0.005f;

        effectDirection.x = dir.x;
        effectDirection.y = dir.y;
        effectDirection.z = dir.z;
        var colorMain = Info.SailEffect.main;
        colorMain.startColor = new Color(1f, 1f, 1f, 0f);
    }

    private IEnumerator activateRecovery()
    {
        t = 0f;
        while (t <= Stat.powerRecoveryDelay)
        {
            t += Time.deltaTime;
            yield return null;
        }
        CurrentInfo.isPowerRecoveryEnable = true;
        CurrentInfo.isPowerRecovering = false;
    }

    public static _STAT_ operator +(CharStateMng charStateMng, SavedData.SavedDataContainer._UPGRADE_TIMES_STAT_ upgradeStat)
    {                                                                               // Base
        charStateMng.Stat.velocity = _CHARACTER_UPGRADE_.velocity * upgradeStat.velocity;                 // 400
        charStateMng.Stat.acceleration = _CHARACTER_UPGRADE_.acceleration * upgradeStat.acceleration + 0.5f;           // 1
        charStateMng.Stat.mobility = _CHARACTER_UPGRADE_.mobility * upgradeStat.mobility + 0.5f;         // 1.5
        charStateMng.Stat.power = _CHARACTER_UPGRADE_.power * upgradeStat.power;                    // 100
        charStateMng.Stat.shieldCapacity = _CHARACTER_UPGRADE_.shieldCapacity * upgradeStat.shieldCapacity;           // 200
        charStateMng.Stat.weaponRange = _CHARACTER_UPGRADE_.weaponRange * upgradeStat.weaponRange + 3000f;     // 4000
        charStateMng.Stat.weaponDamage = _CHARACTER_UPGRADE_.weaponDamage * upgradeStat.weaponDamage + 5f;         // 15
        charStateMng.Stat.weaponFireRate = 0.6f - _CHARACTER_UPGRADE_.weaponFireRate * upgradeStat.weaponFireRate;   // 0.4
        return charStateMng.Stat;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
