﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using System;
using UniRx;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine.Jobs;

public class CharTargetingModuleDisplayer : MonoBehaviour
{
    [SerializeField] private CharStateMng charStateMng;
    [SerializeField] private TargetingModuleStateMng targetingModuleStateMng;
    [SerializeField] private CharWeaponStateMng[] charWeaponArr;

    [Serializable]
    public struct _TARGET_PREDICTION_DISPLAYER_
    {
        public Transform targetPredictionPoint1;
        public Transform targetPredictionPoint2;
        public Indicator targetPredictionPointIndicator1;
        public Indicator targetPredictionPointIndicator2;
    }
    public _TARGET_PREDICTION_DISPLAYER_ TargetPredictionDisplayer;
    public List<TargetingModuleStateMng.ITargetable> Enemies { get; private set; }
    private bool enableDisplayingPredictionPoint;


    private NativeArray<bool> angleCheck;
    private NativeArray<int> distance;
    private NativeArray<bool> isAttackableRange;
    private NativeArray<bool> dirCheck;
    private NativeArray<Vector3> lockStateTextPosition;
    private NativeArray<Vector3> distanceTextPosition;

    // Start is called before the first frame update
    void Start()
    {
        enableDisplayingPredictionPoint = false;
        this.ObserveEveryValueChanged(_ => EnemyManager.Instance.targets.Count).
            Subscribe(_ =>
            {
                Enemies = EnemyManager.Instance.targets.
                    Where(target => !target.Identify.Equals(charStateMng.Identify)).ToList();
                try
                {
                    angleCheck.Dispose();
                    distance.Dispose();
                    isAttackableRange.Dispose();
                    dirCheck.Dispose();
                    lockStateTextPosition.Dispose();
                    distanceTextPosition.Dispose();
                }
                catch { }
                angleCheck = new NativeArray<bool>(1, Allocator.Persistent);
                distance = new NativeArray<int>(1, Allocator.Persistent);
                isAttackableRange = new NativeArray<bool>(1, Allocator.Persistent);
                dirCheck = new NativeArray<bool>(1, Allocator.Persistent);
                lockStateTextPosition = new NativeArray<Vector3>(1, Allocator.Persistent);
                distanceTextPosition = new NativeArray<Vector3>(1, Allocator.Persistent);
            });
    }

    private IEnumerator displayPredictionPoint()
    {
        TargetPredictionDisplayer.targetPredictionPointIndicator1.gameObject.SetActive(true);
        TargetPredictionDisplayer.targetPredictionPointIndicator2.gameObject.SetActive(true);
        while (targetingModuleStateMng.currentTarget != null &&
               !targetingModuleStateMng.currentTarget.Identify.Equals(charStateMng.Identify) &&
               targetingModuleStateMng.isOptimalZoneAvailable &&
               targetingModuleStateMng.currentTarget.getCurrentHitpoints > 0f)
        {
            TargetPredictionDisplayer.targetPredictionPoint1.position = charWeaponArr[0].TargetPredictionPoint;
            TargetPredictionDisplayer.targetPredictionPoint2.position = charWeaponArr[1].TargetPredictionPoint;
            yield return null;
        }
        TargetPredictionDisplayer.targetPredictionPointIndicator1.gameObject.SetActive(false);
        TargetPredictionDisplayer.targetPredictionPointIndicator2.gameObject.SetActive(false);
        enableDisplayingPredictionPoint = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetingModuleStateMng.currentTarget != null &&
            targetingModuleStateMng.isOptimalZoneAvailable &&
            !targetingModuleStateMng.currentTarget.Identify.Equals(charStateMng.Identify))
        {
            if (!enableDisplayingPredictionPoint)
            {
                try
                {
                    var dreadnought = (ShipBase)targetingModuleStateMng.currentTarget;
                    if (dreadnought.Info.ShipType.Equals(ShipBase.SHIP_TYPE.dreadnought))
                    {
                        return;
                    }
                }
                catch { }
                TargetPredictionDisplayer.targetPredictionPoint1.position = charWeaponArr[0].TargetPredictionPoint; // 미리 포지션 배정 후 오브젝트 활성화를 해준다.
                TargetPredictionDisplayer.targetPredictionPoint2.position = charWeaponArr[1].TargetPredictionPoint;
                enableDisplayingPredictionPoint = true;
                StartCoroutine(displayPredictionPoint());
            }
        }

        for (int i = 0; i < Enemies.Count; i++)
        {
            var enemy = Enemies[i];
            if (enemy.IndicatorImage == null)
            {
                Enemies.Remove(enemy);
                return;
            }

            var displayerJobData = new DisplayerData()
            {
                anglePos = charStateMng.Info.ShipTransform.InverseTransformPoint(enemy.TargetableTransform.position),
                charPosition = charStateMng.Info.ShipTransform.position,
                fallOffRange = charStateMng.Targeting.fallOffRange,
                indicatorPosition = enemy.IndicatorTransform.position,
                weaponLimitElevation = charStateMng.WeaponLimitElevation,
                targetPosition = enemy.TargetableTransform.position
            };
            var job = new IndicatorJob
            {
                displayerData = displayerJobData,
                angleCheck = angleCheck,
                distance = distance,
                isAttackableRange = isAttackableRange,
                dirCheck = dirCheck,
                lockStateTextPosition = lockStateTextPosition,
                distanceTextPosition = distanceTextPosition
            };
            JobHandle jobHandle = job.Schedule();
            jobHandle.Complete();

            enemy.LockStateText.alignment = angleCheck[0] ? TextAnchor.LowerLeft : TextAnchor.LowerRight;
            var distanceString = distance[0].ToString() + "m";
            enemy.LockStateText.text = angleCheck[0] && isAttackableRange[0] ? "Locked" : " ";
            enemy.DistanceText.text = distanceString;
            enemy.DistanceText.fontStyle = isAttackableRange[0] ? FontStyle.Bold : FontStyle.Normal;
            enemy.DistanceText.color = isAttackableRange[0] ? new Color(0f, 0.6008307f, 0.735849f, 1f) : new Color(0f, 0.6008307f, 0.735849f, 0.65f);
            enemy.DistanceText.fontSize = isAttackableRange[0] ? 22 : 18;
            enemy.DistanceText.alignment = dirCheck[0] ? TextAnchor.LowerLeft : TextAnchor.LowerRight;
            enemy.LockStateText.transform.position = lockStateTextPosition[0];
            enemy.DistanceText.transform.position = distanceTextPosition[0];
        }
    }

    public struct DisplayerData
    {
        public Vector3 anglePos;
        public Vector3 indicatorPosition;
        public Vector3 targetPosition;
        public Vector3 charPosition;
        public float weaponLimitElevation;
        public float fallOffRange;

        public void Update(out int distance, out bool angleCheck, out bool isAttackableRange, out bool dirCheck, out Vector3 lockStateTextPosition, out Vector3 distanceTextPosition)
        {
            var angle = new Vector2(Mathf.Atan2(anglePos.y, anglePos.z), Mathf.Atan2(anglePos.z, anglePos.x)) * Mathf.Rad2Deg;
            angle.y -= 90f;
            angleCheck = Mathf.Abs(angle.x) <= weaponLimitElevation && Mathf.Abs(angle.y) <= weaponLimitElevation;
            var textPos = indicatorPosition + (angleCheck ? new Vector3(38.5f, -13f) : new Vector3(-38.5f, -13f));
            textPos.x = Mathf.Clamp(textPos.x, -920f, 920f);
            textPos.y = Mathf.Clamp(textPos.y, -500f, 500f);
            lockStateTextPosition = textPos;

            distance = (int)(targetPosition - charPosition).magnitude;
            isAttackableRange = distance <= fallOffRange;
            dirCheck = indicatorPosition.x < 860f;
            var pos = indicatorPosition + (dirCheck ? new Vector3(51f, 6.5f) : new Vector3(-51f, 6.5f));
            pos.x = Mathf.Clamp(pos.x, -920f, 920f);
            pos.y = Mathf.Clamp(pos.y, -520f, 520f);
            distanceTextPosition = pos;
        }
    }

    [Unity.Burst.BurstCompile]
    private struct IndicatorJob : IJob
    {
        public DisplayerData displayerData;
        [NativeDisableParallelForRestriction] public NativeArray<bool> angleCheck;
        [NativeDisableParallelForRestriction] public NativeArray<int> distance;
        [NativeDisableParallelForRestriction] public NativeArray<bool> isAttackableRange;
        [NativeDisableParallelForRestriction] public NativeArray<bool> dirCheck;
        [NativeDisableParallelForRestriction] public NativeArray<Vector3> lockStateTextPosition;
        [NativeDisableParallelForRestriction] public NativeArray<Vector3> distanceTextPosition;

        public void Execute()
        {
            var data = displayerData;
            int distance;
            bool angleCheck;
            bool isAttackableRange;
            bool dirCheck;
            Vector3 lockStateTextPosition;
            Vector3 distanceTextPosition;
            displayerData.Update(out distance, out angleCheck, out isAttackableRange, out dirCheck, out lockStateTextPosition, out distanceTextPosition);
            displayerData = data;
            this.angleCheck[0] = angleCheck;
            this.distance[0] = distance;
            this.isAttackableRange[0] = isAttackableRange;
            this.dirCheck[0] = dirCheck;
            this.lockStateTextPosition[0] = lockStateTextPosition;
            this.distanceTextPosition[0] = distanceTextPosition;
        }
    }

    private void OnDestroy()
    {
        angleCheck.Dispose();
        distance.Dispose();
        isAttackableRange.Dispose();
        dirCheck.Dispose();
        lockStateTextPosition.Dispose();
        distanceTextPosition.Dispose();
    }
}
