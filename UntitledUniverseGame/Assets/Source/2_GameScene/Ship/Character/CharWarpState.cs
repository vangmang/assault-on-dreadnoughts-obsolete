﻿using FiniteStateMachine;
using System;
using System.Collections;
using UnityEngine;
using System.Text;

public class CharWarpState : State<CharStateMng>
{
    public override Enum GetState
    {
        get { return CharStateMng.CHARACTER_STATES.warp; }
    }

    private Quaternion recentRot;
    private WarpablePlace target;
    private bool scaleCheck;
    private bool warpHighlightCheck;
    private bool recentEnemiesTransformParentDisable;
    private int distance;

    public override void Enter(params object[] o_Params)
    {
        instance.CurrentInfo.isWarpin = true;
        //instance.PlayThrusterParticles();
        target = instance.Warp.currentWarpTarget;
        instance.Warp.targetedWarpTarget = target;

        distance = (int)(target.distance * 2f);
        scaleCheck = false;
        warpHighlightCheck = false;
        instance.CharacterFeatureInfo.StatusText.text = "WARP DRIVE ACTIVE";
        //StringBuilder sb = new StringBuilder("Destination: ");
        var sb = "Destination " + target.placetNameText.text;
        //sb.Append(target.placetNameText.text);
        instance.CharacterFeatureInfo.WarpTargetText.text = sb;
        StartCoroutine(aligning());
        instance.CurrentInfo.preLoadAfterWarp = false;
    }

    private IEnumerator aligning()
    {
        instance.Warp.WarpBeginAudio.Play();
        float t = 0f;
        float fov = instance.MainCamera.fieldOfView;        
        while (t <= instance.Warp.WarpAlignTime)
        {
            t += Time.deltaTime;
            instance.MainCamera.fieldOfView = Mathf.Lerp(fov, 57.5f, t * 0.2f);
            var yRot = instance.Info.ShipRigidbody.rotation.eulerAngles.y - recentRot.eulerAngles.y;
            var targetRot = target.transform.position - instance.Info.ShipRigidbody.position;
            var lookRot = Quaternion.LookRotation(targetRot, instance.Info.ShipTransform.up);
            var wantedRot = lookRot.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
            if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(instance.Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
                wantedRot.z = 0f;
            instance.Info.ShipTransform.rotation =
                Quaternion.Lerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
            instance.CurrentInfo.cur_rotation = instance.MainCamera.transform.rotation;
            instance.CurrentInfo.cur_velocity =
                Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity, instance.Stat.acceleration * Time.deltaTime);
            // 이동
            instance.Info.ShipRigidbody.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
            instance.PlayThrusterEffects(1f, 13f, 0.75f, 1f);
            instance.PlaySailEffect();
            recentRot = instance.Info.ShipRigidbody.rotation;
            yield return null;
        }
        //instance.CurrentInfo.cur_velocity = 0f;
        EnemyManager.Instance.ClearTargets();
        StartCoroutine(activateWarpSequence1());
    }

    // 시퀀스1) 현재 지점에서 70만 이상 날라가주고 현지점 스케일을 줄여나간다.
    private IEnumerator activateWarpSequence1()
    {
        var targetPos = (instance.Info.ShipTransform.forward * 1000000f);
        var targetDistance = targetPos.magnitude; //(target.transform.position - instance.Info.ShipRigidbody.position).magnitude;
        float t = 0f;        
        //yield return new WaitForSeconds(2f);
        while(t < 2f)
        {
            t += Time.deltaTime;
            instance.CurrentInfo.cur_velocity =
                Mathf.Lerp(instance.CurrentInfo.cur_velocity, 0f, instance.Stat.acceleration * Time.deltaTime);
            instance.Info.ShipRigidbody.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
            instance.PlayThrusterEffects(1f, 13f, 0.75f, 1f);
            instance.PlaySailEffect();
            yield return null;
        }
        t = 0f;
        //instance.CharacterFeatureInfo.mouseFollowingCamera.interpolation = false;
        do
        {
            var distance = (targetPos - instance.Info.ShipRigidbody.position).magnitude;
            //instance.CharacterFeatureInfo.cameraShaker.ShakeCamera(1);
            //instance.CharacterFeatureInfo.cameraShaker.shakeAmount = Mathf.Abs(1 - t) * 5f;
            t = Mathf.InverseLerp(0f, targetDistance, distance);
            instance.CurrentInfo.cur_warpVelocity = Mathf.Lerp(instance.CurrentInfo.cur_warpVelocity, instance.Warp.WarpVelocity, instance.Warp.WarpAcceleration * 0.2f * Time.deltaTime);
            if (t <= 0.97f)
                instance.PlayThrusterEffects(1f, 0f, 0f, 1f);
            if (t <= 0.97f && !scaleCheck)
            {
                scaleCheck = true;
                StartCoroutine(decreaseScaleSequence());
            }
            if (t <= 0.9f && !warpHighlightCheck)
            {
                warpHighlightCheck = true;
                instance.SetThrusterEffect(0f);
                //instance.CurrentInfo.cur_WarpPoint.SetPointParent(false);
                StartCoroutine(playWarpSailEffectSequence());
            }
            if(t <= 0.5f && !recentEnemiesTransformParentDisable)
            {
                recentEnemiesTransformParentDisable = true;
                instance.CurrentInfo.cur_WarpPoint.createEnemies.gameObject.SetActive(false);
            }
            instance.Info.ShipRigidbody.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_warpVelocity * Time.deltaTime;
            instance.Warp.isWarpHighlight = true;
            yield return null;
        }
        while (t >= 0.3f);
        instance.Warp.WarpEnterAudio.Play();
        instance.CurrentInfo.recentStage = instance.CurrentInfo.cur_Stage;
        StartCoroutine(activateWarpSequence2());
        //instance.CurrentInfo.cur_Stage.gameObject.SetActive(false);
    }

    private IEnumerator decreaseScaleSequence()
    {
        while (scaleCheck)
        {
            instance.CurrentInfo.cur_Stage.stage.localScale =
                Vector3.Lerp(instance.CurrentInfo.cur_Stage.stage.localScale, Vector3.zero, instance.Warp.WarpAcceleration * 0.225f * Time.deltaTime);
            instance.MainCamera.fieldOfView = Mathf.Lerp(instance.MainCamera.fieldOfView, 75f, instance.Stat.acceleration * 2f * Time.deltaTime);
            yield return null;
        }
    }

    private IEnumerator playWarpSailEffectSequence()
    {
        var trail = instance.Info.SailEffect.trails;
        trail.colorOverTrail = new Color(1f, 1f, 1f, 0f);
        float t = 0f;
        while (warpHighlightCheck)
        {
            t = Mathf.Lerp(t, 1f, instance.Warp.WarpAcceleration * 0.03f * Time.deltaTime);
            trail.colorOverTrail = new Color(1f, 1f, 1f, t);
            instance.PlayWarpSailEffect();
            yield return null;
        }
    }

    // 시퀀스2) 타겟의 반대방향으로 10만만큼 플레이어가 눈치채지 못하도록 이동한다.\
    // 태양도 이동시켜준다.
    private IEnumerator activateWarpSequence2()
    {
        GC.Collect();
        instance.Warp.WarpMainAudio.volume = 1f;
        instance.Warp.WarpMainAudio.Play();
        var dir = (instance.Info.ShipRigidbody.position - this.target.transform.position).normalized;
        var start = instance.Info.ShipRigidbody.position;
        var target = dir * 100000f;
        //instance.Info.ShipRigidbody.position = target;
        float t = 0f;
        // 태양 이동
        var sunStartRot = GameMng.Instance.DirectionalLight.transform.rotation;
        var sunEndRot = Quaternion.LookRotation(((Planet)this.target).planetInfo.OriginalDireciton, Vector3.up);
        bool disableRecentStage = true;
        //bool targetScaleCheck = true;
        while (t <= distance)
        {
            if (t >= distance * 0.1f && disableRecentStage)
            {
                instance.CurrentInfo.recentStage.gameObject.SetActive(false);
                disableRecentStage = false;
            }
            instance.CharacterFeatureInfo.WarpTimeLeftText.text = string.Format("{0:N2}/sec", (distance - t)); //sb.ToString();
            t += Time.deltaTime;
            instance.Info.ShipRigidbody.position = Vector3.Lerp(start, target, t / distance);
            GameMng.Instance.SetDirectionalLightRotation(Quaternion.Lerp(sunStartRot, sunEndRot, t / distance));
            yield return null;
        }
        scaleCheck = false;
        instance.CharacterFeatureInfo.WarpTimeLeftText.text = "";
        StartCoroutine(clearWarpSailEffect());
        instance.CurrentInfo.preLoadAfterWarp = true;
        yield return new WaitForSeconds(0.75f);
        IEnumerator sequence2_5 = activateWarpSequence2_5(dir);
        GC.Collect();
        StartCoroutine(sequence2_5);
        StartCoroutine(activateWarpSequence3());
    }

    // 추가된 중간 시퀀스 2.5) 타겟지점의 행성 스케일을 먼저 늘려준다.
    private IEnumerator activateWarpSequence2_5(Vector3 _target)
    {
        var planet = (Planet)target;
        if (planet)
        {
            var planetPos = _target * -ShipBase._WARP_.warpConstant;
            planet.planet.localPosition = planetPos;
        }
        target.stage.gameObject.SetActive(true);
        float t = 0f;
        while (t < 2f)
        {
            t += Time.deltaTime;
            target.stage.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, t * 0.5f);
            yield return null;
        }
    }

    // 시퀀스3) 타겟지점의 게임오브젝트를 활성화하고 스케일을 늘려줌과 동시에 함선이 도착한다.
    private IEnumerator activateWarpSequence3()
    {
        //yield return new WaitForSeconds(0.9f);
        var targetDistance = (-instance.Info.ShipRigidbody.position).magnitude;
        float t = 1f;
        float time = 0f;
        float endTime = 10f;

        //StringBuilder sb = new StringBuilder(target.stage.planet.ToString());
        var sb = target.stage.planet.ToString() + instance.CurrentInfo.cur_Stage.planet.ToString();
        //sb.Append(instance.CurrentInfo.cur_Stage.planet.ToString());
        do
        {
            time += Time.deltaTime;
            var distance = (-instance.Info.ShipRigidbody.position).magnitude;
            t = Mathf.InverseLerp(0f, targetDistance, distance);
            instance.Warp.WarpMainAudio.volume = t;
            if (t <= 0.5f && instance.Warp.isWarpHighlight)
            {
                instance.CurrentInfo.cur_Stage.isCurrentStage = false;
                instance.CurrentInfo.cur_Stage.planet.Indictaor.target = instance.CurrentInfo.cur_Stage.planet.transform;
                instance.CurrentInfo.cur_Stage.planetTransform.position = Vector3.zero;
                instance.CurrentInfo.recentStage = instance.CurrentInfo.cur_Stage;
                instance.CurrentInfo.cur_Stage = target.stage;
                instance.CurrentInfo.cur_WarpPoint = WarpablePlace.WarpPointTable[sb];
                instance.Warp.isWarpHighlight = false;
                instance.SetCurrentStage();
            }
            instance.PlayThrusterEffects(1f, 13f, 0.75f, 1f);
            instance.CurrentInfo.cur_warpVelocity = 
                Mathf.Lerp(instance.CurrentInfo.cur_warpVelocity, 
                           instance.Stat.velocity, 
                           instance.Warp.WarpAcceleration * 0.21f * Time.deltaTime);
            instance.Info.ShipRigidbody.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_warpVelocity * Time.deltaTime;
            yield return null;
        }
        while (t >= 0.019f && time <= endTime);
        instance.Warp.WarpMainAudio.Stop();
        instance.Warp.WarpMainAudio.volume = 0f;
        instance.CurrentInfo.cur_Stage.stage.localScale = Vector3.one;
        instance.CurrentInfo.cur_rotation = instance.Info.ShipRigidbody.rotation;
        instance.StateTransition(CharStateMng.CHARACTER_STATES.idle);
    }

    private IEnumerator clearWarpSailEffect()
    {
        var trail = instance.Info.SailEffect.trails;
        trail.colorOverTrail = new Color(1f, 1f, 1f, 1f);
        float t = 1f;
        while (warpHighlightCheck)
        {
            t = Mathf.Lerp(t, 0f, instance.Warp.WarpAcceleration * 0.5f * Time.deltaTime);
            trail.colorOverTrail = new Color(1f, 1f, 1f, t);
            instance.PlayWarpSailEffect();
            yield return null;
        }
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        //instance.CharacterFeatureInfo.mouseFollowingCamera.interpolation = true;
        instance.CharacterFeatureInfo.StatusText.text = "";
        instance.CharacterFeatureInfo.WarpTargetText.text = "";
        warpHighlightCheck = false;
        scaleCheck = false;
        var trail = instance.Info.SailEffect.trails;
        trail.enabled = false;
        var colorMain = instance.Info.SailEffect.main;
        colorMain.startColor = new Color(1f, 1f, 1f, 1f);
        instance.CurrentInfo.isWarpin = false;
        EnemyManager.Instance.targets.Add(instance);
    }
}