﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class TargetingAlert : MonoBehaviour
{
    [SerializeField] private EnemyShipBase enemyShipStateMng;
    [SerializeField] private GameObject enemyIndicator;
    //[SerializeField] private Transform target;
    public CharStateMng charStateMng;
    public EnemyPointIndicator PointIndicator { get; private set; }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitUntil(() => charStateMng != null);
        transform.localPosition = Vector3.zero;
        // 1) 인디케이터 프리팹 생성
        if (enemyShipStateMng.Identify != charStateMng.Identify)
        {
            PointIndicator = Instantiate(enemyIndicator, EnemyManager.Instance.AlertIndicatorParent.transform).GetComponent<EnemyPointIndicator>();
            PointIndicator.mainCamera = GameMng.Instance.MainCamera;
            PointIndicator.target = transform;
            PointIndicator.targetIndicator = charStateMng.CharacterFeatureInfo.characterPointIndicator;
            // 2) 적이 타겟팅 했을 시 거리 및 방향 계산
            this.ObserveEveryValueChanged(_ => enemyShipStateMng.Info.TargetingModuleStateMng.currentTarget).
                Where(_ => !charStateMng.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause)).
                Subscribe(_ =>
                {
                    if (enemyShipStateMng.Info.TargetingModuleStateMng.currentTarget != null)
                    {
                        try // 타겟이 제거되어 삭제 되었을 때
                        {
                            if (!PointIndicator.SubImage.gameObject.activeSelf)
                            {
                                PointIndicator.TargetingAlertAnim.Play();
                                PointIndicator.SubImage.gameObject.SetActive(true);
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        PointIndicator.TargetingAlertAnim.Stop();
                        PointIndicator.SubImage.gameObject.SetActive(false);
                    }
                });
            this.ObserveEveryValueChanged(_ => enemyShipStateMng.getCurrentHitpoints).
                Where(_ => enemyShipStateMng.getCurrentHitpoints <= 0).
                Subscribe(_ =>
                {
                    try
                    {
                        Destroy(PointIndicator.gameObject);
                    }
                    catch { }
                });
        }
        Observable.EveryLateUpdate().Subscribe(_ => { Display(); }).AddTo(this);
    }

    private void Display()
    {
        if (enemyShipStateMng.getCurrentHitpoints > 0)
        {
            var dir = (enemyShipStateMng.Info.ShipTransform.position - charStateMng.Info.ShipTransform.position).normalized;
            var lookRot = Quaternion.LookRotation(dir, charStateMng.Info.ShipTransform.up);
            transform.localRotation = lookRot;
            transform.localPosition = transform.forward * 85f;
        }
    }

    private void OnDestroy()
    {
        
    }
}
