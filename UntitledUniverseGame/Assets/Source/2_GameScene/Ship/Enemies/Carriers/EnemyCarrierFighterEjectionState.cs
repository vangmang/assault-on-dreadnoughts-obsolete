﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyCarrierFighterEjectionState : State<EnemyCarrierStateMng>
{
    public override Enum GetState
    {
        get { return EnemyCarrierStateMng.ENEMY_CARRIER_STATES.fighterEjection; }
    }

    public override void Enter(params object[] o_Params)
    {
        StartCoroutine(invokeFighterEjection());
    }

    private IEnumerator invokeFighterEjection()
    {
        foreach (var fighter in instance.EnemyCarrierAIData.FighterStateMngArr)
        {
            fighter.StateTransition(EnemyFighterStateMng.ENEMY_FIGHTER_STATES.ejectFromCarrier);
            var t = 3500f / (instance.EnemyCarrierAIData.FighterStateMngArr[instance.EnemyCarrierAIData.FighterStateMngArr.Length - 1].Stat.velocity * 3f);
            yield return new WaitForSeconds(t + instance.EnemyCarrierAIData.fighterEjectionDelay);
        }
        instance.StateTransition(EnemyCarrierStateMng.ENEMY_CARRIER_STATES.engage);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
