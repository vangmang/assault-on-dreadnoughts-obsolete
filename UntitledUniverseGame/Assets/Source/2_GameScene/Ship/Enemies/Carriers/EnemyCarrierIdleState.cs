﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyCarrierIdleState : State<EnemyCarrierStateMng>
{
    public override Enum GetState
    {
        get { return EnemyCarrierStateMng.ENEMY_CARRIER_STATES.idle; }
    }
    
    public override void Enter(params object[] o_Params)
    {
        //instance.StopThrusterParticles();
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
