﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using System.Linq;

public class EnemyCarrierStateMng : EnemyShipBase
{
    public enum ENEMY_CARRIER_STATES
    {
        idle,
        fighterEjection,
        engage,
        down
    }

    [Serializable]
    public struct _ENEMY_CARRIER_AI_DATA_
    {
        public TargetingModuleStateMng.ITargetable detectedTarget;
        public float maintainDistance;
        public float detectableDistance;
        public float detectableAngle;
        public float fighterEjectionDelay;
        public bool isEngaging;
        [SerializeField] private EnemyFighterStateMng[] fighterStateMngArr;
        public EnemyFighterStateMng[] FighterStateMngArr { get { return fighterStateMngArr; } }
    }

    public Light[] additionalLights;
    public _ENEMY_CARRIER_AI_DATA_ EnemyCarrierAIData;
    [NonSerialized] public float zDirection;
    private Quaternion recentRot;

    protected override void InitOnAwake()
    {
        WeaponLimitElevation = EnemyCarrierAIData.detectableAngle;
        TargetableTransform = Info.ShipTransform;
        base.InitOnAwake();
    }

    // Start is called before the first frame update
    void Start()
    {
        //CreateEnemies.Instance.targets.Add(this);
        CurrentInfo.cur_ShieldCapacity = Stat.shieldCapacity;
        StartCoroutine(playAdditionalLightEffects());
        StateEnter(ENEMY_CARRIER_STATES.idle);
        this.ObserveEveryValueChanged(_ => Info.ShipTransform.rotation).
            Subscribe(_ =>
            {
                var yRot = Info.ShipTransform.rotation.eulerAngles.y - recentRot.eulerAngles.y;
                // 0 : forward / 1 : left / -1 : right
                var dir = yRot < 0 ? 1f : -1f;
                if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(Info.ShipTransform.rotation.eulerAngles.x - 180f) <= 135f)
                    dir = 0f;
                var rot = Mathf.Min(Mathf.Abs(yRot) * 0.5f, 1f);
                dir *= rot * 90f;
                zDirection = Mathf.Lerp(zDirection, dir, Stat.mobility * Time.deltaTime);
                recentRot = Info.ShipTransform.rotation;
            });

    }

    public override void ShipDown()
    {
        StateTransition(ENEMY_CARRIER_STATES.down);
        PlayShipExplosionEffect();
    }

    private IEnumerator playAdditionalLightEffects()
    {
        while (CurrentInfo.cur_ShieldCapacity > 0)
        {
            for (int i = 0; i < additionalLights.Length - 1; i += 2)
            {
                additionalLights[i].intensity = 50;
                additionalLights[i + 1].intensity = 5;
                yield return new WaitForSeconds(0.05f);
                additionalLights[i].intensity = 1;
                additionalLights[i + 1].intensity = 1;
            }
            yield return new WaitForSeconds(1f);
        }
    }

    private void Update()
    {
        Execute();
        DetectTarget();
    }

    public void DetectTarget()
    {
        var enemyList = Info.TargetingModuleStateMng.EnemyList;
        // 거리 우선순위로 타겟선별
        TargetingModuleStateMng.ITargetable target = null;
        try
        {
            //target = enemyList.Where(ship => ship.CurrentDistance <= EnemyCarrierAIData.detectableDistance).OrderBy(ship => ship.CurrentDistance).First();
            target = Info.TargetingModuleStateMng.currentTarget;
        }
        catch { target = null; }
        if (target != null)
        {
            EnemyCarrierAIData.detectedTarget = target;
            if (!Info.TargetingModuleStateMng.IsInRange)
            {
                EnemyCarrierAIData.isEngaging = false;
                StateTransition(ENEMY_CARRIER_STATES.idle);
            }
            else if ((Info.TargetingModuleStateMng.currentTarget != null || Info.TargetingModuleStateMng.IsInRange) &&
                !EnemyCarrierAIData.isEngaging)
            {
                if (!GetCurrentState.Equals(ENEMY_CARRIER_STATES.fighterEjection))
                {
                    EnemyCarrierAIData.isEngaging = true;
                    StateTransition(ENEMY_CARRIER_STATES.fighterEjection);
                }
            }
        }
        else
        {
            StateTransition(ENEMY_CARRIER_STATES.idle);
        }
    }

    private void OnDestroy()
    {

    }
}
