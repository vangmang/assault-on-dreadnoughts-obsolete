﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFighterDownState : State<EnemyFighterStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFighterStateMng.ENEMY_FIGHTER_STATES.down; }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
