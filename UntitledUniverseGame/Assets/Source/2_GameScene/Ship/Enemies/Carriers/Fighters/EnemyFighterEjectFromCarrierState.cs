﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFighterEjectFromCarrierState : State<EnemyFighterStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFighterStateMng.ENEMY_FIGHTER_STATES.ejectFromCarrier; }
    }

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
        instance.EnemyFighterAIData.ejectFromCarrier = true;
        
        instance.Info.ShipTransform.position = instance.enemyCarrierStateMng.Info.ShipTransform.position + new Vector3(0f, -85f, -900f);
        instance.Info.ShipTransform.rotation = instance.enemyCarrierStateMng.Info.ShipTransform.rotation;
    }

    public override void Execute()
    {
        instance.CurrentInfo.cur_velocity = Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity * 3f, instance.Stat.acceleration * Time.deltaTime);
        instance.Info.ShipRigidbody.velocity = instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity;
        var dist = (instance.Info.ShipTransform.position - instance.enemyCarrierStateMng.Info.ShipTransform.position).magnitude;
        if(dist >= 2000f)
            instance.Info.ShipTransform.Rotate(instance.Info.ShipTransform.right, -10f * Time.deltaTime);
        if(dist >= 3500f)        
            instance.StateTransition(EnemyFighterStateMng.ENEMY_FIGHTER_STATES.engage);
    }

    public override void Exit()
    {
        instance.EnemyFighterAIData.ejectFromCarrier = false;
    }
}
