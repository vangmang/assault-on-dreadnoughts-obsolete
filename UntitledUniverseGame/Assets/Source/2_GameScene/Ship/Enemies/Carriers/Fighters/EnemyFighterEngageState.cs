﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFighterEngageState : State<EnemyFighterStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFighterStateMng.ENEMY_FIGHTER_STATES.engage; }
    }

    private Vector3 primaryTarget;
    private float yAngle;
    private float angle;
    private float rotDir;
    private Quaternion recentRot;
    private bool evasion;
    private float evasionTime;

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
        primaryTarget = Vector3.zero;

        var targetShip = (ShipBase)instance.EnemyFighterAIData.detectedTarget;
        if (targetShip)
        {
            var pos = instance.Info.Identity.position - targetShip.Info.Identity.position;
            yAngle = Mathf.Atan2(pos.z, pos.x);
        }
        primaryTarget = new Vector3(Mathf.Cos(yAngle) * instance.EnemyFighterAIData.maintainDistance,
                            instance.EnemyFighterAIData.detectedTarget.TargetableTransform.position.y - 500f,
                            Mathf.Sin(yAngle) * instance.EnemyFighterAIData.maintainDistance);
        rotDir = yAngle <= 90f && yAngle >= -90f ? 1f : -1f; // true -> left : false -> right
        recentRot = instance.Info.ShipRigidbody.rotation;
        angle = 0;
        evasionTime = 0f;
    }

    public override void Execute()
    {
        var currentTarget = instance.EnemyFighterAIData.detectedTarget;
        var dist = (primaryTarget - instance.Info.ShipTransform.position).magnitude;
        var targetShip = (ShipBase)instance.EnemyFighterAIData.detectedTarget;
        if (targetShip)
        {
            if (dist <= 500f)
            {
                //rotDir = yAngle <= 90f && yAngle >= -90f ? 1f : -1f; // true -> left : false -> right
                angle += 10f;
                var pos = instance.Info.Identity.position - targetShip.Info.Identity.position;
                yAngle = Mathf.Atan2(pos.z, pos.x);
                yAngle += angle * Mathf.Deg2Rad * rotDir;
            }
        }
        var wantedPos = new Vector3(Mathf.Cos(yAngle) * instance.EnemyFighterAIData.maintainDistance,
                                    currentTarget.TargetableTransform.position.y - 500f,
                                    Mathf.Sin(yAngle) * instance.EnemyFighterAIData.maintainDistance);
        var evasionDist = instance.Stat.velocity * 0.25f;
        if (evasion)
        {
            var distWithObstacle = (instance.Info.ShipTransform.position - primaryTarget).magnitude;
            evasionTime += Time.deltaTime;
            //TODO: EvasionTime 거리 / 속력으로 바꿀 것
            if (distWithObstacle <= evasionDist || evasionTime >= 3f)
            {
                evasionTime = 0f;
                //var pos = instance.Info.Identity.position - targetShip.Info.Identity.position;
                //yAngle = Mathf.Atan2(pos.z, pos.x);
                evasion = false;
            }
        }
        else
        {
            primaryTarget = wantedPos;
        }
        instance.DetectObstacle(ref evasion, ref primaryTarget);

        var targetRot = primaryTarget - instance.Info.ShipTransform.position;
        var yRot = instance.Info.ShipRigidbody.rotation.eulerAngles.y - recentRot.eulerAngles.y;
        var lookRot = Quaternion.LookRotation(targetRot, instance.Info.ShipTransform.up);
        var wantedRot = lookRot.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
        if (Mathf.Abs(yRot) < 0.25f || Mathf.Abs(instance.Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
            wantedRot.z = 0f;
        instance.Info.ShipTransform.rotation =
            Quaternion.Slerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
        instance.CurrentInfo.cur_rotation = lookRot;
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity, instance.Stat.acceleration * Time.deltaTime);
        instance.Info.ShipRigidbody.velocity = instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * 50f * Time.deltaTime;
        recentRot = instance.Info.ShipRigidbody.rotation;
    }

    public override void Exit()
    {
    }
}
