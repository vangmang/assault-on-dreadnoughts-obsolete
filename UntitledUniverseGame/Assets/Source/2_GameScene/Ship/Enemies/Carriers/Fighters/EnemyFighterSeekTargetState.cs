﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFighterSeekTargetState : State<EnemyFighterStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFighterStateMng.ENEMY_FIGHTER_STATES.seekTarget; }
    }

    private Quaternion recentRot;
    private Vector3 primaryTarget;
    private Vector3 secondaryTarget;
    private bool evasion;
    private float evasionTime;
    private float time;
    private float endTime;

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
        evasionTime = 0f;
        var detectedTarget = instance.EnemyFighterAIData.detectedTarget;
        secondaryTarget = detectedTarget.TargetableTransform.position +
                        (instance.Info.ShipTransform.position - detectedTarget.TargetableTransform.position).normalized *
                        -instance.EnemyFighterAIData.optimizedEngageDistance;
        primaryTarget = secondaryTarget;
        evasionTime = 0f;
        time = 0f;
        evasionTime = 0f;
        endTime = instance.EnemyFighterAIData.optimizedEngageDistance / instance.Stat.velocity;
        recentRot = instance.Info.ShipRigidbody.rotation;
    }

    public override void Execute()
    {
        var evasionDist = instance.Stat.velocity * 0.25f;
        if (evasion)
        {
            var distWithObstacle = (instance.Info.ShipTransform.position - primaryTarget).magnitude;
            evasionTime += Time.deltaTime;
            if (distWithObstacle <= evasionDist || evasionTime >= 3f)
            {
                evasionTime = 0f;
                evasion = false;
            }
        }
        else
        {
            time += Time.deltaTime;
            primaryTarget = secondaryTarget;
            float dist = (primaryTarget - instance.Info.ShipRigidbody.position).magnitude;

            if (time >= endTime || dist <= instance.Stat.velocity * 0.2f)
            {
                // 교전 가능 거리에 타겟이 있는지 체크하고 상태 전이를 실시한다.
                //EnemyFighterStateMng.ENEMY_FIGHTER_STATES state =
                //    instance.IsInRange ? EnemyFighterStateMng.ENEMY_FIGHTER_STATES.engage : EnemyFighterStateMng.ENEMY_FIGHTER_STATES.seekTarget /* seekTargetBehind */;
                instance.StateTransition(EnemyFighterStateMng.ENEMY_FIGHTER_STATES.engage);
                return;
            }
            //if (instance.Info.TargetingModuleStateMng.currentTarget != null)
            //{
            //    instance.StateTransition(EnemyFighterStateMng.ENEMY_FIGHTER_STATES.engage);
            //    return;
            //}
        }

        var yRot = instance.Info.ShipRigidbody.rotation.eulerAngles.y - recentRot.eulerAngles.y;
        var targetRot = primaryTarget - instance.Info.ShipRigidbody.position;
        var lookRot = Quaternion.LookRotation(targetRot, instance.Info.ShipTransform.up);
        var wantedRot = lookRot.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
        if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(instance.Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
            wantedRot.z = 0f;
        instance.Info.ShipTransform.rotation =
            Quaternion.Lerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
        instance.CurrentInfo.cur_rotation = Quaternion.Euler(wantedRot);
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity, instance.Stat.acceleration * Time.deltaTime);
        // 이동
        instance.Info.ShipRigidbody.velocity = instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * 50f * Time.deltaTime;
        //instance.PlayThrusterEffect(13f, 1f);
        recentRot = instance.Info.ShipRigidbody.rotation;
    }

    public override void Exit()
    {
    }
}
