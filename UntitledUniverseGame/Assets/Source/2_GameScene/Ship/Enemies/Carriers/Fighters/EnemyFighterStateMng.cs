﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using System.Linq;

public class EnemyFighterStateMng : EnemyShipBase
{
    public enum ENEMY_FIGHTER_STATES
    {
        idle,
        ejectFromCarrier,
        engage,
        seekTarget,
        down
    }

    [Serializable]
    public struct _ENEMY_FIGHTER_AI_DATA_
    {
        public TargetingModuleStateMng.ITargetable detectedTarget;
        public float maintainDistance;
        public float detectableDistance;
        public float detectableAngle;
        public float optimizedEngageDistance;
        public bool ejectFromCarrier;
        public bool isEngaging;
    }

    public _ENEMY_FIGHTER_AI_DATA_ EnemyFighterAIData;
    public EnemyCarrierStateMng enemyCarrierStateMng;
    [SerializeField] private float weaponLimitElevation;
    [NonSerialized] public float zDirection;
    private Quaternion recentRot;

    protected override void InitOnAwake()
    {
        TargetableTransform = Info.ShipTransform;
        EnemyFighterAIData.ejectFromCarrier = enemyCarrierStateMng != null ? false : true;
    }

    // Start is called before the first frame update
    void Start()
    {
        EnemyFighterAIData.isEngaging = false;
        charStateMng = CharStateMng.Instance;
        WeaponLimitElevation = weaponLimitElevation;
        InitInstantiation();
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_WarpPoint).
            Where(point => CurrentInfo.cur_WarpPoint.Equals(point)).
            Subscribe(point =>
            {
                //Info.ShipTransform.position = Vector3.zero;
                Info.ShipTransform.localPosition = new Vector3(0f, 0f, -1000f);// + (CurrentInfo.cur_WarpPoint.originalDirectionWithTarget * _WARP_.warpConstant);
                point.createEnemies.AddEnmiesIntoLocalArea(this);
            });
        this.ObserveEveryValueChanged(_ => Info.ShipRigidbody.rotation).
            Subscribe(_ =>
            {
                var yRot = Info.ShipRigidbody.rotation.eulerAngles.y - recentRot.eulerAngles.y;
                // 0 : forward / 1 : left / -1 : right
                var dir = yRot < 0 ? 1f : -1f;
                if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
                    dir = 0f;
                var rot = Mathf.Min(Mathf.Abs(yRot) * 0.5f, 1f);
                dir *= rot * 90f;
                zDirection = Mathf.Lerp(zDirection, dir, Stat.mobility * Time.deltaTime);
                recentRot = Info.ShipRigidbody.rotation;
            });
        StateEnter(ENEMY_FIGHTER_STATES.idle);
    }

    public override void ShipDown()
    {
        StateTransition(ENEMY_FIGHTER_STATES.down);
        PlayShipExplosionEffect();
    }

    // Update is called once per frame
    void Update()
    {
        Execute();

        //Debug.Log(GetCurrentState);
        //DetectTarget();
    }

    public void DetectObstacle(ref bool evasion, ref Vector3 primaryTarget)
    {
        RaycastHit hit;
        if (Physics.Raycast(Info.ShipTransform.position, Info.ShipTransform.forward, out hit, Stat.velocity * 2f))
        {
            evasion = true;
            var collider = hit.collider.transform;
            var yPosComparer = Info.ShipTransform.position.y > collider.position.y;
            primaryTarget = hit.point + (Info.ShipTransform.up * (hit.collider.bounds.size.magnitude * (yPosComparer ? 1f : -1f)));
        }
    }

    //public void DetectTarget()
    //{
    //    var enemyList = Info.TargetingModuleStateMng.EnemyList;
    //    // 거리 우선순위로 타겟선별
    //    TargetingModuleStateMng.ITargetable target = null;
    //    try
    //    {
    //        // 파이터의 경우 캐리어에 속해 있냐 아니냐로 구분한다. 캐리어에 속한 파이터의 경우 캐리어의 타겟을 우선순위 타겟으로 정한다.
    //        //target = enemyCarrierStateMng ? enemyCarrierStateMng.Info.TargetingModuleStateMng.currentTarget :
    //        //    enemyList.Where(ship => ship.CurrentDistance <= EnemyFighterAIData.detectableDistance).OrderBy(ship => ship.CurrentDistance).First();
    //    }
    //    catch { target = null; }
    //    if (target != null) /* && !EnemyFighterAIData.ejectFromCarrier)*/
    //    {
    //        EnemyFighterAIData.detectedTarget = target;
    //        if (!target.IsInRange)
    //        {
    //            EnemyFighterAIData.isEngaging = false;
    //            if (!GetCurrentState.Equals(ENEMY_FIGHTER_STATES.seekTarget))
    //                StateTransition(ENEMY_FIGHTER_STATES.seekTarget);
    //        }
    //        else if ((Info.TargetingModuleStateMng.currentTarget != null || target.IsInRange) &&
    //            !EnemyFighterAIData.isEngaging)
    //        {
    //            if (!GetCurrentState.Equals(ENEMY_FIGHTER_STATES.engage))
    //            {
    //                EnemyFighterAIData.isEngaging = true;
    //                StateTransition(ENEMY_FIGHTER_STATES.engage);
    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (!GetCurrentState.Equals(ENEMY_FIGHTER_STATES.idle))
    //            StateTransition(ENEMY_FIGHTER_STATES.idle);
    //    }
    //}

    private void OnDestroy()
    {

    }
}
