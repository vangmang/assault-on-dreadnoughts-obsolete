﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyDreadnoughtDownState : State<EnemyDreadnoughtStateMng>
{
    public override Enum GetState
    {
        get { return EnemyDreadnoughtStateMng.ENEMY_DREADNOUGHT_STATES.down; }
    }

    public override void Enter(params object[] o_Params)
    {
        //instance.StopThrusterParticles();
        //instance.CurrentInfo.isShipDown = true;
    }

    public override void Execute()
    {
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, 0f, instance.Stat.acceleration * 100f * Time.deltaTime);
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
    }

    public override void Exit()
    {
    }
}
