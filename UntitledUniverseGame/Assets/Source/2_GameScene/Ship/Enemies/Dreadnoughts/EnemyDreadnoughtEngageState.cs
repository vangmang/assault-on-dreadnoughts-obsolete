﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyDreadnoughtEngageState : State<EnemyDreadnoughtStateMng>
{
    public override Enum GetState
    {
        get { return EnemyDreadnoughtStateMng.ENEMY_DREADNOUGHT_STATES.engage; }
    }

    private Vector3 primaryTarget;
    private float yAngle;
    private float angle;
    private float rotDir;
    private Quaternion recentRot;

    public override void Enter(params object[] o_Params)
    {
        instance.PlayThrusterParticles();
        primaryTarget = Vector3.zero;
        var targetShip = (ShipBase)instance.EnemyDreadnoughtAIData.detectedTarget;
        if (targetShip)
        {
            var pos = instance.Info.Identity.position - targetShip.Info.Identity.position;
            yAngle = Mathf.Atan2(pos.z, pos.x);
        }
        primaryTarget = new Vector3(Mathf.Cos(yAngle) * instance.EnemyDreadnoughtAIData.maintainDistance,
                                    instance.EnemyDreadnoughtAIData.detectedTarget.TargetableTransform.position.y - 500f,
                                    Mathf.Sin(yAngle) * instance.EnemyDreadnoughtAIData.maintainDistance);
        rotDir = yAngle <= 90f && yAngle >= -90f ? 1f : -1f; // true -> left : false -> right
        angle = 0;
        recentRot = instance.Info.ShipTransform.rotation;
    }

    public override void Execute()
    {
        var currentTarget = instance.EnemyDreadnoughtAIData.detectedTarget;
        var dist = (primaryTarget - instance.Info.ShipTransform.position).magnitude;
        var targetShip = (ShipBase)instance.EnemyDreadnoughtAIData.detectedTarget;
        if (targetShip)
        {
            if (dist <= 500f)
            {
                //rotDir = yAngle <= 90f && yAngle >= -90f ? 1f : -1f; // true -> left : false -> right
                angle += 15f;
                var pos = instance.Info.Identity.position - targetShip.Info.Identity.position;
                yAngle = Mathf.Atan2(pos.z, pos.x);
                yAngle += angle * Mathf.Deg2Rad * rotDir;
            }
        }
        var wantedPos = new Vector3(Mathf.Cos(yAngle) * instance.EnemyDreadnoughtAIData.maintainDistance,
                                    currentTarget.TargetableTransform.position.y - 500f,
                                    Mathf.Sin(yAngle) * instance.EnemyDreadnoughtAIData.maintainDistance);
        primaryTarget = wantedPos;

        var targetRot = primaryTarget - instance.Info.ShipTransform.position;
        var yRot = instance.Info.ShipTransform.rotation.eulerAngles.y - recentRot.eulerAngles.y;
        var lookRot = Quaternion.LookRotation(targetRot, instance.Info.ShipTransform.up);
        var wantedRot = lookRot.eulerAngles + new Vector3(0f, 0f, instance.zDirection);
        if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(instance.Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
            wantedRot.z = 0f;
        instance.Info.ShipTransform.rotation =
            Quaternion.Slerp(instance.Info.ShipTransform.rotation, Quaternion.Euler(wantedRot), instance.Stat.mobility * Time.deltaTime);
        instance.CurrentInfo.cur_rotation = Quaternion.Euler(wantedRot);
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, instance.Stat.velocity, instance.Stat.acceleration * Time.deltaTime);
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
        recentRot = instance.Info.ShipTransform.rotation;
        instance.PlayThrusterEffects(2.5f, 0f, 0f, 1f);
    }
    public override void Exit()
    {
        instance.StopThrusterParticles();
    }
}