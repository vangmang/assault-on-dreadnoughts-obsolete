﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyDreadnoughtIdleState : State<EnemyDreadnoughtStateMng>
{
    public override Enum GetState
    {
        get { return EnemyDreadnoughtStateMng.ENEMY_DREADNOUGHT_STATES.idle; }
    }
    public override void Enter(params object[] o_Params)
    {
        //instance.StopThrusterParticles();
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
