﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UniRx;

public class EnemyDreadnoughtStateMng : EnemyShipBase
{
    public enum ENEMY_DREADNOUGHT_STATES
    {
        idle,
        engage,
        down
    }

    [Serializable]
    public struct _ENEMY_DREADNOUGHT_AI_DATA_
    {
        public TargetingModuleStateMng.ITargetable detectedTarget;
        public float maintainDistance;
        public float detectableDistance;
        public float detectableAngle;
        public bool isEngaging;
    }

    [SerializeField] private ParticleSystem[] dreadnoughtThrusterParticles; 
    public DreadnoughtWeaponBase[] dreadnoughtWeaponBases;
    public Light[] additionalLights;
    public _ENEMY_DREADNOUGHT_AI_DATA_ EnemyDreadnoughtAIData;
    public Vector3 originalPos;
    [NonSerialized] public float zDirection;
    private Quaternion recentRot;

    protected override void InitOnAwake()
    {
        InitializeState();
        WeaponLimitElevation = EnemyDreadnoughtAIData.detectableAngle;
        TargetableTransform = Info.ShipTransform;
        isTargetingAlertUndetectable = true;
        brightnessMaxDistance = 9000f;
    }

    private IEnumerator playAdditionalLightEffects()
    {
        while (CurrentInfo.cur_ShieldCapacity > 0f && CurrentInfo.cur_ECCM_capacity > 0f)
        {
            for (int i = 0; i < additionalLights.Length - 1; i += 2)
            {
                additionalLights[i].intensity = 50;
                additionalLights[i + 1].intensity = 5;
                yield return new WaitForSeconds(0.075f);
                additionalLights[i].intensity = 1;
                additionalLights[i + 1].intensity = 1;
            }
            yield return new WaitForSeconds(0.5f);
        }
        foreach (var light in additionalLights)
        {
            light.intensity = 0f;
        }
    }

    private void Start()
    {
        //CreateEnemies.Instance.targets.Add(this);
        StateEnter(ENEMY_DREADNOUGHT_STATES.idle);
        CurrentInfo.cur_ShieldCapacity = Stat.shieldCapacity;
        CurrentInfo.cur_ECCM_capacity = Stat.ECCM_capacity;
        charStateMng = CharStateMng.Instance;
        StartCoroutine(playAdditionalLightEffects());
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_WarpPoint).
            Where(point => CurrentInfo.cur_WarpPoint.Equals(point)).
            Subscribe(point =>
            {
                Info.ShipTransform.localPosition = originalPos;
                point.createEnemies.AddEnmiesIntoLocalArea(this);
            });
        this.ObserveEveryValueChanged(_ => Info.ShipTransform.rotation).
            Subscribe(_ =>
            {
                var yRot = Info.ShipTransform.rotation.eulerAngles.y - recentRot.eulerAngles.y;
                // 0 : forward / 1 : left / -1 : right
                var dir = yRot < 0 ? 1f : -1f;
                if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(Info.ShipTransform.rotation.eulerAngles.x - 180f) <= 135f)
                    dir = 0f;
                var rot = Mathf.Min(Mathf.Abs(yRot) * 0.5f, 1f);
                dir *= rot * 90f;
                zDirection = Mathf.Lerp(zDirection, dir, Stat.mobility * Time.deltaTime);
                recentRot = Info.ShipTransform.rotation;
            });
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_WarpPoint).
            Where(_ => charStateMng.CurrentInfo.cur_WarpPoint != null).
            Subscribe(_ =>
            {
                Info.ShipTransform.gameObject.SetActive(CurrentInfo.cur_WarpPoint.Equals(charStateMng.CurrentInfo.cur_WarpPoint));
            });
        this.ObserveEveryValueChanged(_ => CurrentInfo.cur_ECCM_capacity).
            Where(capacity => capacity <= 0f).
            Subscribe(_ =>
            {
                ShipDown();
            });
        InitCheck = true;
        createEnemies.Updates.Add(dreadnoughtUpdate);
        StartCoroutine(enableCollider());
    }

    private IEnumerator enableCollider()
    {
        yield return new WaitForSeconds(3f);
        Info.ShipCollider.enabled = true;
    }

    private void dreadnoughtUpdate()
    {
        Execute();
        DetectTarget();
    }

    public override void TakeECMdamage(ProjectileStateMng projectile)
    {
        var moduleCount = Mathf.Clamp(dreadnoughtWeaponBases.Length, 1f, 5f);
        var damage = Stat.ECCM_capacity / moduleCount + 10f;
        CurrentInfo.cur_ECCM_capacity -= damage;
    }

    public override void ShipDown()
    {
        createEnemies.Updates.Remove(dreadnoughtUpdate);
        gameMng.enableJumpPortalGeneration = true;
        EnemyManager.Instance.targets.Remove(this);
        CurrentInfo.cur_ShieldCapacity = 0f;
        StateTransition(ENEMY_DREADNOUGHT_STATES.down);
        //PlayShipExplosionEffect();
    }

    public void DetectTarget()
    {
        var enemyList = Info.TargetingModuleStateMng.EnemyList;
        // 거리 우선순위로 타겟선별
        TargetingModuleStateMng.ITargetable target = null;
        try
        {
            target = enemyList.Where(_target => (_target.TargetableTransform.position - Info.ShipTransform.position).magnitude <= EnemyDreadnoughtAIData.detectableDistance).First();
        }
        catch { target = null; }
        if (target != null)
        {
            EnemyDreadnoughtAIData.detectedTarget = target;
            if ((Info.TargetingModuleStateMng.currentTarget != null || Info.TargetingModuleStateMng.IsInRange) &&
                !EnemyDreadnoughtAIData.isEngaging)
            {
                if (!GetCurrentState.Equals(ENEMY_DREADNOUGHT_STATES.engage))
                {
                    EnemyDreadnoughtAIData.isEngaging = true;
                    StateTransition(ENEMY_DREADNOUGHT_STATES.engage);
                }
            }
            else if (!Info.TargetingModuleStateMng.IsInRange)
            {
                EnemyDreadnoughtAIData.isEngaging = false;
                StateTransition(ENEMY_DREADNOUGHT_STATES.idle);
            }
        }
        else
        {
            StateTransition(ENEMY_DREADNOUGHT_STATES.idle);
        }
    }

    /// <summary>
    /// 드레드넛은 트레일 렌더러 없다.
    /// </summary>
    /// <param name="brightness"></param>
    /// <param name="trailWidth"></param>
    /// <param name="trailTime"></param>
    /// <param name="volume"></param>
    public override void PlayThrusterEffects(float brightness, float trailWidth, float trailTime, float volume)
    {
        float maxBrightness = brightness;
        bool zeroBrightness = brightness == 0f;
        brightness *= (((DistanceWithMainCamera / brightnessMaxDistance) - 1) * -1f); // 카메라 거리에 따른 쓰러스터 밝기 조절
        var wantedBrightness = zeroBrightness ? 0f : Mathf.Clamp(brightness, 0.1f, maxBrightness);
        Info.ThrusterEffects.ForEach(flare =>
        {
            flare.brightness = Mathf.Lerp(flare.brightness, wantedBrightness, 1.5f * Time.deltaTime);
        });
        //Info.EngineAudio.Audio.volume = Mathf.Lerp(Info.EngineAudio.Audio.volume, volume, Stat.acceleration * Time.deltaTime);
    }

    public void PlayThrusterParticles()
    {
        foreach (var particle in dreadnoughtThrusterParticles)
            particle.Play();
    }

    public void StopThrusterParticles()
    {
        foreach (var particle in dreadnoughtThrusterParticles)
            particle.Stop();
    }

    private void OnDestroy()
    {
        
    }
}
