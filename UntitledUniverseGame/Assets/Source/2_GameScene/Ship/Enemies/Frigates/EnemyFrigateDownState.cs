﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFrigateDownState : State<EnemyFrigateStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.down; }
    }

    public override void Enter(params object[] o_Params)
    {
        //instance.StopThrusterParticles();
        StartCoroutine(waitForParticlePlaying());
    }

    private IEnumerator waitForParticlePlaying()
    {
        var particleHalfLength = instance.Info.ShipExplosionParticle.main.duration * 0.05f;
        yield return new WaitForSeconds(particleHalfLength);
        instance.RemoveAudios();
        yield return new WaitForFixedUpdate();
        Destroy(instance.Info.ShipTransform.parent.parent.gameObject);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }

}
