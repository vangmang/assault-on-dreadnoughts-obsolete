﻿using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFrigateEngageState : State<EnemyFrigateStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.engage; }
    }

    private Vector3 primaryTarget;
    private Vector3 secondaryTarget;
    private Vector3 thirdTarget;
    private bool isAttackEnd;
    private bool evasion;
    private float evasionTime;
    private float time = 0f;
    private float endTime = 0f;
    private bool evasionTimeEnable;
    private float endEvasionTime;

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
        try
        {
            primaryTarget = instance.EnemyFrigateAIData.detectedTarget.TargetableTransform.position;
            secondaryTarget = primaryTarget;
            isAttackEnd = false;
            evasionTime = 0f;
            time = 0f;
            endTime = instance.EnemyFrigateAIData.optimizedEngageDistance / instance.Stat.velocity;
            evasionTimeEnable = false;
            endEvasionTime = 0f;
        }
        catch (NullReferenceException) { }
    }


    public override void Execute()
    {
        var target = instance.EnemyFrigateAIData.detectedTarget;
        try
        {
            if (target == null)
            {
                return;
            }
            var inversePos = instance.Info.ShipTransform.InverseTransformPoint(target.TargetableTransform.position);
            var y_axis = Mathf.Atan2(inversePos.z, inversePos.x) * Mathf.Rad2Deg;
            var evasionDist = instance.Stat.velocity * 0.25f;
            if (evasion)
            {
                var distWithObstacle = (instance.Info.ShipTransform.position - primaryTarget).magnitude;
                evasionTime += Time.deltaTime;
                if (!evasionTimeEnable)
                {
                    endEvasionTime = distWithObstacle / instance.Stat.velocity;
                    evasionTimeEnable = true;
                }
                if (distWithObstacle <= evasionDist || evasionTime >= endEvasionTime)
                {
                    evasionTime = 0f;
                    evasion = false;
                }
            }
            else
            {
                evasionTimeEnable = false;
                if (y_axis <= 180f && y_axis >= 0f && !isAttackEnd)
                {
                    var yAxisComparer = y_axis > 90f; // 90보다 크면 오른쪽 후방, 작으면 왼쪽 후방에 위치해 있다.
                    secondaryTarget = target.TargetableTransform.position + (instance.Info.ShipTransform.right * (yAxisComparer ? evasionDist : -evasionDist));
                    if (!evasion)
                        primaryTarget = secondaryTarget;
                    thirdTarget = instance.Info.ShipTransform.forward * instance.EnemyFrigateAIData.optimizedEngageDistance;
                }
                else // 전방에 있을 때
                {
                    if (!isAttackEnd)
                    {
                        var distWithPrimary = (primaryTarget - instance.Info.ShipTransform.position).magnitude;
                        // 공격도중 플레이어가 프리깃을 바라보고 있는데 회피 사정거리안에 들면 회피해야 한다.
                        if (distWithPrimary >= instance.EnemyFrigateAIData.distanceForAvoidance)
                        {
                            primaryTarget = secondaryTarget;
                        }
                        else
                        {
                            isAttackEnd = true;
                            primaryTarget = thirdTarget;
                        }
                    }
                }
                if (isAttackEnd)
                {
                    if (!evasion)
                    {
                        primaryTarget = thirdTarget;
                        time += Time.deltaTime;
                    }
                    var distWithEnd = (instance.Info.ShipTransform.position - primaryTarget).magnitude;

                    if (time >= endTime || distWithEnd <= evasionDist)
                    {
                        instance.StateTransition(EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.seekTargetBehind);
                    }
                }
            }
            instance.DetectObstacle(ref evasion, ref primaryTarget);
            instance.Move(primaryTarget);
        }
        catch
        {
            instance.StateTransition(EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.idle);
        }
    }


    public override void Exit()
    {

    }

    private void OnDestroy()
    {

    }
}
