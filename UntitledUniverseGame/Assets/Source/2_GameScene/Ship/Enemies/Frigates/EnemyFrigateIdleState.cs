﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using Unity.Collections;

public class EnemyFrigateIdleState : State<EnemyFrigateStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.idle; }
    }

    public override void Enter(params object[] o_Params)
    {
        //instance.StopThrusterParticles();
    }

    public override void Execute()
    {
        instance.CurrentInfo.cur_velocity =
            Mathf.Lerp(instance.CurrentInfo.cur_velocity, 0f, instance.Stat.acceleration * Time.deltaTime);
        // 이동
        var rot = instance.Info.ShipTransform.rotation;
        rot.z = 0f;
        instance.Info.ShipTransform.rotation = Quaternion.Slerp(instance.Info.ShipTransform.rotation, rot, instance.Stat.mobility * Time.deltaTime);
        instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.CurrentInfo.cur_velocity * Time.deltaTime;
        instance.PlayThrusterEffects(0f, 0f, 0f, 0f);
        //instance.PlayThrusterEffect(0f, 0f);
    }

    public override void Exit()
    {
    }
}
