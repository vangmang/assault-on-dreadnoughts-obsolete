﻿using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFrigateSeekTargetState : State<EnemyFrigateStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.seekTarget; }
    }

    private Vector3 primaryTarget;
    private Vector3 secondaryTarget;
    private bool evasion;
    private float evasionTime;
    private float time;
    private float endTime;

    public override void Enter(params object[] o_Params)
    {
        //instance.PlayThrusterParticles();
        evasionTime = 0f;
        var detectedTarget = instance.EnemyFrigateAIData.detectedTarget;
        secondaryTarget = detectedTarget.TargetableTransform.position +
                        (instance.Info.ShipTransform.position - detectedTarget.TargetableTransform.position).normalized *
                        -instance.EnemyFrigateAIData.optimizedEngageDistance;
        primaryTarget = secondaryTarget;
        evasionTime = 0f;
        time = 0f;
        evasionTime = 0f;
        endTime = instance.EnemyFrigateAIData.optimizedEngageDistance * 2f / instance.Stat.velocity;
    }

    public override void Execute()
    {
        var evasionDist = instance.Stat.velocity * 0.25f;
        if (evasion)
        {
            var distWithObstacle = (instance.Info.ShipTransform.position - primaryTarget).magnitude;
            evasionTime += Time.deltaTime;
            if (distWithObstacle <= evasionDist || evasionTime >= 3f)
            {
                evasionTime = 0f;
                evasion = false;
            }
        }
        else
        {
            time += Time.deltaTime;
            primaryTarget = secondaryTarget;
            float dist = (primaryTarget - instance.Info.ShipRigidbody.position).magnitude;

            if (time >= endTime || dist <= instance.Stat.velocity * 0.2f)
            {
                // 교전 가능 거리에 타겟이 있는지 체크하고 상태 전이를 실시한다.
                EnemyFrigateStateMng.ENEMY_FRIGATE_STATES state =
                    instance.Info.TargetingModuleStateMng.IsInRange ? EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.engage : EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.seekTargetBehind;
                instance.StateTransition(state);
                return;
            }
            if (instance.Info.TargetingModuleStateMng.currentTarget != null)
            {
                instance.StateTransition(EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.engage);
                return;
            }
        }
        instance.DetectObstacle(ref evasion, ref primaryTarget);
        instance.Move(primaryTarget);
    }

    public override void Exit()
    {
    }
}
