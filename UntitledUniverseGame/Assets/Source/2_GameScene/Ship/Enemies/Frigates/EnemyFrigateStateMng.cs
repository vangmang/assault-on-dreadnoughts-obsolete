﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using System.Linq;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Jobs;

public class EnemyFrigateStateMng : EnemyShipBase
{

    public enum ENEMY_FRIGATE_STATES
    {
        idle,
        engage,
        warpArrival,
        seekTarget,
        seekTargetBehind,
        down,
    }

    [Serializable]
    public struct _ENEMY_FRIGATE_AI_DATA_
    {
        public TargetingModuleStateMng.ITargetable detectedTarget;
        public float maintainDistance;
        public float detectableDistance;
        public float detectableAngle;
        public float optimizedEngageDistance;
        public float distanceForAvoidance;// 타겟의 전방에 있을 때 이거리에 있으면 피해야함.
        public bool isEngaging;
    }

    [NonSerialized] public float zDirection;
    [NonSerialized] public Vector3 originalPos;
    [SerializeField] private float weaponLimitElevation;
    private Quaternion recentRot;
    private Quaternion recentRotChecker;
    private TargetingModuleStateMng.ITargetable target;

    public _ENEMY_FRIGATE_AI_DATA_ EnemyFrigateAIData;
    public SoundMngAssistant DestinationArrivalAudio;
    public SoundMngAssistant DestinationArrivalAudio2;

    private TransformAccessArray transformAccessArray;
    private NativeArray<Quaternion> lookRot;
    private NativeArray<float> currentVelocity;
    private JobHandle jobHandle;

    protected override void InitOnAwake()
    {
        InitializeState();
        lookRot = new NativeArray<Quaternion>(1, Allocator.Persistent);
        currentVelocity = new NativeArray<float>(1, Allocator.Persistent);
        TargetableTransform = Info.ShipTransform;
        brightnessMaxDistance = 1200f;
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        EnemyFrigateAIData.isEngaging = false;
        WeaponLimitElevation = weaponLimitElevation;
        charStateMng = CharStateMng.Instance;
        yield return new WaitUntil(() => charStateMng.CurrentInfo.cur_WarpPoint != null);
        TargetingAlert.charStateMng = charStateMng;
        InitInstantiation();
        transformAccessArray = new TransformAccessArray(ParallelForTransform);

        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_WarpPoint).
            Where(point => CurrentInfo.cur_WarpPoint.Equals(point)).
            Subscribe(point =>
            {
                if (overrideInitPos)
                    return;
                Info.ShipTransform.localPosition = originalPos;
                point.createEnemies.AddEnmiesIntoLocalArea(this);
            });
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_WarpPoint).
            Where(_ => charStateMng.CurrentInfo.cur_WarpPoint != null).
            Subscribe(_ =>
            {
                Info.ShipTransform.gameObject.SetActive(CurrentInfo.cur_WarpPoint.Equals(charStateMng.CurrentInfo.cur_WarpPoint));
                try // MissingReferecne
                {
                    indicator.gameObject.SetActive(CurrentInfo.cur_WarpPoint.Equals(charStateMng.CurrentInfo.cur_WarpPoint));
                }
                catch { }
                //StartCoroutine(disableEnemy());
            });
        //Observable.EveryUpdate().
        //    Subscribe(_ => { FrigateUpdate(); }).AddTo(this);
        StateEnter(ENEMY_FRIGATE_STATES.idle);
        InitCheck = true;
        yield return new WaitForFixedUpdate();
        Info.ThrusterTrails.ForEach(trail =>
        {
            trail.gameObject.SetActive(true);
        });
        Info.ThrusterEffects.ForEach(thruster =>
        {
            thruster.gameObject.SetActive(true);
        });
        createEnemies.Updates.Add(frigateUpdate);
        //StartCoroutine(enableCollider());
    }

    public override void OverrideIdentify()
    {
        StartCoroutine(invokeOverrideIdentify());
    }

    public override void DirectWarpArrival()
    {
        overrideInitPos = true;
        StartCoroutine(invokeDirectWarpArrival());
    }

    private IEnumerator invokeOverrideIdentify()
    {
        yield return new WaitUntil(() => this.indicator != null);
        var indicator = this.indicator.GetComponent<Indicator>();
        indicator.textList[0].text = "Ally";
        indicator.textList[1].text = "Frigate";
    }

    private IEnumerator invokeDirectWarpArrival()
    {
        yield return new WaitUntil(() => InitCheck);
        StateTransition(ENEMY_FRIGATE_STATES.warpArrival);
    }

    //private IEnumerator enableCollider()
    //{
    //    yield return new WaitForSeconds(3f);
    //    Info.ShipCollider.enabled = true;
    //}

    public override void TakeDamage(object[] o_Params)
    {
        float damage = (float)o_Params[0];
        ShipBase shipBase = (ShipBase)o_Params[1];
        CurrentInfo.cur_ShieldCapacity -= damage;
        if (shipBase == charStateMng)
            charStateMng.CharacterFeatureInfo.hitEffectPlayer.PlayHitEffect();
    }

    public override void TakeDamage(ProjectileStateMng projectile)
    {
        CurrentInfo.cur_ShieldCapacity -= projectile.CurrentInfo.damage;
        if (projectile.WeaponStateMng.getShipBase == charStateMng)
            charStateMng.CharacterFeatureInfo.hitEffectPlayer.PlayHitEffect();
    }

    public override void ShipDown()
    {
        createEnemies.Updates.Remove(frigateUpdate);
        // 현상금 중복 획득 방지
        if (Info.ShipCollider.enabled)
            gameMng.Goods.bountyAmount += bounty;
        Info.ShipCollider.enabled = false;
        StateTransition(ENEMY_FRIGATE_STATES.down);
        RemoveTarget();
        charStateMng.PlayEnemyDestroyedTextAnim();
        // 타겟 리스트에서 삭제
        EnemyManager.Instance.targets.Remove(this);
        // 인디케이터 삭제
        try
        {
            Destroy(targetingAlert.PointIndicator.gameObject);
            Destroy(targetingAlert.gameObject);
        }
        catch { }
        try
        {
            Destroy(IndicatorTransform.gameObject);
        }
        catch { }
        PlayShipExplosionEffect();
    }

    public override void RemoveAudios()
    {
        gameMng.soundMng.RemoveAudio(Info.EngineAudio);
        gameMng.soundMng.RemoveAudio(Info.ShipExplosionAudio);
        gameMng.soundMng.RemoveAudio(Info.WeaponLaunchAudio);
        gameMng.soundMng.RemoveAudio(DestinationArrivalAudio);
        gameMng.soundMng.RemoveAudio(DestinationArrivalAudio2);
    }

    private void frigateUpdate()
    {
        Info.Identity.rotation = Quaternion.identity;
        var yRot = Info.ShipRigidbody.rotation.eulerAngles.y - recentRot.eulerAngles.y;
        // 0 : forward / 1 : left / -1 : right
        var dir = yRot < 0 ? 1f : -1f;
        if (Mathf.Abs(yRot) < 0.5f || Mathf.Abs(Info.ShipRigidbody.rotation.eulerAngles.x - 180f) <= 135f)
            dir = 0f;
        var rot = Mathf.Min(Mathf.Abs(yRot) * 0.5f, 1f);
        dir *= rot * 90f;
        zDirection = Mathf.Lerp(zDirection, dir, Stat.mobility * Time.deltaTime);
        recentRot = Info.ShipRigidbody.rotation;

        Execute();
        DetectTarget();
        UpdateAudioEnable(DistanceWithPlayer);
    }

    public void DetectObstacle(ref bool evasion, ref Vector3 primaryTarget)
    {
        RaycastHit hit;
        if (Physics.Raycast(Info.ShipTransform.position, Info.ShipTransform.forward, out hit, Stat.velocity * 2f))
        {
            evasion = true;
            var collider = hit.collider.transform;
            var yPosComparer = Info.ShipTransform.position.y > collider.position.y;
            primaryTarget = hit.point + (Info.ShipTransform.up * (hit.collider.bounds.size.magnitude * (yPosComparer ? 1f : -1f)));
        }
    }

    public void DetectTarget()
    {
        if (GetCurrentState.Equals(ENEMY_FRIGATE_STATES.down) || CurrentInfo.isWarpin)
            return;
        var enemyList = Info.TargetingModuleStateMng.EnemyList;

        // 거리 우선순위로 타겟선별
        //try
        //{
        if (target == null && enemyList.Count > 0)
        {
            target = enemyList.Where(_target => (_target.TargetableTransform.position - Info.ShipTransform.position).magnitude <= EnemyFrigateAIData.detectableDistance).
                OrderBy(_target => (_target.TargetableTransform.position - Info.ShipTransform.position).magnitude).First();
        }
        //}
        //catch { target = null; }
        // 최적화 시급 
        if (target != null)
        {
            ShipBase targetShip = target as ShipBase;
            if (targetShip)
            {
                if (targetShip.CurrentInfo.cur_ShieldCapacity <= 0f)
                {
                    target = null;
                    EnemyFrigateAIData.detectedTarget = null;
                    return;
                }
            }
            EnemyFrigateAIData.detectedTarget = target;
            if ((Info.TargetingModuleStateMng.currentTarget != null || Info.TargetingModuleStateMng.IsInRange) && !EnemyFrigateAIData.isEngaging)
            {
                if (!GetCurrentState.Equals(ENEMY_FRIGATE_STATES.engage))
                {
                    EnemyFrigateAIData.isEngaging = true;
                    StateTransition(ENEMY_FRIGATE_STATES.engage);
                }
            }
            else if (!Info.TargetingModuleStateMng.IsInRange)
            {
                EnemyFrigateAIData.isEngaging = false;
                if (!GetCurrentState.Equals(ENEMY_FRIGATE_STATES.seekTarget))
                    StateTransition(ENEMY_FRIGATE_STATES.seekTarget);
            }
        }
        else
        {
            if (!GetCurrentState.Equals(ENEMY_FRIGATE_STATES.idle))
                StateTransition(ENEMY_FRIGATE_STATES.idle);
        }
    }

    public void Move(Vector3 primaryTarget)
    {
        // 이동 잡 
        currentVelocity[0] = CurrentInfo.cur_velocity;
        MovementJob movementJob = new MovementJob()
        {
            primaryTarget = primaryTarget,
            up = Info.ShipTransform.up,
            forward = Info.ShipTransform.forward,
            recentRot = recentRotChecker,
            zDirection = zDirection,
            mobility = Stat.mobility,
            velocity = Stat.velocity,
            accelration = Stat.acceleration,
            lookRot = lookRot,
            currentVelocity = currentVelocity,
            deltaTime = Time.deltaTime
        };
        jobHandle = movementJob.Schedule(transformAccessArray);
        JobHandle.ScheduleBatchedJobs();
        jobHandle.Complete();
        CurrentInfo.cur_rotation = lookRot[0];
        CurrentInfo.cur_velocity = currentVelocity[0];
        recentRotChecker = Info.ShipRigidbody.rotation;
        if (GetCurrentState.Equals(ENEMY_FRIGATE_STATES.idle))
            PlayThrusterEffects(0f, 0f, 0f, 0f);
        else
            PlayThrusterEffects(1f, 13f, 0.75f, 1f);
    }

    [Unity.Burst.BurstCompile]
    private struct MovementJob : IJobParallelForTransform
    {
        public Vector3 primaryTarget;
        public Vector3 up;
        public Vector3 forward;
        public Quaternion recentRot;
        public float zDirection;
        public float mobility;
        public float velocity;
        public float accelration;
        public NativeArray<Quaternion> lookRot;
        public NativeArray<float> currentVelocity;
        [ReadOnly] public float deltaTime;

        public void Execute(int index, TransformAccess transformAccess)
        {
            var pos = primaryTarget - transformAccess.position;
            var yRot = transformAccess.rotation.eulerAngles.y - recentRot.eulerAngles.y;
            lookRot[0] = Quaternion.LookRotation(pos, up);
            var wantedRot = lookRot[0].eulerAngles + new Vector3(0f, 0f, zDirection);
            if (Mathf.Abs(yRot) < 0.25f || Mathf.Abs(transformAccess.rotation.eulerAngles.x - 180f) <= 135f)
                wantedRot.z = 0f;
            transformAccess.rotation = Quaternion.Lerp(transformAccess.rotation, Quaternion.Euler(wantedRot), mobility * deltaTime);
            currentVelocity[0] = Mathf.Lerp(currentVelocity[0], velocity, accelration * deltaTime);
            transformAccess.position += forward * currentVelocity[0] * deltaTime;
        }
    }

    private void OnDestroy()
    {
        lookRot.Dispose();
        transformAccessArray.Dispose();
        currentVelocity.Dispose();
    }
}