﻿using FiniteStateMachine;
using System.Collections;
using UnityEngine;
using System;

public class EnemyFrigateWarpArrivalState : State<EnemyFrigateStateMng>
{
    private bool warpDriveArrivalCheck;
    public override Enum GetState
    {
        get { return EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.warpArrival; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.CurrentInfo.isWarpin = true;
        StartCoroutine(invokeWarpArrival());
    }

    private IEnumerator invokeWarpArrival()
    {
        float t = 0f;
        var dir = (instance.CurrentInfo.cur_WarpPoint.currentPlanet.planet.position - instance.gameMng.stationPoint.currentPlanet.planet.position).normalized;
        var start = instance.Info.ShipTransform.position;
        var end = instance.overrideWarpTargetPos;
        var lookRot = Quaternion.LookRotation(dir);
        instance.Info.ShipTransform.rotation = lookRot;
        while(t < 0.995f)
        {
            t += 0.25f * Time.deltaTime;
            instance.PlayThrusterEffects(1f, 13f, 0.75f, 1f);
            //instance.PlayThrusterEffect(13f, 1f);
            if(t > 0.875f && !warpDriveArrivalCheck)
            {
                warpDriveArrivalCheck = true;
                instance.DestinationArrivalAudio.Audio.Play();
            }
            instance.Info.ShipTransform.localPosition = Vector3.Lerp(start, end, t);
            yield return null;
        }
        t = 0f;
        instance.DestinationArrivalAudio2.Audio.Play();
        while (t <= 4f)
        {
            t += Time.deltaTime;
            instance.PlayThrusterEffects(1f, 13f, 0.75f, 1f);
            //instance.PlayThrusterEffect(13f, 1f);
            instance.Info.ShipTransform.position += instance.Info.ShipTransform.forward * instance.Stat.velocity * Time.deltaTime;
            yield return null;
        }
        instance.StateTransition(EnemyFrigateStateMng.ENEMY_FRIGATE_STATES.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
        instance.CurrentInfo.cur_WarpPoint.createEnemies.AddEnmiesIntoLocalArea(instance);
        instance.CurrentInfo.isWarpin = false;
    }
}
