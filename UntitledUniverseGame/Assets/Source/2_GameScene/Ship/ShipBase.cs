﻿using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UniRx;
using System;
using UnityEngine.UI;
using UnityEngine.Jobs;

public abstract class ShipBase : StateMng, TargetingModuleStateMng.ITargetable, IDamaged
{
    public enum SHIP_TYPE
    {
        frigate,
        assaultFrigate,
        anti_vessel_specialized_AssaultShip,
        ecmShip,
        fighter,
        crusier,
        dreadnought,
    }

    public enum IDENTIFY
    {
        ally,
        hostile,
        neutral
    }

    [SerializeField] private IDENTIFY identify;
    public IDENTIFY Identify { get => identify; set { identify = value; } }

    [SerializeField] protected Image indicatorImage;
    [SerializeField] protected Text lockOnStateText;
    [SerializeField] protected Text distanceText;
    [SerializeField] protected Transform indicator;
    public Image IndicatorImage => indicatorImage; 
    public Text LockStateText => lockOnStateText; 
    public Text DistanceText => distanceText; 
    public Transform IndicatorTransform => indicator; 


    [Serializable]
    public struct _UPGRADE_TIMES_STAT_
    {
        public int velocity;
        public int acceleration;
        public int mobility;
        public int shieldCapacity;
        public int power;
        public int weaponRange;
        public int weaponDamage;
        public int weaponFireRate;

        public static _UPGRADE_TIMES_STAT_ operator +(_UPGRADE_TIMES_STAT_ upgradeStat, SavedData.SavedDataContainer._UPGRADE_TIMES_STAT_ savedUpgradeStat)
        {                                                                               // Base
            upgradeStat.velocity = savedUpgradeStat.velocity;                 // 400
            upgradeStat.acceleration = savedUpgradeStat.acceleration;           // 1
            upgradeStat.mobility = savedUpgradeStat.mobility;         // 1.5
            upgradeStat.power = savedUpgradeStat.power;                    // 100
            upgradeStat.shieldCapacity = savedUpgradeStat.shieldCapacity;           // 200
            upgradeStat.weaponRange = savedUpgradeStat.weaponRange;     // 4000
            upgradeStat.weaponDamage = savedUpgradeStat.weaponDamage;         // 15
            upgradeStat.weaponFireRate = savedUpgradeStat.weaponFireRate;   // 0.4
            return upgradeStat;
        }
    }

    [Serializable]
    public struct _STAT_
    {
        public enum STAT
        {
            velocity,
            acceleration,
            mobility,
            shieldCapacity,
            power,
            weaponRange,
            weaponDamage,
            weaponFireRate
        }

        public float velocity;           // 최대 속력
        public float acceleration;       // 최대 가속력 (이동)
        public float mobility;           // 최대 기동성 (회전)
        public float shieldCapacity;     // 힛포인트 (체력)
        public float power;              // 동력 (스태미나)
        public float weaponRange;        // 동력 (스태미나)
        public float weaponDamage;       // 동력 (스태미나)
        public float weaponFireRate;     // 동력 (스태미나)

        public float evacutationDistance; // 최대 회피 거리
        public float ECCM_capacity;// 대전자전
        public float powerRecoveryRate; // 동력 재생량
        public float powerRecoveryDelay;// 동력 사용 후 재생 대기 시간
        public float AfterburnerVelocity => velocity * 1.5f;// 애프터버너
    }
    [Serializable]
    public struct _CONSUMABLE_STAT_
    {
        [SerializeField] private float evasionConsumption;
        [SerializeField] private float afterburnerConsumption;
        [SerializeField] private float attackConsumption;
        public float EvasionConsumpion { get { return evasionConsumption; } }
        public float AfterburnerConsumption { get { return afterburnerConsumption; } }
        public float AttackConsumption { get { return attackConsumption; } }
        [NonSerialized]
        public float overallAttackConsumption;
    }
    [Serializable]
    public struct _CURRENT_INFO_
    {
        public Stage recentStage;
        public Stage cur_Stage;
        public WarpPoint cur_WarpPoint;
        public float cur_velocity; // 현재 속력
        public float cur_warpVelocity; // 현재 워프 속도
        public float cur_ShieldCapacity; // 현재 쉴드
        public float cur_power; // 현재 동력
        public float cur_ECCM_capacity; // 현재 대전자전 수치
        public Quaternion cur_rotation; // 현재 회전값
        public bool isPowerRecoveryEnable; // 파워 재생 가능 여부
        public bool isPowerRecovering; // 파워 재생 중인지 여부
        public bool isWarpin;
        public bool preLoadAfterWarp;
        public LauncherFormAbility launcherFormAbility;
        public ActiveFormAbility activeFormAbility;
    }
    [Serializable]
    public struct _INFO_
    {
        [SerializeField] private SHIP_TYPE shipType;
        [SerializeField] private Transform shipTransform;
        [SerializeField] private Rigidbody shipRigidbody;
        [SerializeField] private Collider shipCollider;
        [SerializeField] private ParticleSystem sailEffect;
        [SerializeField] private List<LensFlare> thrusterEffects;
        [SerializeField] private List<TrailRenderer> thrusterTrails;
        [SerializeField] private SoundMngAssistant engineAudio;

        [SerializeField] private ParticleSystem shipExplosionParticle;
        [SerializeField] private SoundMngAssistant shipExplosionAudio;

        [SerializeField] private SoundMngAssistant weaponLaunchAudio;
        [SerializeField] private WeaponBase[] weaponBaseArr;
        [SerializeField] private TargetingModuleStateMng targetingModuleStateMng;
        [SerializeField] private Transform identity; // 회전각 비교할 때 사용할 놈
        public SHIP_TYPE ShipType { get { return shipType; } }
        public Transform ShipTransform { get { return shipTransform; } }
        public Rigidbody ShipRigidbody { get { return shipRigidbody; } }
        public Collider ShipCollider { get { return shipCollider; } }
        public ParticleSystem SailEffect { get { return sailEffect; } }
        public ParticleSystem ShipExplosionParticle { get { return shipExplosionParticle; } }
        public List<LensFlare> ThrusterEffects { get { return thrusterEffects; } }
        public List<TrailRenderer> ThrusterTrails { get { return thrusterTrails; } }
        public SoundMngAssistant EngineAudio { get { return engineAudio; } }
        public SoundMngAssistant ShipExplosionAudio { get { return shipExplosionAudio; } }
        public SoundMngAssistant WeaponLaunchAudio { get { return weaponLaunchAudio; } }
        public WeaponBase[] WeaponBaseArr { get { return weaponBaseArr; } }
        public TargetingModuleStateMng TargetingModuleStateMng { get { return targetingModuleStateMng; } }
        public Transform Identity { get { return identity; } }
    }
    [Serializable]
    public struct _WARP_
    {
        public enum WARP_TYPE
        {
            longRangeWarp,              // 장거리 워프 -> 행성에서 행성간 워프
            microWormholePenetation     // 단거리 워프 -> 포인트에서 포인트간 워프
        }
        [SerializeField] private float warpAcceleration;
        [SerializeField] private float warpAlignTime;
        [SerializeField] private float warpVelocity;
        [SerializeField] private AudioSource warpBeginAudio;
        [SerializeField] private AudioSource warpEnterAudio;
        [SerializeField] private AudioSource warpMainAudio;

        [SerializeField] private float microWormholePenetrationAlignTime;
        [SerializeField] private AudioSource microWormholePenetrationBeginAudio;
        [SerializeField] private AudioSource microWormholePenetrationEnterAudio;

        public float WarpAcceleration { get { return warpAcceleration; } }
        public float WarpAlignTime { get { return warpAlignTime; } }
        public float WarpVelocity { get { return warpVelocity; } }
        public AudioSource WarpBeginAudio { get { return warpBeginAudio; } }
        public AudioSource WarpEnterAudio { get { return warpEnterAudio; } }
        public AudioSource WarpMainAudio { get { return warpMainAudio; } }
        public AudioSource MicroWormholePenetrationBeginAudio { get { return microWormholePenetrationBeginAudio; } }
        public AudioSource MicroWormholePenetrationEnterAudio { get { return microWormholePenetrationEnterAudio; } }
        public float MicroWormholePenetrationAlignTime { get { return microWormholePenetrationAlignTime; } }

        public static readonly float warpConstant = 350000f;
        public WarpablePlace currentWarpTarget;
        public WarpablePlace targetedWarpTarget;
        public bool isWarpHighlight;
        [NonSerialized] public bool enableMicroWormholePenetration;
        public event EventHandler<WarpEventArgs> WarpEvent;
    }
    [Serializable]
    public struct _TARGETING_
    {
        public float fallOffRange; // 공격 가능 거리
        public float optimalRange; // 공격 최적 거리
        public float optimalZone; // 공격 최적 최단 거리
    }

    public _UPGRADE_TIMES_STAT_ UpgradeTimesStat;
    public _STAT_ Stat;
    public _CONSUMABLE_STAT_ ConsumableStat;
    public _CURRENT_INFO_ CurrentInfo;
    public _INFO_ Info;
    public _WARP_ Warp;
    public _TARGETING_ Targeting;

    public Transform[] ParallelForTransform { get; protected set; }
    public bool isTargetingAlertUndetectable { get; protected set; }
    public float getCurrentHitpoints { get { return CurrentInfo.cur_ShieldCapacity; } }
    public Transform TargetableTransform { get; protected set; }
    public bool IsCapturable { get => !CurrentInfo.isWarpin; }
    public TargetingModuleStateMng.TARGET_TYPE TargetType { get => TargetingModuleStateMng.TARGET_TYPE.ship; }
    public float CurrentVelocity => CurrentInfo.cur_velocity;

    protected float t;
    public float WeaponLimitElevation { get; protected set; }
    public GameMng gameMng;
    [NonSerialized] public bool overrideInitPos;
    [NonSerialized] public Vector3 overrideWarpTargetPos;

    public float OriginVelocity { get; private set; }

    public Transform[] LockStateTextTransform { get; protected set; }
    public Transform[] DistanceTextTransform { get; protected set; }

    private void Awake()
    {
        OriginVelocity = Stat.velocity;

        InitOnAwake();
        CurrentInfo.isPowerRecoveryEnable = true;
        CurrentInfo.cur_ShieldCapacity = Stat.shieldCapacity;
        CurrentInfo.cur_power = Stat.power;
        ConsumableStat.overallAttackConsumption = ConsumableStat.AttackConsumption * Info.WeaponBaseArr.Length;
        this.ObserveEveryValueChanged(_ => CurrentInfo.cur_ShieldCapacity).
            Where(hp => hp <= 0f).
            Subscribe(_ =>
            {
                ShipDown();
            });
    }

    protected virtual void InitOnAwake() { }
    public abstract void ShipDown();
    public virtual void RemoveAudios() { }

    /// <summary>
    /// 리플렉션 순서: damage, shipBase
    /// </summary>
    /// <param name="o_Params"></param>
    public virtual void TakeDamage(object[] o_Params) { }
    public virtual void TakeDamage(ProjectileStateMng projectile) { }
    public virtual void TakeECMdamage(ProjectileStateMng projectile) { }
    public void TakeAbnormalStateFromEnemy(Action<ShipBase> action)
    {
        action(this);
    }

    public virtual void PlayThrusterEffects(float brightness, float trailWidth, float trailTime, float volume)
    {
        Info.ThrusterTrails.ForEach((trail) =>
        {
            trail.startWidth = Mathf.Lerp(trail.startWidth, trailWidth, Stat.acceleration * 5f * Time.deltaTime);
            trail.time = Mathf.Lerp(trail.time, trailTime, Stat.acceleration * 5f * Time.deltaTime);
        });
        Info.ThrusterEffects.ForEach(flare =>
        {
            flare.brightness = Mathf.Lerp(flare.brightness, brightness, Stat.acceleration * Time.deltaTime);
        });
        Info.EngineAudio.Audio.volume = Mathf.Lerp(Info.EngineAudio.Audio.volume, volume, Stat.acceleration * Time.deltaTime);
    }

    public void SetThrusterEffect(float trailWidth)
    {
        Info.ThrusterTrails.ForEach((trail) =>
        {
            trail.startWidth = trailWidth;
        });
    }

    /// <summary>
    /// 워프 드라이브로 도착한 것처럼 연출
    /// </summary>
    public virtual void DirectWarpArrival() { }
    /// <summary>
    /// 아군 기동타격대로 결성될 경우
    /// </summary>
    public virtual void OverrideIdentify() { }
    protected virtual void UpdateAudioEnable(float distance)
    {
        Info.EngineAudio.updateAudioEnable(distance);
    }

    // 함선 폭☆8
    protected virtual void PlayShipExplosionEffect()
    {
        Info.ShipExplosionAudio.transform.SetParent(null);
        Info.ShipExplosionParticle.transform.SetParent(null);

        Info.ShipExplosionAudio.Audio.Play();
        //Info.ShipExplosionParticle.gameObject.SetActive(true);
        Info.ShipExplosionParticle.Play();
    }

    public void ResetPowerRecovery()
    {
        t = 0f;
    }

    public void RemoveTarget()
    {
        try
        {
            var target = (ShipBase)Info.TargetingModuleStateMng.currentTarget;
            if (target != null)
                target.Info.TargetingModuleStateMng.EnemyTargetingMyShipList.Remove(this);
        }
        catch { }
    }

    void OnDestroy()
    {
    }
}

public abstract class EnemyShipBase : ShipBase
{
    public int bounty;
    public CreateEnemies createEnemies;
    [SerializeField] protected CharStateMng charStateMng;
    [SerializeField] protected TargetingAlert targetingAlert;
    [SerializeField] protected Transform indicatorTarget;
    public bool InitCheck { get; protected set; }
    public TargetingAlert TargetingAlert => targetingAlert;
    public float DistanceWithMainCamera => Mathf.Min((gameMng.MainCamera.transform.position - Info.ShipTransform.position).magnitude, brightnessMaxDistance);
    public float DistanceWithPlayer => (gameMng.charStateMng.Info.ShipTransform.position - Info.ShipTransform.position).magnitude;
    // 아래 필드들은 카메라 거리에 따른 렌즈 플레어 밝기 조정을 위한 친구들
    // InitOnAwake에서 초기화해 준다.    
    protected float brightnessMaxDistance;

    // 프리팹생성
    protected void InitInstantiation()
    {
        Indicator indicator = Instantiate(this.indicator, EnemyManager.Instance.EnemyIndicatorParent.transform).GetComponent<Indicator>();
        indicatorImage = indicator.normalImage;
        lockOnStateText = indicator.textList[0];
        distanceText = indicator.textList[1];
        indicator.mainCamera = GameMng.Instance.MainCamera;
        indicator.target = indicatorTarget;
        this.indicator = indicator.transform;

        try
        {
            ParallelForTransform = new Transform[]{
            Info.ShipTransform
        };
            LockStateTextTransform = new Transform[] {
            LockStateText.transform
        };
            DistanceTextTransform = new Transform[] {
            DistanceText.transform
        };
        }
        catch (NullReferenceException) { }
    }
    
    public override void PlayThrusterEffects(float brightness, float trailWidth, float trailTime, float volume)
    {
        bool zeroBrightness = brightness == 0f;
        brightness *= (((DistanceWithMainCamera / brightnessMaxDistance) - 1) * -1f); // 카메라 거리에 따른 쓰러스터 밝기 조절
        var wantedBrightness = zeroBrightness ? 0f : Mathf.Clamp(brightness, 0.1f, 1f);
        Info.ThrusterTrails.ForEach((trail) =>
        {
            trail.startWidth = Mathf.Lerp(trail.startWidth, trailWidth, Stat.acceleration * 5f * Time.deltaTime);
            trail.time = Mathf.Lerp(trail.time, trailTime, Stat.acceleration * 5f * Time.deltaTime);
        });
        Info.ThrusterEffects.ForEach(flare =>
        {
            flare.brightness = Mathf.Lerp(flare.brightness, wantedBrightness, /* 고정 */ 1.5f * Time.deltaTime);
        });
        Info.EngineAudio.Audio.volume = Mathf.Lerp(Info.EngineAudio.Audio.volume, volume, Stat.acceleration * Time.deltaTime);
    }
}

public interface IDamaged
{
    float getCurrentHitpoints { get; }
    //bool BeAttacked { get; set; }
}