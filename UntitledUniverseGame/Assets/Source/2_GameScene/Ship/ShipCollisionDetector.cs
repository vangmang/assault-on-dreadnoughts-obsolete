﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShipCollisionDetector : MonoBehaviour
{
    public ShipBase shipBase;

    public void TakeDamage(ProjectileStateMng projectile)
    {
        shipBase.TakeDamage(projectile);
    }

    public void TakeDamage(object[] o_Params)
    {
        shipBase.TakeDamage(o_Params);
    }

    public void TakeAbnormalStateFromEnemy(Action<ShipBase> action)
    {
        shipBase.TakeAbnormalStateFromEnemy(action);
    }
}
