﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using System.Linq;

public class TargetingModuleCaptureState : State<TargetingModuleStateMng>
{
    public override Enum GetState
    {
        get { return TargetingModuleStateMng.TARGETING_MODULE_STATES.capture; }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
        try
        {
            instance.currentTarget = instance.CapturableTargetList[0];//.OrderBy(ship => ship.CurrentDistance).ElementAt(0);
            var distance = (instance.shipBase.Info.ShipTransform.position - instance.currentTarget.TargetableTransform.position).magnitude;
            instance.isOptimalZoneAvailable =
                distance <= instance.shipBase.Targeting.optimalRange &&
                distance >= instance.shipBase.Targeting.optimalZone;
        }
        catch
        {
            instance.isOptimalZoneAvailable = false;
            instance.StateTransition(TargetingModuleStateMng.TARGETING_MODULE_STATES.idle);
        }
    }

    public override void Exit()
    {
        instance.currentTarget = null;
    }
}
