﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class TargetingModuleIdleState : State<TargetingModuleStateMng>
{
    public override Enum GetState
    {
        get { return TargetingModuleStateMng.TARGETING_MODULE_STATES.idle; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.currentTarget = null;
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
