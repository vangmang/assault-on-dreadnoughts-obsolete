﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using FiniteStateMachine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Jobs;


public class TargetingModuleStateMng : StateMng
{
    public enum TARGETING_MODULE_STATES
    {
        idle,
        capture
    }

    public enum TARGET_TYPE
    {
        ship,
        module
    }
    public interface ITargetable : IDamaged
    {
        TARGET_TYPE TargetType { get; }
        ShipBase.IDENTIFY Identify { get; set; }
        Transform TargetableTransform { get; }
        float CurrentVelocity { get; }
        bool IsCapturable { get; }

        Image IndicatorImage { get; }
        Text LockStateText { get; }
        Text DistanceText { get; }
        Transform IndicatorTransform { get; }
        Transform[] ParallelForTransform { get; }
        Transform[] LockStateTextTransform { get; }
        Transform[] DistanceTextTransform { get; }
    }

    public List<ITargetable> EnemyList { get; private set; }
    public List<ITargetable> CapturableTargetList { get; private set; }
    public List<ShipBase> EnemyTargetingMyShipList { get; private set; }
    public ShipBase shipBase;
    public ITargetable currentTarget;
    public bool IsInAngle { get; private set; }
    public bool IsInRange { get; private set; }

    //[NonSerialized] 
    public bool isOptimalZoneAvailable;

    private void Awake()
    {
        InitializeState();
        CapturableTargetList = new List<ITargetable>();
        EnemyList = new List<ITargetable>();
        EnemyTargetingMyShipList = new List<ShipBase>();
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForFixedUpdate();
        this.ObserveEveryValueChanged(_ => EnemyManager.Instance.targets.Count).
            Subscribe(_ =>
            {
                checkTarget();
                EnemyList = EnemyManager.Instance.targets.Where(target => target.Identify != shipBase.Identify).ToList();
            });
        StateEnter(TARGETING_MODULE_STATES.idle);

        this.ObserveEveryValueChanged(_ => CapturableTargetList.Count).
            Where(_ => !shipBase.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !shipBase.gameMng.gameStateMng.isShowingInfoState).
            Subscribe(_ =>
            {
                if (CapturableTargetList.Count > 0)
                    StateTransition(TARGETING_MODULE_STATES.capture);
                else
                    StateTransition(TARGETING_MODULE_STATES.idle);
            });
        //this.ObserveEveryValueChanged(_ => shipBase.CurrentInfo.cur_ShieldCapacity).
        //    Where(hitpoints => hitpoints <= 0f).
        //    Subscribe(_ =>
        //    {
        //        try
        //        {
        //            var target = (ShipBase)currentTarget;
        //            if (target != null)
        //                target.Info.TargetingModuleStateMng.EnemyTargetingMyShipList.Remove(shipBase);
        //        }
        //        catch { }
        //    });
    }

    private void Update()
    {
        Execute();
        checkTarget();
    }

    private void checkTarget()
    {
        // 타겟팅 가능 각도
        foreach (ITargetable target in EnemyList)
        {
            if (!target.IsCapturable)
                return;
            try 
            {
                var pos = shipBase.Info.ShipTransform.InverseTransformPoint(target.TargetableTransform.position);
                var angle = new Vector2(Mathf.Atan2(pos.y, pos.z), Mathf.Atan2(pos.z, pos.x)) * Mathf.Rad2Deg;
                angle.y -= 90f;
                bool angleCheck = Mathf.Abs(angle.x) <= shipBase.WeaponLimitElevation &&
                                  Mathf.Abs(angle.y) <= shipBase.WeaponLimitElevation;
                IsInAngle = angleCheck;

                // 유효사정거리 체크
                var dist = (shipBase.Info.ShipTransform.position - target.TargetableTransform.position).magnitude;
                IsInRange = dist <= shipBase.Targeting.fallOffRange;

                bool isBeingCapturable = IsInAngle && IsInRange;
                if (target.getCurrentHitpoints <= 0f)
                {
                    if (target.Equals(currentTarget))
                        currentTarget = null;
                    EnemyList.Remove(target);
                    if (shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Contains(target))
                        shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Remove(target);
                    return;
                }

                try
                {
                    var shipBaseTarget = (ShipBase)target;
                    if (shipBaseTarget)
                    {
                        if (isBeingCapturable)
                        {
                            if (!shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Contains(shipBaseTarget))
                            {
                                shipBaseTarget.Info.TargetingModuleStateMng.EnemyTargetingMyShipList.Add(shipBase);
                                shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Add(shipBaseTarget);
                            }
                        }
                        else
                        {
                            if (shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Contains(shipBaseTarget))
                            {
                                shipBaseTarget.Info.TargetingModuleStateMng.EnemyTargetingMyShipList.Remove(shipBase);
                                shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Remove(shipBaseTarget);
                            }
                        }
                    }
                }
                catch
                {
                    var moduleTarget = (DreadnoughtWeaponBase)target;
                    if (moduleTarget)
                    {
                        if (isBeingCapturable)
                        {
                            if (!shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Contains(moduleTarget))
                                shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Add(moduleTarget);
                        }
                        else
                        {
                            if (shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Contains(moduleTarget))
                                shipBase.Info.TargetingModuleStateMng.CapturableTargetList.Remove(moduleTarget);
                        }
                    }
                }
            }
            catch (MissingReferenceException) { }
        }
    }


    private void OnDestroy()
    {

    }
}
