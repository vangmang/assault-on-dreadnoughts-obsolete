﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class BuildIconDisplayer : MonoBehaviour
{
    [SerializeField] private BuildIcon buildIcon;

    [SerializeField] private GameStateMng gameStateMng;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => gameStateMng.GetCurrentState).
            //Where(_ => !gameStateMng.GetRecentState.Equals(GameStateMng.GAME_STATES.loading)).
            Where(state => state.Equals(GameStateMng.GAME_STATES.idle)).
            Subscribe(_ =>
            {
                transform.localPosition = new Vector2(buildIcon.currentIndex * 60f, 0f);
            });
    }
}
