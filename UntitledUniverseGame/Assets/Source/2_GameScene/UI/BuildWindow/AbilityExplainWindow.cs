﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityExplainWindow : MonoBehaviour
{
    public Text abilityNameText;
    public Text abilityDescriptionText;

    public void SetAbilityDescription(string name, string description)
    {
        abilityNameText.text = name;
        abilityDescriptionText.text = description;
    }

    public void ResetAbilityDescription()
    {
        abilityNameText.text = "";
        abilityDescriptionText.text = "";
    }
}
