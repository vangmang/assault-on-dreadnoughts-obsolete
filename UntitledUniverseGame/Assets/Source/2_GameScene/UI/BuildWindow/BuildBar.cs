﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;

/// <summary>
/// 여기서 빌드 세팅이 이루어진다.
/// </summary>
public class BuildBar : MonoBehaviour
{
    [SerializeField] private CharacterBuildMng charBuildMng;
    [SerializeField] private BuildIcon[] buildIcons;
    [SerializeField] private BuildSlot[] buildSlots;
    public BuildIcon[] BuildIcons { get { return buildIcons; } }
    public BuildSlot[] BuildSlots { get { return buildSlots; } }

    private void Awake()
    {
        foreach(KeyValuePair<CharacterBuildMng.ACTIVE_WEAPON_TYPE, int> pair in charBuildMng.BuildTableForSetting)
        {
            var buildIcon = buildIcons.First(icon => icon.getActiveWeaponType.Equals(pair.Key));
            buildIcon.currentIndex = pair.Value;
            buildIcon.transform.position = buildSlots[pair.Value].transform.position;
            buildSlots[pair.Value].buildIcon = buildIcon;
            buildSlots[pair.Value].charBuildMng = charBuildMng;
        }
    }

    private void OnDestroy()
    {
        
    }
}
