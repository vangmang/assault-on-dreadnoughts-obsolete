﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildDescription : MonoBehaviour
{
    public Text buildTitleText;
    public Text buildDescriptionText;

}
