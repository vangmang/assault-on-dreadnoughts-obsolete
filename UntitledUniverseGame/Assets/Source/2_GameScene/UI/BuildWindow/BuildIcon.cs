﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildIcon : MonoBehaviour
{
    [SerializeField]private CharacterBuildMng.ACTIVE_WEAPON_TYPE activeWeaponType;
    public CharacterBuildMng.ACTIVE_WEAPON_TYPE getActiveWeaponType { get { return activeWeaponType; } }
    public int currentIndex;
    public string buildTtile;
    public TextAsset buildDescription;
}
