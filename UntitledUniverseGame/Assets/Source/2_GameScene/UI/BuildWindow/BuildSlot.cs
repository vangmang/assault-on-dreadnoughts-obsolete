﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class BuildSlot : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public BuildBar buildBar;
    public Image selectedImage;
    public BuildIcon buildIcon;
    public Camera UICamera;
    public static BuildSlot currentBuildSlot = null;
    public static BuildSlot selectedSlot = null;
    public CharacterBuildMng charBuildMng;
    public BuildDescription buildDescription;

    void Awake()
    {
        this.ObserveEveryValueChanged(_ => buildIcon).
            Where(x => x).
            Subscribe(_ =>
            {
                buildIcon.transform.position = transform.position;
            });
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var color = selectedImage.color;
        color.a = 1f;
        selectedImage.color = color;
        currentBuildSlot = this;
        buildDescription.transform.position = transform.position + new Vector3(140f, -140f);
        buildDescription.buildTitleText.text = buildIcon.buildTtile;
        buildDescription.buildDescriptionText.text = buildIcon.buildDescription.text;
        buildDescription.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        var color = selectedImage.color;
        color.a = 0f;
        selectedImage.color = color;
        currentBuildSlot = null;
        buildDescription.gameObject.SetActive(false);
    }

    public void OnPointerDown(PointerEventData e)
    {
        selectedSlot = this;
        StartCoroutine(setBuildIcon());
    }

    private IEnumerator setBuildIcon()
    {
        while (Input.GetMouseButton(0))
        {
            selectedSlot.buildIcon.transform.position = UICamera.ScreenToWorldPoint(Input.mousePosition);
            if (currentBuildSlot)
            {
                checkFittingAvailiable(currentBuildSlot.buildIcon, selectedSlot.buildIcon);
            }
            yield return null;
        }
        if (currentBuildSlot)
        {
            var currentIcon = currentBuildSlot.buildIcon;
            if (checkFittingAvailiable(currentBuildSlot.buildIcon, selectedSlot.buildIcon) &&
                checkFittingAvailiable(selectedSlot.buildIcon, currentBuildSlot.buildIcon) && // 바뀔 대상이 위치하게 될 인덱스도 검사해줘야 함
                !selectedSlot.Equals(currentBuildSlot))
            {
                var tempBuild = charBuildMng.CurrentBuildTable[currentIcon.currentIndex];
                int tempIndex = currentIcon.currentIndex;
                charBuildMng.CurrentBuildTable[currentIcon.currentIndex] = charBuildMng.CurrentBuildTable[selectedSlot.buildIcon.currentIndex];
                currentIcon.currentIndex = selectedSlot.buildIcon.currentIndex;
                currentBuildSlot.buildIcon = selectedSlot.buildIcon;
                charBuildMng.CurrentBuildTable[selectedSlot.buildIcon.currentIndex] = tempBuild;
                selectedSlot.buildIcon.currentIndex = tempIndex;
                selectedSlot.buildIcon = currentIcon;
            }
            else
            {
                selectedSlot.buildIcon.transform.position = selectedSlot.transform.position;
            }
        }
        else
        {
            selectedSlot.buildIcon.transform.position = selectedSlot.transform.position;
        }
        selectedSlot = null;
    }

    private bool checkFittingAvailiable(BuildIcon currentIcon, BuildIcon selectIcon)
    {
        int index = currentIcon.currentIndex;
        int leftIndex = index - 1;
        int rightIndex = index + 1;
        bool check = true;
        bool leftCheck = true;
        bool rightCheck = true;
        if (leftIndex >= 0)
        {
            var leftIcon = buildBar.BuildIcons.First(icon => icon.currentIndex == leftIndex);
            leftCheck = !charBuildMng.BuildTypeTable[leftIcon.getActiveWeaponType].Equals(charBuildMng.BuildTypeTable[selectIcon.getActiveWeaponType]) ||
                        leftIcon.Equals(selectIcon);
        }
        if (rightIndex <= (int)CharacterBuildMng.ACTIVE_WEAPON_TYPE.hacking)
        {
            var rightIcon = buildBar.BuildIcons.First(icon => icon.currentIndex == rightIndex);
            rightCheck = !charBuildMng.BuildTypeTable[rightIcon.getActiveWeaponType].Equals(charBuildMng.BuildTypeTable[selectIcon.getActiveWeaponType]) ||
                         rightIcon.Equals(selectIcon);
        }

        check = leftCheck && rightCheck;
        return check;
    }

    void OnDestroy()
    {
        currentBuildSlot = null;
        selectedSlot = null;
    }
}
