﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class LauncherSelectionMng : MonoBehaviour
{
    public CharStateMng charStateMng;
    /// <summary>
    /// 능력 enum상수 순서에 맞게 할당되어 있는 프리팹
    /// </summary>
    public LauncherFormAbility[] launcherPrefabs;
    
    public void InstantiateLauncher(LauncherFormAbility.LAUNCHER_TYPE launcherType)
    {
        //currentLauncherType = launcherType;
        var recentLauncher = charStateMng.CurrentInfo.launcherFormAbility;
        var launcher = Instantiate(launcherPrefabs[(int)launcherType], charStateMng.CharacterFeatureInfo.LauncherParentTransform);
        launcher.shipBase = charStateMng;
        charStateMng.CurrentInfo.launcherFormAbility = launcher;
        if(recentLauncher != null)
            Destroy(recentLauncher.gameObject);
    }
}
