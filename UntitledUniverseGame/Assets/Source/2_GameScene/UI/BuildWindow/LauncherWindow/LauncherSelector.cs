﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class LauncherSelector : GameSceneButtonManager
{
    [SerializeField] private CharStateMng charStateMng;
    [SerializeField] private LauncherSelectionMng launcherSelectionMng;
    [SerializeField] private FittedLauncherDisplayer fittedLauncherDisplayer;
    [SerializeField] private Image iconImage;
    [SerializeField] private LauncherFormAbility.LAUNCHER_TYPE launcherType;
    [SerializeField] private AbilityExplainWindow abilityExplainWindow;

    [SerializeField] private string abilityName;
    [SerializeField] private TextAsset abilityDescription;

    public override void OnPointerDown(PointerEventData e)
    {
        // 오른쪽 마우스 클릭으로 런처 스왑
        if(e.button.Equals(PointerEventData.InputButton.Right))
        {
            // 능력 쿨타임 도중 변경 불가
            if(charStateMng.CurrentInfo.launcherFormAbility && charStateMng.CurrentInfo.launcherFormAbility.isCooldownExecuted)
            {
                return;
            }
            fittedLauncherDisplayer.launcherImage.sprite = iconImage.sprite;
            launcherSelectionMng.InstantiateLauncher(launcherType);
        }
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        abilityExplainWindow.SetAbilityDescription(abilityName, abilityDescription.text);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        abilityExplainWindow.ResetAbilityDescription();
    }

    public override void OnPointerUp(PointerEventData e)
    {
    }
}
