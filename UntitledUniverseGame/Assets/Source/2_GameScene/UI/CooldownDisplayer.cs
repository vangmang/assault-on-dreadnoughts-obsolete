﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownDisplayer : MonoBehaviour
{
    [SerializeField] private CharStateMng charStateMng;
    [SerializeField] private Image cooldownImage;
    
    public void InvokeCooldown(float time)
    {
        charStateMng.CurrentInfo.launcherFormAbility.isCooldownExecuted = true;
        IEnumerator cooldown = invokeCooldown(time);
        StartCoroutine(cooldown);
    }

    private IEnumerator invokeCooldown(float time)
    {
        float t = time;
        while(t > 0f)
        {
            t -= Time.deltaTime;
            cooldownImage.fillAmount = t / time;
            yield return null;
        }
        charStateMng.CurrentInfo.launcherFormAbility.isCooldownExecuted = false;
    }

    private void OnDestroy()
    {
        
    }
}
