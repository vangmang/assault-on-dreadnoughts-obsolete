﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class GageBar : MonoBehaviour
{
    public Image BackgroundImage;
    /// <summary>
    /// 메인 게이지바
    /// </summary>
    public Image GagebarSlideImage1;
    public Image GagebarSlideImage2;
}
