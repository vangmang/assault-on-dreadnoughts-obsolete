﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneFadeImage : MonoBehaviour
{
    public Image image;
    public GraphicRaycaster UIgraphicRaycaster;
    public MouseFollowingCamera followingCamera;
    public MouseFollowingCamera effectCamera;
    public bool Init { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        Init = false;
        StartCoroutine(playImageFade());
    }

    private IEnumerator playImageFade()
    {
        yield return new WaitForSeconds(1f);
        float t = 1f;
        var color = image.color;
        while (t > 0f)
        {
            t -= Time.deltaTime;
            color.a = t;
            image.color = color;
            yield return null;
        }
        color.a = 0f;
        image.color = color;
        UIgraphicRaycaster.enabled = true;
        followingCamera.enabled = true;
        effectCamera.enabled = true;
        Init = true;
    }

    public void SetImageAlpha1F()
    {
        UIgraphicRaycaster.enabled = false;
        var color = image.color;
        color.a = 1f;
        image.color = color;
    }
}
