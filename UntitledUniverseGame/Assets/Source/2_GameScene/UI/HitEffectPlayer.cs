﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitEffectPlayer : MonoBehaviour
{
    [SerializeField] private Image effectImage;
    [SerializeField] private AudioSource hitAudio;
    private float t;
    private bool isImageEffectPlaying;

    public void PlayHitEffect()
    {
        t = 1f;
        if(!isImageEffectPlaying)
        {
            isImageEffectPlaying = true;
            StartCoroutine(invokeImageEffect());
        }
        hitAudio.Play();
    }

    private IEnumerator invokeImageEffect()
    {
        var color = effectImage.color;
        color.a = 1f;
        effectImage.color = color;
        var startScale = Vector3.one;
        var endScale = new Vector3(1.5f, 1.5f);
        while (t > 0f)
        {
            t -= 5f * Time.deltaTime;
            color.a = t;
            effectImage.transform.localScale = Vector3.Lerp(startScale, endScale, t);
            effectImage.color = color;
            yield return null;
        }
        color.a = 0f;
        effectImage.color = color;
        isImageEffectPlaying = false;

        //effectImage.color = 
    }
}
