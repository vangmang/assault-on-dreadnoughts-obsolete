﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class HitpointsBar : GageBar
{
    [SerializeField] private CharStateMng charStateMng;

    private bool isDecreasing;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => charStateMng.UpgradeTimesStat.shieldCapacity).
            Subscribe(_ =>
            {
                BackgroundImage.rectTransform.sizeDelta = new Vector2(48f * charStateMng.UpgradeTimesStat.shieldCapacity, 30f);
                GagebarSlideImage1.rectTransform.sizeDelta = new Vector2(48f * charStateMng.UpgradeTimesStat.shieldCapacity, 10f);
                GagebarSlideImage2.rectTransform.sizeDelta = new Vector2(48f * charStateMng.UpgradeTimesStat.shieldCapacity, 50f);

                GagebarSlideImage1.transform.localPosition = new Vector3(-232.5f + 0.55f * (charStateMng.UpgradeTimesStat.shieldCapacity - 10), 0f);
                GagebarSlideImage2.transform.localPosition = new Vector3(-232.5f + 0.55f * (charStateMng.UpgradeTimesStat.shieldCapacity - 10), 0f);
            });
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_ShieldCapacity).
            Subscribe(_ =>
            {
                if (GagebarSlideImage1.fillAmount > GagebarSlideImage2.fillAmount)
                    GagebarSlideImage2.fillAmount = GagebarSlideImage1.fillAmount;
                var amount = charStateMng.CurrentInfo.cur_ShieldCapacity / charStateMng.Stat.shieldCapacity;
                GagebarSlideImage1.fillAmount = amount;
                if (!isDecreasing)
                {
                    StartCoroutine(invokeDecreasing());
                }
            });
    }
    private IEnumerator invokeDecreasing()
    {
        isDecreasing = true;
        yield return new WaitForSeconds(1f);
        while (GagebarSlideImage2.fillAmount >= GagebarSlideImage1.fillAmount)
        {
            GagebarSlideImage2.fillAmount -= 0.75f * Time.deltaTime;
            yield return null;
        }
        isDecreasing = false;
    }
}
