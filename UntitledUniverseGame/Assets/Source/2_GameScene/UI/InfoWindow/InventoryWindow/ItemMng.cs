﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMng : MonoBehaviour
{
    public GameObject currentItemDescription;

    public void SetItemDescription(GameObject itemDescription)
    {
        if (currentItemDescription)
            currentItemDescription.gameObject.SetActive(false);
        currentItemDescription = itemDescription;
        currentItemDescription.SetActive(true);
    }
}
