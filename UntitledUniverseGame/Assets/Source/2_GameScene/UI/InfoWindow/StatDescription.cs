﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatDescription : MonoBehaviour
{
    public Text statTitle;
    public Text statDescription;
}
