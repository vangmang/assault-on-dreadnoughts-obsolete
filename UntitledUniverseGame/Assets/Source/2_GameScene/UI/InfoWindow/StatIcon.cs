﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StatIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private string statTitleText;
    [SerializeField] private TextAsset statDescriptionText;
    [SerializeField] private StatDescription statDescription;

    public void OnPointerEnter(PointerEventData e)
    {
        statDescription.statTitle.text = statTitleText;
        statDescription.statDescription.text = statDescriptionText.text;
        statDescription.transform.position = transform.position + new Vector3(475, -45f);
        statDescription.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData e)
    {
        statDescription.gameObject.SetActive(false);
    }
}
