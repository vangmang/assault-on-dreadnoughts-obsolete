﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class UnableToEditStatDisplayer : MonoBehaviour
{
    public GameStateMng gameStateMng;
    public CharStateMng charStateMng;
    public ShipBase._STAT_.STAT statType;
    public Text statText;
    public Text upgradeText;
    public Text costText;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => gameStateMng.GetCurrentState).
            Where(state => state.Equals(GameStateMng.GAME_STATES.pauseAndShowInfo)).
            Subscribe(_ =>
            {
                switch (statType)
                {
                    case ShipBase._STAT_.STAT.velocity:
                        upgradeText.text = charStateMng.UpgradeTimesStat.velocity.ToString();
                        statText.text = charStateMng.Stat.velocity.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.velocity);
                        break;
                    case ShipBase._STAT_.STAT.acceleration:
                        upgradeText.text = charStateMng.UpgradeTimesStat.acceleration.ToString();
                        statText.text = charStateMng.Stat.acceleration.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.acceleration);
                        break;
                    case ShipBase._STAT_.STAT.mobility:
                        upgradeText.text = charStateMng.UpgradeTimesStat.mobility.ToString();
                        statText.text = charStateMng.Stat.mobility.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.mobility);
                        break;
                    case ShipBase._STAT_.STAT.shieldCapacity:
                        upgradeText.text = charStateMng.UpgradeTimesStat.shieldCapacity.ToString();
                        statText.text = charStateMng.Stat.shieldCapacity.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.shieldCapacity);
                        break;
                    case ShipBase._STAT_.STAT.power:
                        upgradeText.text = charStateMng.UpgradeTimesStat.power.ToString();
                        statText.text = charStateMng.Stat.power.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.power);
                        break;
                    case ShipBase._STAT_.STAT.weaponDamage:
                        upgradeText.text = charStateMng.UpgradeTimesStat.weaponDamage.ToString();
                        statText.text = charStateMng.Stat.weaponDamage.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.weaponDamage);
                        break;
                    case ShipBase._STAT_.STAT.weaponRange:
                        upgradeText.text = charStateMng.UpgradeTimesStat.weaponRange.ToString();
                        statText.text = charStateMng.Stat.weaponRange.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.weaponRange);
                        break;
                    case ShipBase._STAT_.STAT.weaponFireRate:
                        upgradeText.text = charStateMng.UpgradeTimesStat.weaponFireRate.ToString();
                        statText.text = charStateMng.Stat.weaponFireRate.ToString("N2");
                        setCostText(charStateMng.UpgradeTimesStat.weaponFireRate);
                        break;
                    default: throw new System.Exception();
                }
            });
    }

    private void setCostText(int upgradeTimes)
    {
        string text = (500 * (int)Mathf.Pow(2, upgradeTimes - 10)).ToString();
        if (upgradeTimes == 19)
            text = "200000";
        else if (upgradeTimes == 20)
            text = "Max";
        costText.text = text;
    }
}
