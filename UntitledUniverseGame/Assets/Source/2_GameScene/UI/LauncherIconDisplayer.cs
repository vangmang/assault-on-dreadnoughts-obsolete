﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class LauncherIconDisplayer : MonoBehaviour
{
    [SerializeField] private CharStateMng charStateMng;
    [SerializeField] private GameObject[] IconArr;
    private GameObject recentIcon;

    private void Start()
    {
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.launcherFormAbility).
            Where(_ => charStateMng.CurrentInfo.launcherFormAbility != null).
            Subscribe(_ =>
            {
                if (recentIcon)
                    recentIcon.SetActive(false);
                var currentIcon = IconArr[(int)charStateMng.CurrentInfo.launcherFormAbility.LauncherType];
                recentIcon = currentIcon;
                currentIcon.SetActive(true);
            });
    }

    private void OnDestroy()
    {
        
    }
}
