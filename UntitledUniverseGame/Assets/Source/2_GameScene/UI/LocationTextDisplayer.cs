﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class LocationTextDisplayer : MonoBehaviour
{
    [SerializeField] private CharStateMng charStateMng;
    [SerializeField] private Text text;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForFixedUpdate();
        //text.text = 
        this.ObserveEveryValueChanged(_ => charStateMng.CurrentInfo.cur_WarpPoint).
            Where(point => point != null).
            Subscribe(point =>
            {
                StringBuilder sb = new StringBuilder(point.currentPlanet.placetNameText.text);
                sb.Append(" - ").Append(point.ToString().Replace("(WarpPoint)","").ToString());
                text.text = sb.ToString(); 
            });
    }

}
