﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InfoBtn : GameSceneButtonManager
{
    public override void OnPointerDown(PointerEventData e)
    {
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
        gameMng.gameStateMng.isShowingInfoState = true;
        if (gameMng.PauseInfo.currentWindow.Equals(gameMng.PauseInfo.pauseWindow))
            gameMng.PauseInfo.recentWindows.Push(gameMng.PauseInfo.currentWindow); // 일시정지 창 푸쉬
        gameMng.gameStateMng.StateTransition(GameStateMng.GAME_STATES.pauseAndShowInfo);
    }

}
