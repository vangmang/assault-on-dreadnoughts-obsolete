﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MainMenuBtn : GameSceneButtonManager
{
    public override void OnPointerDown(PointerEventData e)
    {
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
        Time.timeScale = 1f;
        StopAllCoroutines();
        gameMng.LoadScene(LoadingMng.SCENE.mainMenu);
        //SceneManager.LoadScene((int)LoadingMng.SCENE.loading);
        //LoadingMng.Instance.n_TargetScene = (int)LoadingMng.SCENE.mainMenu;
    }
}
