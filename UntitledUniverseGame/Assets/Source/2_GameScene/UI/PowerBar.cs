﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PowerBar : GageBar
{
    [SerializeField] private CharStateMng charStateMng;

    private bool isDecreasing;
    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => charStateMng.UpgradeTimesStat.power).
            Subscribe(_ =>
            {
                BackgroundImage.rectTransform.sizeDelta = new Vector2(48f * charStateMng.UpgradeTimesStat.power, 30f);
                GagebarSlideImage1.rectTransform.sizeDelta = new Vector2(48f * charStateMng.UpgradeTimesStat.power, 10f);
                GagebarSlideImage2.rectTransform.sizeDelta = new Vector2(48f * charStateMng.UpgradeTimesStat.power, 50f);

                GagebarSlideImage1.transform.localPosition = new Vector3(-232.5f + 0.55f * (charStateMng.UpgradeTimesStat.shieldCapacity - 10), 0f);
                GagebarSlideImage2.transform.localPosition = new Vector3(-232.5f + 0.55f * (charStateMng.UpgradeTimesStat.shieldCapacity - 10), 0f);
            });
        this.ObserveEveryValueChanged(instance => instance.charStateMng.CurrentInfo.cur_power).
            Subscribe(_ =>
            {
                if (GagebarSlideImage1.fillAmount > GagebarSlideImage2.fillAmount)
                    GagebarSlideImage2.fillAmount = GagebarSlideImage1.fillAmount;
                var amount = charStateMng.CurrentInfo.cur_power / charStateMng.Stat.power;
                GagebarSlideImage1.fillAmount = amount;
                if (!isDecreasing && !charStateMng.CurrentInfo.isPowerRecoveryEnable)
                {
                    StartCoroutine(invokeDecreasing());
                }
            });
    }

    private IEnumerator invokeDecreasing()
    {
        isDecreasing = true;
        yield return new WaitForSeconds(1f);
        while (GagebarSlideImage2.fillAmount >= GagebarSlideImage1.fillAmount)
        {
            GagebarSlideImage2.fillAmount -= 0.75f * Time.deltaTime;
            yield return null;
        }
        isDecreasing = false;
    }
}
