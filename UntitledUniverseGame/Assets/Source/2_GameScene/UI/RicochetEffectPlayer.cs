﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RicochetEffectPlayer : MonoBehaviour
{
    public AudioSource[] audios;

    public void PlayRicochetEffectAudio()
    {
        var rand = Random.Range(0, audios.Length);
        audios[rand].Play();
    }
}
