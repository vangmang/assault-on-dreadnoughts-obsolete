﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedIconDisplayer : MonoBehaviour
{
    private void Update()
    {
        transform.Rotate(Vector3.forward, 50f * Time.deltaTime);
    }
}
