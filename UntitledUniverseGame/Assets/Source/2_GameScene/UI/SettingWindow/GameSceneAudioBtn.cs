﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameSceneAudioBtn : GameSceneButtonManager
{
    public override void OnPointerDown(PointerEventData e)
    {
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
        gameMng.PauseInfo.currentWindow.gameObject.SetActive(false);
        gameMng.PauseInfo.recentWindows.Push(gameMng.PauseInfo.currentWindow);
        gameMng.PauseInfo.currentWindow = gameMng.PauseInfo.audioWindow;
        //Debug.Log(gameMng.PauseInfo.graphicWindow + " " + gameMng.PauseInfo.audioWindow);

        gameMng.PauseInfo.currentWindow.gameObject.SetActive(true);
    }

}
