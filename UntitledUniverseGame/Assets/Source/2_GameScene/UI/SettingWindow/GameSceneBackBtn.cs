﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameSceneBackBtn : GameSceneButtonManager
{
    public override void OnPointerDown(PointerEventData e)
    {
        PlayPressButtonSound();ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
        if (gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pauseAndShowStation))
            gameMng.gameStateMng.escapePause();
        else if (gameMng.gameStateMng.isShowingInfoState)
            gameMng.gameStateMng.StateTransition(GameStateMng.GAME_STATES.pause);
        else
            invokeBack();
    }

}
