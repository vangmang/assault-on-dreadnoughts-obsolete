﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class SliderChanger : MonoBehaviour
{
    public Slider slider;
    public Text valueText;

    private void Start()
    {
        this.ObserveEveryValueChanged(_ => slider.value).
            Subscribe(_ =>
            {
                valueText.text = (slider.value * 100f).ToString("N0");
            });
    }
}
