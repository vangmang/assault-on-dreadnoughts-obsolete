﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class ShieldRecoveryCountDisplayer : MonoBehaviour
{
    [SerializeField] private CharStateMng charStateMng;
    [SerializeField] private Text countText;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => charStateMng.currentShieldRecoveryCount).
            Subscribe(_ =>
            {
                countText.text = charStateMng.currentShieldRecoveryCount.ToString();
            });
    }
}
