﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetDisplayer : MonoBehaviour
{
    public PlanetInfoIndicator indicator;
    public Transform indicatorParent;
    public PlanetInfo planetInfo;
    public LineRenderer lineRenderer;
    public GameStateMng gameStateMng;
    public CurrentPlanetMapDisplayer currentPlanetMapDisplayer;
    public StarSystemDisplayer starSystemDisplayer;

    // Start is called before the first frame update
    void Start()
    {
        PlanetInfoIndicator indicator = Instantiate(this.indicator, indicatorParent);
        indicator.target = transform;
        indicator.planetInfo = planetInfo;
        indicator.starSystemDisplayer = starSystemDisplayer;
        indicator.textList[0].text = planetInfo.planet.placetNameText.text;
        currentPlanetMapDisplayer.transform.SetParent(indicator.transform); //.transform.parent = indicator.transform;
        currentPlanetMapDisplayer.transform.localPosition = new Vector3(0f, 34f);
        currentPlanetMapDisplayer.gameStateMng = gameStateMng;
        currentPlanetMapDisplayer.planetInfo = planetInfo;
    }

    private void Update()
    {
        lineRenderer.SetPosition(0, -transform.localPosition);
    }
}
