﻿using System.Collections;
using UnityEngine;
using UniRx;
using System.Linq;
using UnityEngine.UI;
using System.Text;

public class StarSystemDisplayer : MonoBehaviour
{
    public StarMap starMap;
    public GameStateMng gameStateMng;
    public GameObject planetForMapDisplayer;
    public Transform planetIndicatorParent;
    public Camera mapCamera;
    public GameObject mapUICanvas;
    public MouseFollowingCamera mouseFollowingCamera;

    public Text planetNameText;
    public Text warpPointsListText;
    //PlanetInfo
    private float radian;
    private float rotY;
    private float rotX;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForFixedUpdate();
        var maxDist = starMap.starMapList.OrderByDescending(planet => planet.distanceWithStar).First().distanceWithStar;
        radian = 225 * maxDist;
        mapCamera.transform.position += mapCamera.transform.forward * -radian;
        starMap.starMapList.ForEach(p =>
        {
            var pos = p.OriginalPos * 0.001f;
            var planetDisplayer = Instantiate(planetForMapDisplayer, transform).GetComponent<PlanetDisplayer>();
            planetDisplayer.transform.localPosition = pos;
            planetDisplayer.indicatorParent = planetIndicatorParent;
            planetDisplayer.indicator.mainCamera = mapCamera;
            planetDisplayer.planetInfo = p;
            planetDisplayer.gameStateMng = gameStateMng;
            planetDisplayer.starSystemDisplayer = this;
        });
        this.ObserveEveryValueChanged(_ => gameStateMng.GetCurrentState).
            Subscribe(_ =>
            {
                bool activeSelf = gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pauseAndShowMap);
                mapUICanvas.SetActive(activeSelf);
                if (activeSelf)
                {
                    StringBuilder sb = new StringBuilder();
                    for(int i = 0; i < gameStateMng.gameMng.charStateMng.CurrentInfo.cur_Stage.planet.planetInfo.possessedWarpPoints.Count; i++)
                    {
                        var warpPoint = gameStateMng.gameMng.charStateMng.CurrentInfo.cur_Stage.planet.planetInfo.possessedWarpPoints[i];
                        sb.Append((i + 1).ToString()).Append(") ").Append(warpPoint.placetNameText.text).Append("\n").Append("탐사 여부: ").
                        Append(warpPoint.explorationCheck ? "탐사 완료" : "미확인");
                        if (warpPoint.currentPointCheck)
                            sb.Append(" <현재 위치>");
                        sb.Append("\n");
                    }
                    initMapDescription(gameStateMng.gameMng.charStateMng.CurrentInfo.cur_Stage.planet.placetNameText.text, sb.ToString());
                }
                else
                    initMapDescription("", "");
            });
        Observable.EveryUpdate().
            Where(_ => Input.GetMouseButton(0)).
            Where(_ => gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pauseAndShowMap)).
            Subscribe(_ =>
            {
                float mouseX = Input.GetAxis("Mouse X");
                float mouseY = -Input.GetAxis("Mouse Y");

                rotY += mouseX * mouseFollowingCamera.Sensitivity * 5f * Time.unscaledDeltaTime;
                rotX += mouseY * mouseFollowingCamera.Sensitivity * 5f * Time.unscaledDeltaTime;
                rotX = Mathf.Clamp(rotX, -90f, 90f);
                mapCamera.transform.rotation = Quaternion.Euler(!mouseFollowingCamera.Xreverse ? rotX : -rotX, !mouseFollowingCamera.Yreverse ? rotY : -rotY, 0f);
                var target = transform.position + mapCamera.transform.forward * -radian;
                mapCamera.transform.position = target;

                //Debug.Log(mapCamera.transform.position + " " + target);
            }).AddTo(this);
    }

    private void initMapDescription(string nameText, string warpPointsListText)
    {
        planetNameText.text = nameText;
        this.warpPointsListText.text = warpPointsListText;
    }

    private void OnDestroy()
    {
        
    }
}
