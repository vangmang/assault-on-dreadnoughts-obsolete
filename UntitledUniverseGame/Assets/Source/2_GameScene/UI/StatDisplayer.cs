﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

/// <summary>
/// 업그레이드 횟수와 스탯 능력치를 보여준다.
/// </summary>
public class StatDisplayer : MonoBehaviour
{
    public GameMng gameMng;
    public CharStateMng charStateMng;
    public ShipBase._STAT_.STAT statType;
    public Text statText;
    public Text upgradeText;
    public Text costText;
    public float stat;
    public int upgradeTimes;
    public int cost;
    public const int MaximumUpgrade = 20;

    public void UpgradeStat()
    {
        if (gameMng.Goods.bountyAmount < cost)
            return;
        gameMng.Goods.bountyAmount -= cost;
        upgradeTimes++;
    }

    // Start is called before the first frame update
    void Start()
    {
        switch (statType)
        {
            case ShipBase._STAT_.STAT.velocity:
                upgradeTimes = charStateMng.UpgradeTimesStat.velocity;
                break;
            case ShipBase._STAT_.STAT.acceleration:
                upgradeTimes = charStateMng.UpgradeTimesStat.acceleration;
                break;
            case ShipBase._STAT_.STAT.mobility:
                upgradeTimes = charStateMng.UpgradeTimesStat.mobility;
                break;
            case ShipBase._STAT_.STAT.shieldCapacity:
                upgradeTimes = charStateMng.UpgradeTimesStat.shieldCapacity;
                break;
            case ShipBase._STAT_.STAT.power:
                upgradeTimes = charStateMng.UpgradeTimesStat.power;
                break;
            case ShipBase._STAT_.STAT.weaponDamage:
                upgradeTimes = charStateMng.UpgradeTimesStat.weaponDamage;
                break;
            case ShipBase._STAT_.STAT.weaponRange:
                upgradeTimes = charStateMng.UpgradeTimesStat.weaponRange;
                break;
            case ShipBase._STAT_.STAT.weaponFireRate:
                upgradeTimes = charStateMng.UpgradeTimesStat.weaponFireRate;
                break;
            default: throw new System.Exception();
        }
        this.ObserveEveryValueChanged(_ => cost).
            Subscribe(_ =>
            {
                string text = cost.ToString();
                if (upgradeTimes == 20)
                    text = "Max";
                costText.text = text;
            });
        this.ObserveEveryValueChanged(_ => stat).
            Subscribe(_ =>
            {
                statText.text = stat.ToString("N2");
            });
        this.ObserveEveryValueChanged(_ => upgradeTimes).
            Subscribe(_ =>
            {
                int value = 500 * (int)Mathf.Pow(2, (upgradeTimes - 10));
                if (upgradeTimes == 19)
                    value = 200000;
                cost = value;
                upgradeText.text = upgradeTimes.ToString();
                switch (statType)
                {
                    case ShipBase._STAT_.STAT.velocity:
                        charStateMng.UpgradeTimesStat.velocity = upgradeTimes;
                        break;
                    case ShipBase._STAT_.STAT.acceleration:
                        charStateMng.UpgradeTimesStat.acceleration = upgradeTimes;
                        break;
                    case ShipBase._STAT_.STAT.mobility:
                        charStateMng.UpgradeTimesStat.mobility = upgradeTimes;
                        break;
                    case ShipBase._STAT_.STAT.shieldCapacity:
                        charStateMng.UpgradeTimesStat.shieldCapacity = upgradeTimes;
                        break;
                    case ShipBase._STAT_.STAT.power:
                        charStateMng.UpgradeTimesStat.power = upgradeTimes;
                        break;
                    case ShipBase._STAT_.STAT.weaponDamage:
                        charStateMng.UpgradeTimesStat.weaponDamage = upgradeTimes;
                        break;
                    case ShipBase._STAT_.STAT.weaponRange:
                        charStateMng.UpgradeTimesStat.weaponRange = upgradeTimes;
                        break;
                    case ShipBase._STAT_.STAT.weaponFireRate:
                        charStateMng.UpgradeTimesStat.weaponFireRate = upgradeTimes;
                        break;
                    default: throw new System.Exception();
                }
            });
    }
}
