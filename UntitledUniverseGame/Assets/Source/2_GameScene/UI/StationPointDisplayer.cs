﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class StationPointDisplayer : MonoBehaviour
{
    public WarpPoint warpPoint;

    // Start is called before the first frame update
    void Start()
    {
        this.ObserveEveryValueChanged(_ => CharStateMng.Instance.CurrentInfo.cur_WarpPoint).
            Subscribe(_ =>
            {
                gameObject.SetActive(warpPoint.currentPlanet.Equals(CharStateMng.Instance.CurrentInfo.cur_WarpPoint.currentPlanet));
            });
    }
}
