﻿using UnityEngine.EventSystems;
using UnityEngine;

public class AbilityPurchaseBtn : GameSceneButtonManager
{
    public AbilityBase abilityBase;
    private bool isPurchased = false;

    public override void OnPointerDown(PointerEventData e)
    {
        if (isPurchased)
            return;
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        if (isPurchased)
            return;
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        if (isPurchased)
            return;
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        if (isPurchased)
            return;
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerClick(PointerEventData e)
    {
        Color color = new Color(1f, 1f, 1f, 1f);
        btnText.color = color;
        btnText.text = "SOLD";
        isPurchased = true;
    }
}
