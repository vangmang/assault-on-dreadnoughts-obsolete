﻿using UnityEngine.EventSystems;
using UnityEngine;

public class QRFEmploymentBtn : GameSceneButtonManager
{
    public QRFMng qrfMng;
    public QRFInfo qrfInfo;
    public ShipBase.SHIP_TYPE shipType;

    public override void OnPointerDown(PointerEventData e)
    {
        if (qrfMng.total >= 10)
            return;
        switch(qrfInfo.Employment.shipType)
        {
            case ShipBase.SHIP_TYPE.frigate: 
                qrfMng.AddFrigate();
                break;
            case ShipBase.SHIP_TYPE.assaultFrigate: 
                qrfMng.AddAssaultFrigate(); 
                break;
            case ShipBase.SHIP_TYPE.fighter: 
                qrfMng.AddFighter();
                break;
            case ShipBase.SHIP_TYPE.crusier: 
                qrfMng.AddCruiser(); 
                break;
            default: return;
        }
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    protected override void ChangeColor(Color color)
    {
        idleImage.color = color;
    }
}
