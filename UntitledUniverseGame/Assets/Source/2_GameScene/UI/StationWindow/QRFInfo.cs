﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QRFInfo : MonoBehaviour
{
    [System.Serializable]
    public struct _EMPLOYMENT_
    {
        public ShipBase.SHIP_TYPE shipType;
        public int cost;
    }

    public _EMPLOYMENT_ Employment;
}
