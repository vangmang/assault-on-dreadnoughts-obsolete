﻿using UnityEngine.EventSystems;
using UnityEngine;

public class ReassembleBtn : GameSceneButtonManager
{
    [SerializeField] private QRFMng qrfMng;
    public override void OnPointerDown(PointerEventData e)
    {
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
        qrfMng.Reassemble();
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        ChangeColor(buttonSetting.exitColor);
    }
}
