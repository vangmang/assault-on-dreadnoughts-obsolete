﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class GoodsMng : MonoBehaviour
{
    public GameMng gameMng;
    public Text bountyText;
    public Text dreadnoughtSerialChipText;

    private void Start()
    {
        this.ObserveEveryValueChanged(_ => gameMng.gameStateMng.GetCurrentState).
            Where(state => state.Equals(GameStateMng.GAME_STATES.pauseAndShowStation)).
            Subscribe(_ =>
            {
                bountyText.text = gameMng.Goods.bountyAmountText.text;
                dreadnoughtSerialChipText.text = gameMng.Goods.dreadoughtSerialChipText.text;
            });
        this.ObserveEveryValueChanged(_ => gameMng.Goods.bountyAmount).
            Subscribe(amount =>
            {
                bountyText.text = amount.ToString();
            });
        this.ObserveEveryValueChanged(_ => gameMng.Goods.dreadnoughtSerialChip).
            Subscribe(amount =>
            {
                dreadnoughtSerialChipText.text = amount.ToString();
            });
        //bountyText.text = gameMng.Goods.bountyAmount.ToString();
        //dreadnoughtSerialChipText.text = gameMng.Goods.dreadnoughtSerialChip.ToString();
    }
}
