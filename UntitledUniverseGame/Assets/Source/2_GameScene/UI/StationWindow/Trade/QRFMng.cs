﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System;

public class QRFMng : MonoBehaviour
{
    public GoodsMng goodsMng;
    public QRFInfo frigateInfo;
    public QRFInfo assaultFrigateInfo;
    public QRFInfo fighterInfo;
    public QRFInfo cruiserInfo;

    public int frigateCount;
    public int assaultFrigateCount;
    public int fighterCount;
    public int cruiserCount;
    public int total;
    public int payment;

    public Text frigateCountText;
    public Text assaultFrigateCountText;
    public Text fighterCountText;
    public Text cruiserCountText;
    public Text totalText;
    public Text paymentText;
    private List<ShipBase.SHIP_TYPE> qrfMembers;

    // Start is called before the first frame update
    void Start()
    {
        qrfMembers = new List<ShipBase.SHIP_TYPE>();
        this.ObserveEveryValueChanged(_ => frigateCount).
            Subscribe(_ =>
            {
                frigateCountText.text = frigateCount.ToString();
            });
        this.ObserveEveryValueChanged(_ => assaultFrigateCount).
            Subscribe(_ =>
            {
                assaultFrigateCountText.text = assaultFrigateCount.ToString();
            });
        this.ObserveEveryValueChanged(_ => fighterCount).
            Subscribe(_ =>
            {
                fighterCountText.text = fighterCount.ToString();
            });
        this.ObserveEveryValueChanged(_ => cruiserCount).
            Subscribe(_ =>
            {
                cruiserCountText.text = cruiserCount.ToString();
            });
        this.ObserveEveryValueChanged(_ => total).
            Subscribe(_ =>
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder(total.ToString());
                sb.Append("/10");
                totalText.text = sb.ToString();
            });
        this.ObserveEveryValueChanged(_ => payment).
            Subscribe(_ =>
            {
                paymentText.text = payment.ToString();
            });
    }

    public void Reassemble()
    {
        frigateCount = 0;
        assaultFrigateCount = 0;
        fighterCount = 0;
        cruiserCount = 0;
        total = 0;
        payment = 0;
        qrfMembers.Clear();
    }

    public void Organize()
    {
        if (goodsMng.gameMng.QRFMemberOrganizationCheck || goodsMng.gameMng.Goods.bountyAmount - payment < 0 || total == 0)
            return;
        goodsMng.gameMng.Goods.bountyAmount -= payment;
        //goodsMng.gameMng.QRFMembers.CopyTo(qrfMembers.ToArray());
        qrfMembers.ForEach(member =>
        {
            goodsMng.gameMng.QRFMembers.Add(member);
        });
        goodsMng.gameMng.QRFMemberOrganizationCheck = true;
        Reassemble();
    }

    public void AddFrigate()
    {
        if (goodsMng.gameMng.QRFMemberOrganizationCheck)
            return;
        frigateCount++;
        total++;
        payment += frigateInfo.Employment.cost;
        qrfMembers.Add(ShipBase.SHIP_TYPE.frigate);
    }

    public void AddAssaultFrigate()
    {
        if (goodsMng.gameMng.QRFMemberOrganizationCheck)
            return;
        assaultFrigateCount++;
        total++;
        payment += assaultFrigateInfo.Employment.cost;
        qrfMembers.Add(ShipBase.SHIP_TYPE.assaultFrigate);
    }

    public void AddFighter()
    {
        if (goodsMng.gameMng.QRFMemberOrganizationCheck)
            return;
        fighterCount++;
        total++;
        payment += fighterInfo.Employment.cost;
        qrfMembers.Add(ShipBase.SHIP_TYPE.fighter);
    }

    public void AddCruiser()
    {
        if (goodsMng.gameMng.QRFMemberOrganizationCheck)
            return;
        cruiserCount++;
        total++;
        payment += cruiserInfo.Employment.cost;
        qrfMembers.Add(ShipBase.SHIP_TYPE.crusier);
    }

    private void OnDestroy()
    {
        
    }
}
