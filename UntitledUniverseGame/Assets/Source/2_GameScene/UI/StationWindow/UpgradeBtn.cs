﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UpgradeBtn : GameSceneButtonManager
{
    [SerializeField] StatDisplayer statDisplayer;

    public override void OnPointerDown(PointerEventData e)
    {
        if (statDisplayer.upgradeTimes == StatDisplayer.MaximumUpgrade)
            return;
        PlayPressButtonSound();
        ChangeColor(buttonSetting.pressedColor);
    }

    public override void OnPointerEnter(PointerEventData e)
    {
        if (statDisplayer.upgradeTimes == StatDisplayer.MaximumUpgrade)
            return;
        PlayEnterButtonSound();
        ChangeColor(buttonSetting.enterColor);
    }

    public override void OnPointerExit(PointerEventData e)
    {
        if (statDisplayer.upgradeTimes == StatDisplayer.MaximumUpgrade)
            return;
        ChangeColor(buttonSetting.exitColor);
    }

    public override void OnPointerUp(PointerEventData e)
    {
        if (statDisplayer.upgradeTimes == StatDisplayer.MaximumUpgrade)
            return;
        statDisplayer.UpgradeStat();
        ChangeColor(buttonSetting.exitColor);
    }
}
