﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class WeaponStyleDisplayer : MonoBehaviour
{
    [SerializeField] private GameMng gameMng;
    [SerializeField] private GameObject singleShot;
    [SerializeField] private GameObject burst;
    [SerializeField] private GameObject blaze;

    private GameObject currentStyle;
    private GameObject recentStyle;
    private int style;
    // Start is called before the first frame update
    void Start()
    {
        style = 0;
        currentStyle = singleShot;
        this.ObserveEveryValueChanged(_ => Input.mouseScrollDelta).
        Where(delta => Mathf.Abs(delta.y) > 0f).
        Where(_ =>
            !gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) &&
            !gameMng.gameStateMng.isShowingInfoState).
            Subscribe(_ =>
            {
                // 0보다 크면 위로 아니면 아래로
                bool scroll = Input.mouseScrollDelta.y > 0;
                int n_scroll = 0;
                if (scroll)
                {
                    n_scroll = ++style;
                    if (n_scroll > 2)
                        n_scroll = 0;
                }
                else
                {
                    n_scroll = --style;
                    if (n_scroll < 0)
                        n_scroll = 2;
                }
                style = n_scroll;

                recentStyle = currentStyle;
                switch (style)
                {
                    case 0: currentStyle = singleShot; break;
                    case 1: currentStyle = burst; break;
                    case 2: currentStyle = blaze; break;
                }
                try
                {
                    currentStyle.SetActive(true);
                    recentStyle.SetActive(false);
                }
                catch { }
            });
    }

    private void OnDestroy()
    {
        
    }
}
