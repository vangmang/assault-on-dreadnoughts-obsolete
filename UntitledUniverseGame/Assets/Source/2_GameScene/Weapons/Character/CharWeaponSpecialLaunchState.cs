﻿using FiniteStateMachine;
using System;

public class CharWeaponSpecialLaunchState : State<CharWeaponStateMng>
{
    public override Enum GetState
    {
        get { return CharWeaponStateMng.CHAR_WEAPON_STATES.specialLaunch; }
    }
    public override void Enter(params object[] o_Params)
    {
        instance.Info.LaunchParticle.gameObject.SetActive(true);
        instance.Info.LaunchParticle.Play();
        instance.charStateMng.CurrentInfo.cur_power -= instance.charStateMng.ConsumableStat.AttackConsumption;

        ++instance.CurrentInfo.currentChargeCount;
        instance.CurrentInfo.currentChargeCount %= instance.Info.MaxChargeCount;
        var projectile = instance.Info.ProjectileStateMngArr[instance.CurrentInfo.currentChargeCount];
        projectile.gameObject.SetActive(true);
        try
        {
            projectile.StateTransition(ProjectileStateMng.PROJECTILE_STATES.fired);
        }
        catch { }

        instance.charStateMng.charBuildMng.currentBuild(projectile);
        instance.charStateMng.ResetPowerRecovery();
        instance.StateTransition(CharWeaponStateMng.CHAR_WEAPON_STATES.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
