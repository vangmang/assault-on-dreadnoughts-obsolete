﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UniRx;
using System;

public class CharWeaponStateMng : WeaponBase
{
    public enum CHAR_WEAPON_STATES
    {
        idle,
        launch,
        specialLaunch
    }

    [Serializable]
    public struct _WEAPON_PARTICLES_
    {
        public ParticleSystem shieldVampireParticle;
        public ParticleSystem powerVampireParticle;
    }
    public _WEAPON_PARTICLES_ WeaponParticles;


    public CharStateMng charStateMng;

    private Vector3 targetPredictionPoint;
    public Vector3 TargetPredictionPoint { get { return targetPredictionPoint; } }
    //public Dictionary<int, CharacterBuildMng.build> AppliedBuildTable { get; private set; }

    private void Awake()
    {
        TargetingModuleStateMng = charStateMng.Info.TargetingModuleStateMng;
        CurrentInfo.currentChargeCount = Info.MaxChargeCount;
        InitializeState();
    }

    // Start is called before the first frame update
    private void Start()
    {
        StateEnter(CHAR_WEAPON_STATES.idle);
        Stat.fireRate = shipBase.Stat.weaponFireRate;
        Stat.damage = shipBase.Stat.weaponDamage;
        this.ObserveEveryValueChanged(_ => shipBase.Stat.weaponFireRate).
            Subscribe(_ =>
            {
                Stat.fireRate = shipBase.Stat.weaponFireRate;
            });
        this.ObserveEveryValueChanged(_ => shipBase.Stat.weaponDamage).
            Subscribe(_ =>
            {
                Stat.damage = shipBase.Stat.weaponDamage;
            });

        #region SingleShot
        // 단발
        Observable.EveryUpdate().Where(_ => Input.GetMouseButtonDown(0)).
            Where(_ => !charStateMng.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) 
                        && !charStateMng.gameMng.gameStateMng.isShowingInfoState).
            Where(_ => weaponStyle.Equals(WEAPON_STYLE.singleShot)).
            Where(_ => !charStateMng.GetCurrentState.Equals(CharStateMng.CHARACTER_STATES.warp)).
            Where(_ => !charStateMng.GetCurrentState.Equals(CharStateMng.CHARACTER_STATES.microWormholePenetate)).
            Timestamp().
            Where(x => x.Timestamp > CurrentInfo.lastFired.AddSeconds(Stat.fireRate)).
            Where(_ => charStateMng.CurrentInfo.cur_power > 0f &&
                       charStateMng.CurrentInfo.cur_power - charStateMng.ConsumableStat.overallAttackConsumption > 0f).
            Subscribe(x =>
            {
                charStateMng.CurrentInfo.isPowerRecoveryEnable = false;
                var state = Input.GetKey(KeyCode.LeftShift) ? CHAR_WEAPON_STATES.specialLaunch : CHAR_WEAPON_STATES.launch;
                StateTransition(state);
                charStateMng.gameMng.mouseFollowingCamera.additionalRadian = 10f;
                CurrentInfo.lastFired = x.Timestamp;
            }).AddTo(this);
        #endregion
        #region Burst
        // 점사
        Observable.EveryUpdate().Where(_ => Input.GetMouseButtonDown(0)).
            Where(_ => !charStateMng.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause)
                        && !charStateMng.gameMng.gameStateMng.isShowingInfoState).
            Where(_ => weaponStyle.Equals(WEAPON_STYLE.burst)).
            Where(_ => !charStateMng.GetCurrentState.Equals(CharStateMng.CHARACTER_STATES.warp)).
            Where(_ => !charStateMng.GetCurrentState.Equals(CharStateMng.CHARACTER_STATES.microWormholePenetate)).
            Timestamp().
            Where(x => x.Timestamp > CurrentInfo.lastFired.AddSeconds(Stat.fireRate * 2.5f)).
            Where(_ => charStateMng.CurrentInfo.cur_power > 0f &&
                       charStateMng.CurrentInfo.cur_power - charStateMng.ConsumableStat.overallAttackConsumption > 0f).
            Subscribe(x =>
            {
                try // 공격 도중 메인메뉴로 나갈 수 있으므로 예외처리해줌
                {
                    IEnumerator burstCall = launchBurstWeapon(Input.GetKey(KeyCode.LeftShift));
                    StartCoroutine(burstCall);
                }
                catch { }
                CurrentInfo.lastFired = x.Timestamp;
            }).AddTo(this);
        #endregion
        #region Blaze
        // 연발
        Observable.EveryUpdate().Where(_ => Input.GetMouseButtonDown(0)).
            Where(_ => !charStateMng.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause)
                        && !charStateMng.gameMng.gameStateMng.isShowingInfoState).
            Where(_ => weaponStyle.Equals(WEAPON_STYLE.blaze)).
            Where(_ => !charStateMng.GetCurrentState.Equals(CharStateMng.CHARACTER_STATES.warp)).
            Where(_ => !charStateMng.GetCurrentState.Equals(CharStateMng.CHARACTER_STATES.microWormholePenetate)).
            Timestamp().
            Where(x => x.Timestamp > CurrentInfo.lastFired.AddSeconds(Stat.fireRate * 2.5f)).
            Where(_ => charStateMng.CurrentInfo.cur_power > 0f &&
                       charStateMng.CurrentInfo.cur_power - charStateMng.ConsumableStat.overallAttackConsumption > 0f).
            Subscribe(x =>
            {
                try // 공격 도중 메인메뉴로 나갈 수 있으므로 예외처리해줌
                {
                    StartCoroutine(launchBlazeWeapon());
                }
                catch { }
                CurrentInfo.lastFired = x.Timestamp;
            }).AddTo(this);
        #endregion
        this.ObserveEveryValueChanged(_ => Input.mouseScrollDelta).
            Where(_ => !charStateMng.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !charStateMng.gameMng.gameStateMng.isShowingInfoState).
            Where(delta => Mathf.Abs(delta.y) > 0f).
            Subscribe(_ =>
            {
                // 0보다 크면 위로 아니면 아래로
                bool scroll = Input.mouseScrollDelta.y > 0;
                int n_scroll = 0;
                if (scroll)
                {
                    n_scroll = (int)++weaponStyle;
                    if (n_scroll >= (int)WEAPON_STYLE.max)
                        n_scroll = 0;
                }
                else
                {
                    n_scroll = (int)--weaponStyle;
                    if (n_scroll < 0)
                        n_scroll = 2;
                }
                weaponStyle = (WEAPON_STYLE)n_scroll;
            });
    }

    private IEnumerator launchBurstWeapon(bool special = false)
    {
        var state = special ? CHAR_WEAPON_STATES.specialLaunch : CHAR_WEAPON_STATES.launch;

        for (int i = 0; i < 3; i++)
        {
            if (charStateMng.CurrentInfo.cur_power < 0f ||
                charStateMng.CurrentInfo.cur_power - charStateMng.ConsumableStat.overallAttackConsumption < 0f)
                yield break;
            yield return new WaitForSeconds(Stat.fireRate * 0.575f); // 0.092
            charStateMng.CurrentInfo.isPowerRecoveryEnable = false;
            charStateMng.gameMng.mouseFollowingCamera.additionalRadian = 10f;
            StateTransition(state);
        }
    }

    private IEnumerator launchBlazeWeapon()
    {
        while (Input.GetMouseButton(0))
        {
            if (charStateMng.CurrentInfo.cur_power < 0f ||
                charStateMng.CurrentInfo.cur_power - charStateMng.ConsumableStat.overallAttackConsumption < 0f)
                yield break;
            yield return new WaitForSeconds(Stat.fireRate * 0.575f);
            charStateMng.CurrentInfo.isPowerRecoveryEnable = false;
            var state = Input.GetKey(KeyCode.LeftShift) ? CHAR_WEAPON_STATES.specialLaunch : CHAR_WEAPON_STATES.launch;
            charStateMng.gameMng.mouseFollowingCamera.additionalRadian = 10f;
            StateTransition(state);
        }
    }

    private void Update()
    {
        Execute();
        PredictTarget(out targetPredictionPoint);
    }

    private void OnDestroy()
    {

    }
}
