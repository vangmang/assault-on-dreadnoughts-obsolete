﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//public enum ACTIVE_WEAPON_TYPE
//{
//    blast = 0, // 강공격           // 동력을 2배 소모하고 적중시 1.5배 강력한 데미지를 준다.
//    explosion = 1, // 폭발             // 적중시 폭발하며 주변에 기본 데미지의 0.75배에 해당하는 데미지를 준다. 
//    shieldVampire = 2, // 쉴드 흡수        // 적중시 쉴드를 일정량 흡수한다.
//    powerVampire = 3, // 동력 흡수        // 적중시 동력을 일정량 흡수한다.
//    hullExposure = 4, // 약점 노출        // 적중시 0.25배의 추가 데미지를 주고 최대 5번 중첩된 데미지를 준다.
//    corrosiveExplosion = 5, // 부식성 폭발      // 공격 4번 적중시 기본 데미지의 2배에 해당하는 고정 데미지를 준다.
//    stasis = 6, // 이동속도 감소    // 적중시 타겟의 이동속도가 10%감소하고 최대 5번 중첩된다. 
//    hacking = 7, // 공격속도 감소    // 적중시 타겟의 공격속도가 10%감소하고 최대 5번 중첩된다.
//}
public class CharacterBuild : MonoBehaviour
{
    [SerializeField] private CharacterBuildMng buildMng;
    [Serializable]
    public struct _AUDIOS_
    {
        public AudioSource blastAudio;
        public AudioSource explosionAudio;
        public AudioSource hullExposureAudio;
        public AudioSource corrosiveExplosionAudio;
        public AudioSource shieldVampireAudio;
        public AudioSource powerVampireAudio;
        public AudioSource stasisAudio;
        public AudioSource hackingAudio;
    }
    public _AUDIOS_ Audios;

    private float stasisTime;
    private IEnumerator _invokeStasis;

    //public CharacterBuildMng.BUILD_TYPE buildType;
    //public CharacterBuildMng.ACTIVE_WEAPON_TYPE activeWeaponType;

    private void Start()
    {
        stasisTime = 3f;
    }

    public CharacterBuildMng.BUILD_TYPE ActivateBlast(ProjectileStateMng projectile)
    {
        // 단발모드에서만 사용가능
        if (projectile.WeaponStateMng.weaponStyle.Equals(WeaponBase.WEAPON_STYLE.singleShot))
        {
            Audios.blastAudio.Play();
            projectile.Info.ProjectileTrail.startColor = new Color(0.5294118f, 0.2286528f, 0f, 1f);
            projectile.Info.ProjectileTrail.endColor = new Color(0.3396226f, 0.06915682f, 0.05606977f, 1f);

            projectile.Info.ProjectileTrail.time = 0.15f;
            projectile.Info.ProjectileTrail.startWidth = 15f;
            projectile.Info.ProjectileTrail.endWidth = 2.5f;

            // 공격력 1.5배 증가, 동력소모 두 배 증가
            projectile.WeaponStateMng.getShipBase.CurrentInfo.cur_power -=
                projectile.WeaponStateMng.getShipBase.ConsumableStat.AttackConsumption * 0.5f;
            projectile.SpecialAttackHitEffect += (hitObject) =>
            {
                projectile.CurrentInfo.damage *= 1.5f;
            };
        }
        else
        {
            projectile.WeaponStateMng.Info.LaunchAudio.Play();
        }
        return CharacterBuildMng.BUILD_TYPE.normal;
    }

    public CharacterBuildMng.BUILD_TYPE ActivateExplosion(ProjectileStateMng projectile)
    {
        Audios.explosionAudio.Play();
        projectile.Info.ProjectileTrail.startColor = new Color(0f, 0.5294118f, 0.5024346f, 1f);
        projectile.Info.ProjectileTrail.endColor = new Color(0f, 0.06374557f, 0.509434f, 1f);

        projectile.Info.ProjectileTrail.time = 0.15f;
        projectile.Info.ProjectileTrail.startWidth = 15f;
        projectile.Info.ProjectileTrail.endWidth = 2.5f;
        projectile.SpecialAttackHitEffect += (hitObject) =>
        {
            //Debug.Log("explosion");
        };
        return CharacterBuildMng.BUILD_TYPE.normal;
    }

    public CharacterBuildMng.BUILD_TYPE ActivateShieldVampire(ProjectileStateMng projectile)
    {
        // 단발 혹은 점사모드에서 사용가능
        if (!projectile.WeaponStateMng.weaponStyle.Equals(WeaponBase.WEAPON_STYLE.blaze))
        {
            Audios.shieldVampireAudio.Play();
            var charWeaponStateMng = projectile.WeaponStateMng as CharWeaponStateMng;
            charWeaponStateMng.WeaponParticles.shieldVampireParticle.Play();

            // 일정량의 쉴드 흡수
            projectile.SpecialAttackHitEffect += (hitObject) =>
            {
                projectile.WeaponStateMng.getShipBase.CurrentInfo.cur_ShieldCapacity += 10f;
                if (projectile.WeaponStateMng.getShipBase.CurrentInfo.cur_ShieldCapacity > projectile.WeaponStateMng.getShipBase.Stat.shieldCapacity)
                    projectile.WeaponStateMng.getShipBase.CurrentInfo.cur_ShieldCapacity = projectile.WeaponStateMng.getShipBase.Stat.shieldCapacity;
            };
        }
        else
        {
            projectile.WeaponStateMng.Info.LaunchAudio.Play();
        }

        return CharacterBuildMng.BUILD_TYPE.vampire;
    }

    public CharacterBuildMng.BUILD_TYPE ActivatePowerVampire(ProjectileStateMng projectile)
    {
        // 단발 혹은 점사모드에서 사용가능
        if (!projectile.WeaponStateMng.weaponStyle.Equals(WeaponBase.WEAPON_STYLE.blaze))
        {
            Audios.powerVampireAudio.Play();
            var charWeaponStateMng = projectile.WeaponStateMng as CharWeaponStateMng;
            charWeaponStateMng.WeaponParticles.powerVampireParticle.Play();

            // 일정량의 동력 흡수
            projectile.SpecialAttackHitEffect += (hitObject) =>
            {
                projectile.WeaponStateMng.getShipBase.CurrentInfo.cur_power += 5f;
                if (projectile.WeaponStateMng.getShipBase.CurrentInfo.cur_power > projectile.WeaponStateMng.getShipBase.Stat.power)
                    projectile.WeaponStateMng.getShipBase.CurrentInfo.cur_power = projectile.WeaponStateMng.getShipBase.Stat.power;
            };
        }
        else
        {
            projectile.WeaponStateMng.Info.LaunchAudio.Play();
        }

        return CharacterBuildMng.BUILD_TYPE.vampire;
    }

    public CharacterBuildMng.BUILD_TYPE ActivateHullExposure(ProjectileStateMng projectile)
    {
        Audios.hullExposureAudio.Play();
        projectile.Info.ProjectilieSubTrail.startColor = new Color(0.5294118f, 0.2286528f, 0f, 1f);
        projectile.Info.ProjectilieSubTrail.endColor = new Color(0.3396226f, 0.06915682f, 0.05606977f, 1f);
        projectile.Info.ProjectilieSubTrail.gameObject.SetActive(true);

        projectile.SpecialAttackHitEffect += (hitObject) =>
        {
        };
        return CharacterBuildMng.BUILD_TYPE.exposure;
    }

    public CharacterBuildMng.BUILD_TYPE ActivateCorrosiveExplosion(ProjectileStateMng projectile)
    {
        Audios.corrosiveExplosionAudio.Play();
        projectile.Info.ProjectilieSubTrail.startColor = new Color(0f, 0.5294118f, 0.5024346f, 1f);
        projectile.Info.ProjectilieSubTrail.endColor = new Color(0f, 0.06374557f, 0.509434f, 1f);
        projectile.Info.ProjectilieSubTrail.gameObject.SetActive(true);
        projectile.SpecialAttackHitEffect += (hitObject) =>
        {
            //Debug.Log("corrosiveExplosion");
        };
        return CharacterBuildMng.BUILD_TYPE.exposure;
    }

    public CharacterBuildMng.BUILD_TYPE ActivateStasis(ProjectileStateMng projectile)
    {
        // 단발모드에서만 사용가능
        if (projectile.WeaponStateMng.weaponStyle.Equals(WeaponBase.WEAPON_STYLE.singleShot))
        {
            Audios.stasisAudio.Play();
            projectile.SpecialAttackHitEffect += (hitObject) =>
            {
                GameObject _hitObject = (GameObject)hitObject[0];
                Action<ShipBase> action = (shipBase) =>
                {
                    if (_invokeStasis != null)
                        StopCoroutine(StartCoroutine(_invokeStasis));
                    else
                        shipBase.Stat.velocity *= 0.75f;
                    _invokeStasis = invokeStasis(shipBase);
                    StartCoroutine(_invokeStasis);

                };
                _hitObject.SendMessage("TakeAbnormalStateFromEnemy", action, SendMessageOptions.DontRequireReceiver);
            };
        }
        else
        {
            projectile.WeaponStateMng.Info.LaunchAudio.Play();
        }

        return CharacterBuildMng.BUILD_TYPE.utility;
    }

    private IEnumerator invokeStasis(ShipBase shipBase)
    {
        float t = 0f;
        while (t < stasisTime)
        {
            t += Time.deltaTime;
            yield return null;
        }
        shipBase.Stat.velocity = shipBase.OriginVelocity;
        _invokeStasis = null;
    }

    public CharacterBuildMng.BUILD_TYPE ActivateHacking(ProjectileStateMng projectile)
    {
        Audios.hackingAudio.Play();
        projectile.SpecialAttackHitEffect += (hitObject) =>
        {
            projectile.WeaponStateMng.CurrentInfo.ECMvalue = 10f;
        };
        return CharacterBuildMng.BUILD_TYPE.utility;
    }
}
