﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class CharacterBuildMng : MonoBehaviour
{
    [SerializeField] private CharStateMng charStateMng;
    [SerializeField] private CharacterBuild characterBuild;
    [SerializeField] private Transform leftSelectedBuild;
    [SerializeField] private Transform rightSelectedBuild;

    public enum BUILD_TYPE // 빌드 중복을 막기 위한 타입. 같은 타입끼리 묶을 수 없다.
    {
        normal,     // 강공격, 폭발
        vampire,    // 쉴드 흡수, 동력 흡수
        exposure,   // 약점 노출, 부식성 폭발
        utility     // 이동속도 감소, 공격속도 감소
    }

    public enum ACTIVE_WEAPON_TYPE
    {
        blast               = 0, // 강공격           // 동력을 2배 소모하고 적중시 1.5배 강력한 데미지를 준다.
        explosion           = 1, // 폭발             // 적중시 폭발하며 주변에 기본 데미지의 0.75배에 해당하는 데미지를 준다. 
        shieldVampire       = 2, // 쉴드 흡수        // 적중시 쉴드를 일정량 흡수한다.
        powerVampire        = 3, // 동력 흡수        // 적중시 동력을 일정량 흡수한다.
        hullExposure        = 4, // 약점 노출        // 적중시 0.25의 추가 데미지를 주고 최대 5번 중첩된 데미지를 준다.
        corrosiveExplosion  = 5, // 부식성 폭발      // 공격 4번 적중시 기본 데미지의 2배에 해당하는 고정 데미지를 준다.
        stasis              = 6, // 이동속도 감소    // 적중시 타겟의 이동속도가 10%감소하고 최대 5번 중첩된다. 
        hacking             = 7, // 공격속도 감소    // 적중시 타겟의 공격속도가 10%감소하고 최대 5번 중첩된다.
    }

    public int Left { get; private set; }
    public int Right { get; private set; }
    public delegate BUILD_TYPE build(ProjectileStateMng projectile);
    public Dictionary<ACTIVE_WEAPON_TYPE, build> BuildTable { get; private set; }
    public Dictionary<ACTIVE_WEAPON_TYPE, BUILD_TYPE> BuildTypeTable { get; private set; }
    public Dictionary<int, build> CurrentBuildTable { get; private set; }
    public Dictionary<ACTIVE_WEAPON_TYPE, int> BuildTableForSetting { get; private set; }

    public void currentBuild(ProjectileStateMng projectile)
    {
        CurrentBuildTable[Left](projectile);
        CurrentBuildTable[Right](projectile);
    }

    public void currentBuild(ProjectileStateMng projectile, Action action)
    {
        CurrentBuildTable[Left](projectile);
        CurrentBuildTable[Right](projectile);
        action();
    }
    private void Awake()
    {
        #region BuildTable
        BuildTable = new Dictionary<ACTIVE_WEAPON_TYPE, build>();
        BuildTable.Add(ACTIVE_WEAPON_TYPE.blast, characterBuild.ActivateBlast);
        BuildTable.Add(ACTIVE_WEAPON_TYPE.explosion, characterBuild.ActivateExplosion);
        BuildTable.Add(ACTIVE_WEAPON_TYPE.shieldVampire, characterBuild.ActivateShieldVampire);
        BuildTable.Add(ACTIVE_WEAPON_TYPE.powerVampire, characterBuild.ActivatePowerVampire);
        BuildTable.Add(ACTIVE_WEAPON_TYPE.hullExposure, characterBuild.ActivateHullExposure);
        BuildTable.Add(ACTIVE_WEAPON_TYPE.corrosiveExplosion, characterBuild.ActivateCorrosiveExplosion);
        BuildTable.Add(ACTIVE_WEAPON_TYPE.stasis, characterBuild.ActivateStasis);
        BuildTable.Add(ACTIVE_WEAPON_TYPE.hacking, characterBuild.ActivateHacking);
        #endregion

        #region BuildTypeTable
        BuildTypeTable = new Dictionary<ACTIVE_WEAPON_TYPE, BUILD_TYPE>();
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.blast, BUILD_TYPE.normal);
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.explosion, BUILD_TYPE.normal);
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.shieldVampire, BUILD_TYPE.vampire);
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.powerVampire, BUILD_TYPE.vampire);
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.hullExposure, BUILD_TYPE.exposure);
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.corrosiveExplosion, BUILD_TYPE.exposure);
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.stasis, BUILD_TYPE.utility);
        BuildTypeTable.Add(ACTIVE_WEAPON_TYPE.hacking, BUILD_TYPE.utility);
        #endregion

        #region CurrentBuildTable
        CurrentBuildTable = new Dictionary<int, build>();
        // Default Setting: blast / shieldVampire / hullExposure / stasis / explosion / powerVampire / corrosiveExplosion / hacking
        CurrentBuildTable.Add(0, characterBuild.ActivateBlast);
        CurrentBuildTable.Add(1, characterBuild.ActivateShieldVampire);
        CurrentBuildTable.Add(2, characterBuild.ActivateHullExposure);
        CurrentBuildTable.Add(3, characterBuild.ActivateStasis);
        CurrentBuildTable.Add(4, characterBuild.ActivateExplosion);
        CurrentBuildTable.Add(5, characterBuild.ActivatePowerVampire);
        CurrentBuildTable.Add(6, characterBuild.ActivateCorrosiveExplosion);
        CurrentBuildTable.Add(7, characterBuild.ActivateHacking);
        BuildTableForSetting = new Dictionary<ACTIVE_WEAPON_TYPE, int>();
        /* Default */
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.blast, 0);
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.shieldVampire, 1);
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.hullExposure, 2);
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.stasis, 3);
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.explosion, 4);
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.powerVampire, 5);
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.corrosiveExplosion, 6);
        BuildTableForSetting.Add(ACTIVE_WEAPON_TYPE.hacking, 7);
        #endregion
    }

    private void Start()
    {
        Left = 0;
        Right = 1;
        Observable.EveryUpdate().
            Where(_ => Input.GetKeyDown(KeyCode.Q)).
            Where(_ => !charStateMng.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !charStateMng.gameMng.gameStateMng.isShowingInfoState).
            Subscribe(_ =>
            {
                if (Left - 1 < 0)
                    return;
                Left--;
                Right--;
                leftSelectedBuild.localPosition = new Vector3(Left * 60f, 0f);
                rightSelectedBuild.localPosition = new Vector3(Right * 60f, 0f);
            }).AddTo(this);
        Observable.EveryUpdate().
            Where(_ => Input.GetKeyDown(KeyCode.E)).
            Where(_ => !charStateMng.gameMng.gameStateMng.GetCurrentState.Equals(GameStateMng.GAME_STATES.pause) && !charStateMng.gameMng.gameStateMng.isShowingInfoState).
            Subscribe(_ =>
            {
                if (Right + 1 > (int)ACTIVE_WEAPON_TYPE.hacking)
                    return;
                Left++;
                Right++;
                leftSelectedBuild.localPosition = new Vector3(Left * 60f, 0f);
                rightSelectedBuild.localPosition = new Vector3(Right * 60f, 0f);
            }).AddTo(this);
    }

    private void OnDestroy()
    {
        
    }
}
