﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DreadnoughtWeaponCollisionDetector : MonoBehaviour
{
    public DreadnoughtWeaponBase dreadnoughtWeaponBase;

    public void TakeDamage(ProjectileStateMng projectile)
    {
        dreadnoughtWeaponBase.TakeDamage(projectile);
    }

    public void TakeDamage(object[] arr)
    {
        dreadnoughtWeaponBase.TakeDamage(arr);
    }
}
