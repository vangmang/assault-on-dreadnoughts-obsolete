﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyDreadnoughtIonCannonDestroyedCompleteState : State<EnemyDreadnoughtIonCannonStateMng>
{
    public override Enum GetState
    {
        get { return EnemyDreadnoughtIonCannonStateMng.ENEMY_DREADNOUGHT_IONCANNON_STATES.destroyedComplete; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.getShipBase.TakeECMdamage(null);
        instance.PlayDestroyedCompleteParticle();
        instance.CurrentInfo.currentHitpoints = 0f;
        instance.CurrentInfo.currentECCM_Capacity = 0f;
        instance.Info.WeaponCollider.enabled = false;
        try // MissingReference
        {
            Destroy(instance.IndicatorTransform.gameObject);
            instance.PlayDestroyedCompleteAudio();
        }
        catch { }
        EnemyManager.Instance.targets.Remove(instance);
        try
        {
            Destroy(instance.gameObject);
        }
        catch { }
    }

    public override void Execute()
    {        
    }

    public override void Exit()
    {
    }
}
