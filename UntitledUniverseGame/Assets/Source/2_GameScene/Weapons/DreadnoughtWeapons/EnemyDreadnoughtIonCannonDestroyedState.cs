﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyDreadnoughtIonCannonDestroyedState : State<EnemyDreadnoughtIonCannonStateMng>
{
    public override Enum GetState
    {
        get { return EnemyDreadnoughtIonCannonStateMng.ENEMY_DREADNOUGHT_IONCANNON_STATES.destroyed; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.PlayDestroyedParticle();
        instance.PlayDestroyedAudio();
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
