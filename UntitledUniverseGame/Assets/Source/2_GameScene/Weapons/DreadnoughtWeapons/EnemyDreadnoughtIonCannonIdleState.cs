﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyDreadnoughtIonCannonIdleState : State<EnemyDreadnoughtIonCannonStateMng>
{
    public override Enum GetState
    {
        get { return EnemyDreadnoughtIonCannonStateMng.ENEMY_DREADNOUGHT_IONCANNON_STATES.idle; }
    }
    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
