﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyDreadnoughtIonCannonLaunchState : State<EnemyDreadnoughtIonCannonStateMng>
{
    public override Enum GetState
    {
        get { return EnemyDreadnoughtIonCannonStateMng.ENEMY_DREADNOUGHT_IONCANNON_STATES.launch; }
    }
    public override void Enter(params object[] o_Params)
    {
        instance.LaunchParticle1.gameObject.SetActive(true);
        instance.LaunchParticle2.gameObject.SetActive(true);
        instance.LaunchParticle3.gameObject.SetActive(true);

        instance.LaunchParticle1.Play();
        instance.LaunchParticle2.Play();
        instance.LaunchParticle3.Play();
        instance.Info.LaunchAudio.Play();
        /* gun Transform */
        var obj = (Transform)o_Params[0];
        instance.getShipBase.CurrentInfo.cur_power -= instance.getShipBase.ConsumableStat.AttackConsumption;
        for (int i = 0; i < 3; i++)
        {
            //instance.CurrentInfo.currentCharge = --instance.CurrentInfo.currentCharge % instance.Info.MaxCharge;
            //int index = -instance.CurrentInfo.currentCharge;
            ++instance.CurrentInfo.currentChargeCount;
            instance.CurrentInfo.currentChargeCount %= instance.Info.MaxChargeCount;
            var dreadnoughtProjectile = instance.Info.DreadnoughtProjectiles[instance.CurrentInfo.currentChargeCount]; //Instantiate(instance.Info.DreadnoughtProjectilePrefab);
            //dreadnoughtProjectile.DreadnoughtWeaponStateMng = instance;
            //dreadnoughtProjectile.CurrentInfo.currentVelocity = instance.Stat.MaxVelocity;
            dreadnoughtProjectile.weaponLaunchTransform = obj;
            dreadnoughtProjectile.additionalPosition = obj.right * (12.5f * (i - 1));
            instance.Info.DreadnoughtProjectiles[instance.CurrentInfo.currentChargeCount].gameObject.SetActive(true);
            try
            {
                instance.Info.DreadnoughtProjectiles[instance.CurrentInfo.currentChargeCount].
                    StateTransition(IonCannonProjectileStateMng.ION_CANNON_PROJECTILE_STATES.fired, obj, obj.right * (8.5f * (i - 1)));
            }
            catch { }
        }

        instance.getShipBase.ResetPowerRecovery();
        instance.StateTransition(EnemyDreadnoughtIonCannonStateMng.ENEMY_DREADNOUGHT_IONCANNON_STATES.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
