﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class EnemyDreadnoughtIonCannonStateMng : DreadnoughtWeaponBase
{
    public enum ENEMY_DREADNOUGHT_IONCANNON_STATES
    {
        idle,
        launch,
        destroyed, // 플레이어가 전자전 공격 가능
        destroyedComplete // 완전히 파괴된 상태.  
    }

    public bool IsAttackable { get; private set; }

    [Header("Unable to attack from following angles")]
    [SerializeField] private float minModuleRotationLimit;
    [SerializeField] private float maxModuleRotationLimit;

    [SerializeField] private Transform gun;
    [SerializeField] private Transform turret;
    [SerializeField] private ParticleSystem launchParticle1;
    [SerializeField] private ParticleSystem launchParticle2;
    [SerializeField] private ParticleSystem launchParticle3;
    [SerializeField] private ParticleSystem destroyedParticle;
    [SerializeField] private ParticleSystem destroyedCompleteParticle;
    [SerializeField] private ParticleSystem destroyedSmokeParticle;
    [SerializeField] private AudioSource launcherBootUpAudio;
    [SerializeField] private AudioSource launcherDestroyedAudio;
    [SerializeField] private AudioSource launcherDestroyedCompleteAudio;

    public ParticleSystem LaunchParticle1 => launchParticle1;
    public ParticleSystem LaunchParticle2 => launchParticle2;
    public ParticleSystem LaunchParticle3 => launchParticle3;
    public ParticleSystem DestroyedParticle => destroyedParticle;
    public ParticleSystem DestroyedCompleteParticle => destroyedCompleteParticle;
    public ParticleSystem DestroyedSmokeParticle => destroyedSmokeParticle;

    public CharStateMng charStateMng { get; private set; }
    //private bool isRotatable;

    private void Awake()
    {
        InitializeState();
        charStateMng = CharStateMng.Instance;
        TargetingModuleStateMng = shipBase.Info.TargetingModuleStateMng;
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentInfo.currentHitpoints = Stat.hitpoints;
        CurrentInfo.currentECCM_Capacity = Stat.maxECCM_Capacity;

        StateEnter(ENEMY_DREADNOUGHT_IONCANNON_STATES.idle);
        this.ObserveEveryValueChanged(_ => CharStateMng.Instance.CurrentInfo.cur_WarpPoint).
            Where(point => shipBase.CurrentInfo.cur_WarpPoint.Equals(point)).
            Subscribe(point =>
            {
                point.createEnemies.AddEnmiesIntoLocalArea(this);
            });
        this.ObserveEveryValueChanged(_ => shipBase.Info.TargetingModuleStateMng.currentTarget).
            Timestamp().
            Where(x => x.Timestamp > CurrentInfo.lastFired.AddSeconds(Stat.maxFireRate * 15f)).
            Where(_ => shipBase.Info.TargetingModuleStateMng.currentTarget != null).
            Subscribe(x =>
            {
                StartCoroutine(launchWeapons());
                CurrentInfo.lastFired = x.Timestamp;
            });
        this.ObserveEveryValueChanged(_ => CurrentInfo.currentHitpoints).
            Where(hitpoints => hitpoints <= 1f).
            Subscribe(_ =>
            {
                StateTransition(ENEMY_DREADNOUGHT_IONCANNON_STATES.destroyed);
            });
        this.ObserveEveryValueChanged(_ => CurrentInfo.currentECCM_Capacity).
            Where(capacity => capacity <= 0f).
            Subscribe(_ =>
            {
                StateTransition(ENEMY_DREADNOUGHT_IONCANNON_STATES.destroyedComplete);
            });
        this.ObserveEveryValueChanged(_ => shipBase.CurrentInfo.cur_ShieldCapacity).
            //Where(_ => !shipBase.CurrentInfo.isShipDown).
            Where(hitpoints => hitpoints <= 0f).
            Subscribe(_ =>
            {
                StateTransition(ENEMY_DREADNOUGHT_IONCANNON_STATES.destroyedComplete);
            });
        this.ObserveEveryValueChanged(_ => shipBase.CurrentInfo.cur_ECCM_capacity).
            //Where(_ => !shipBase.CurrentInfo.isShipDown).
            Where(capacity => capacity <= 0f).
            Subscribe(_ =>
            {
                StateTransition(ENEMY_DREADNOUGHT_IONCANNON_STATES.destroyedComplete);
            });
        InitInstantiation();
    }

    private IEnumerator launchWeapons()
    {
        if (IsAttackable)
            launcherBootUpAudio.Play();
        for (int i = 0; i < 3; i++)
        {
            if (IsAttackable)
            {
                if (shipBase.Info.TargetingModuleStateMng.currentTarget == null || CurrentInfo.currentECCM_Capacity <= 0f || CurrentInfo.currentHitpoints <= 1f)
                    yield break;
                StateTransition(ENEMY_DREADNOUGHT_IONCANNON_STATES.launch, gun);
                yield return new WaitForSeconds(Stat.maxFireRate);
            }
            else
                break;
        }
        float t = 0f;
        while (t < Stat.maxFireRate * 15f)
        {
            t += Time.deltaTime;
            if (shipBase.Info.TargetingModuleStateMng.currentTarget == null || CurrentInfo.currentECCM_Capacity <= 0f || CurrentInfo.currentHitpoints <= 1f)
                yield break;
            yield return null;
        }
        yield return StartCoroutine(launchWeapons());
    }

    public void PlayDestroyedAudio()
    {
        launcherDestroyedAudio.transform.SetParent(null);
        launcherDestroyedAudio.Play();
    }

    public void PlayDestroyedCompleteAudio()
    {
        launcherDestroyedCompleteAudio.transform.SetParent(null);
        launcherDestroyedCompleteAudio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (shipBase.Info.TargetingModuleStateMng.currentTarget != null && CurrentInfo.currentHitpoints > 1)
        {
            rotateTurret();
            rotateGun();
        }
    }

    public void PlayDestroyedParticle()
    {
        destroyedParticle.Play();
        destroyedSmokeParticle.transform.SetParent(shipBase.Info.ShipTransform);
        destroyedSmokeParticle.Play();
    }

    public void PlayDestroyedCompleteParticle()
    {
        destroyedCompleteParticle.transform.SetParent(shipBase.Info.ShipTransform);
        destroyedCompleteParticle.Play();
    }

    public override void TakeDamage(object[] o_Params)
    {
        float damage = (float)o_Params[0];
        ShipBase shipBase = (ShipBase)o_Params[1];
        CurrentInfo.currentHitpoints -= damage;
        if (shipBase == charStateMng)
            charStateMng.CharacterFeatureInfo.hitEffectPlayer.PlayHitEffect();
    }

    public override void TakeDamage(ProjectileStateMng projectile)
    {
        if (projectile.WeaponStateMng.getShipBase == charStateMng)
            charStateMng.CharacterFeatureInfo.hitEffectPlayer.PlayHitEffect();
        if (CurrentInfo.currentHitpoints <= 1f)
        {
            TakeECM(projectile);
            return;
        }
        CurrentInfo.currentHitpoints -= projectile.CurrentInfo.damage;
    }

    public override void TakeECM(ProjectileStateMng projectile)
    {
        if(CurrentInfo.currentECCM_Capacity > 0)
            CurrentInfo.currentECCM_Capacity -= projectile.WeaponStateMng.CurrentInfo.ECMvalue;
    }

    private void rotateTurret()
    {
        var target = shipBase.Info.TargetingModuleStateMng.currentTarget.TargetableTransform.position;
        var pos = Info.WeaponTransform.InverseTransformPoint(target);
        var turretRot = turret.localRotation;
        var eulerAngle = turretRot.eulerAngles;
        eulerAngle.y = Mathf.Atan2(pos.x, pos.z) * Mathf.Rad2Deg;
        eulerAngle.x = 0f;
        eulerAngle.z = 0f;
        IsAttackable = eulerAngle.y <= maxModuleRotationLimit && eulerAngle.y >= minModuleRotationLimit;

        turretRot.eulerAngles = eulerAngle;
        //isRotatable = eulerAngle.y >= minModuleRotationLimit && eulerAngle.y <= maxModuleRotationLimit;
        turret.localRotation = Quaternion.Slerp(turret.localRotation, turretRot, 10f * Time.deltaTime);
    }

    private void rotateGun()
    {
        var target = shipBase.Info.TargetingModuleStateMng.currentTarget.TargetableTransform.position;
        var pos = target - gun.position;
        var wantedRot = Quaternion.LookRotation(pos, Info.WeaponTransform.up);
        //if (isRotatable)
        //{
        gun.rotation = Quaternion.Slerp(gun.rotation, wantedRot, 10f * Time.deltaTime);
        var eulerAngle = gun.localRotation.eulerAngles;
        var clampX = eulerAngle.x;
        if (clampX >= 270f)
            clampX = (360f - clampX) * -1f;
        IsAttackable = IsAttackable && clampX <= 2f;
        clampX = Mathf.Clamp(clampX, -90f, 2f);
        eulerAngle.x = clampX;
        var localRot = gun.localRotation;
        localRot.eulerAngles = eulerAngle;
        gun.localRotation = Quaternion.Slerp(gun.localRotation, localRot, 10f * Time.deltaTime);
        //}
    }


    private void OnDestroy()
    {

    }
}
