﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFrigateWeaponIdleState : State<EnemyFrigateWeaponStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFrigateWeaponStateMng.ENEMY_FRIGATE_WEAPON_STATES.idle; }
    }

    public override void Enter(params object[] o_Params)
    {
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
