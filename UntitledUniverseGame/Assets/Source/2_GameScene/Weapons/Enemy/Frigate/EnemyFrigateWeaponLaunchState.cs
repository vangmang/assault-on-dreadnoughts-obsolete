﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;

public class EnemyFrigateWeaponLaunchState : State<EnemyFrigateWeaponStateMng>
{
    public override Enum GetState
    {
        get { return EnemyFrigateWeaponStateMng.ENEMY_FRIGATE_WEAPON_STATES.launch; }
    }

    public override void Enter(params object[] o_Params)
    {
        instance.Info.LaunchAudio.Play();
        instance.enemyFrigateStateMng.CurrentInfo.cur_power -= instance.enemyFrigateStateMng.ConsumableStat.AttackConsumption;
        ++instance.CurrentInfo.currentChargeCount;
        instance.CurrentInfo.currentChargeCount %= instance.Info.MaxChargeCount;
        var projectile = instance.Info.ProjectileStateMngArr[instance.CurrentInfo.currentChargeCount];
        projectile.gameObject.SetActive(true);
        try
        {
            projectile.StateTransition(ProjectileStateMng.PROJECTILE_STATES.fired);
        }
        catch { }
        instance.enemyFrigateStateMng.ResetPowerRecovery();
        instance.StateTransition(EnemyFrigateWeaponStateMng.ENEMY_FRIGATE_WEAPON_STATES.idle);
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}
