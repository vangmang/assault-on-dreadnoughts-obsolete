﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UniRx;

public class EnemyFrigateWeaponStateMng : WeaponBase
{
    public enum ENEMY_FRIGATE_WEAPON_STATES
    {
        idle,
        launch
    }

    public EnemyFrigateStateMng enemyFrigateStateMng;
    private float time;

    private void Awake()
    {
        TargetingModuleStateMng = enemyFrigateStateMng.Info.TargetingModuleStateMng;
        InitializeState();
    }


    // Start is called before the first frame update
    void Start()
    {
        StateEnter(ENEMY_FRIGATE_WEAPON_STATES.idle);
    }

    // Update is called once per frame
    void Update()
    {
        Execute();
        if (enemyFrigateStateMng.Info.TargetingModuleStateMng.currentTarget != null)
        {
            time += Time.deltaTime;
            if (time >= Stat.fireRate)
            {
                StateTransition(ENEMY_FRIGATE_WEAPON_STATES.launch);
                time = 0f;
            }
        }
        else
            time = 0f;
        Vector3 outVec;
        PredictTarget(out outVec);
    }

    private void OnDestroy()
    {
        
    }
}
