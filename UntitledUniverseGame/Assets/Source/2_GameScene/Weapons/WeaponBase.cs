﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using System;
using UnityEngine.UI;

public abstract class WeaponBase : StateMng
{
    public enum ARMAMENT
    {
        _155mm, // 주로 소형 함선에 장착
        _300mm, // 주로 중형 함선에 장착
        _1200mm, // 주로 드레드넛에 장착
    }

    public enum WEAPON_STYLE
    {
        singleShot, // 단발
        burst,      // 점사
        blaze,      // 연발
        max //
    }

    /// <summary>
    /// 함선에서 설정된 스탯을 따른다.
    /// </summary>
    [Serializable]
    public struct _STAT_
    {
        public float velocity;
        public float fireRate;
        public float damage;
    }

    [Serializable]
    public struct _CURRENT_INFO_
    {
        public DateTimeOffset lastFired;
        public bool isFired;
        public float ECMvalue; // 스탯에 선언하지 않고 현재 정보에 선언한 이유는 ECM은 hacking 속성 공격에 의해 활성화 되기 때문에 해킹 활성시 현재 ECM수치에 값을 대입하도록 함
        public int currentChargeCount;
    }
    [Serializable]
    public struct _INFO_
    {
        [SerializeField] private Transform weaponTransform;
        [SerializeField] private AudioSource launchAudio;
        [SerializeField] private ProjectileStateMng[] projectileStateMngArr;
        //[SerializeField] private ProjectileStateMng projectilePrefab;
        [SerializeField] private ParticleSystem launchParticle;
        public Transform WeaponTransform => weaponTransform; 
        public AudioSource LaunchAudio => launchAudio; 
        public ProjectileStateMng[] ProjectileStateMngArr { get { return projectileStateMngArr; } }
        //public ProjectileStateMng ProjectilePrefab => projectilePrefab;
        public ParticleSystem LaunchParticle => launchParticle;
        public int MaxChargeCount => projectileStateMngArr.Length;
    }

    public _STAT_ Stat;
    public _CURRENT_INFO_ CurrentInfo;
    public _INFO_ Info;
    public ARMAMENT armament;
    public WEAPON_STYLE weaponStyle = WEAPON_STYLE.singleShot;

    public TargetingModuleStateMng TargetingModuleStateMng { get; protected set; }
    [SerializeField] protected ShipBase shipBase;
    public ShipBase getShipBase { get { return shipBase; } }

    public void PredictTarget(out Vector3 predictedTargetPoint)
    {
        var wantedRot = shipBase.Info.ShipTransform.rotation;
        predictedTargetPoint = Vector3.zero;
        if (TargetingModuleStateMng.currentTarget != null)
        {
            TargetingModuleStateMng.ITargetable target = TargetingModuleStateMng.currentTarget;
            var shipRot = shipBase.Info.ShipTransform.rotation;
            var limitRot = shipRot.eulerAngles - wantedRot.eulerAngles;

            var targetRot = target.TargetableTransform.position - Info.WeaponTransform.position;
            if (TargetingModuleStateMng.isOptimalZoneAvailable)
            {
                var velocity = target.CurrentVelocity;
                float v1 = velocity; // 타겟의 현재 속도
                float v2 = Stat.velocity;                // 프로젝타일 속도
                float d1 = (shipBase.Info.ShipTransform.position - target.TargetableTransform.position).magnitude;           // 타겟의 거리
                float t = d1 / v2;
                float d2 = t * v1;
                targetRot += target.TargetableTransform.forward * d2;
                predictedTargetPoint =
                    target.TargetableTransform.position +
                    target.TargetableTransform.forward * d2;
            }
            wantedRot = Quaternion.LookRotation(targetRot, Info.WeaponTransform.up);

            if (Mathf.Abs(limitRot.x) <= shipBase.WeaponLimitElevation &&
                Mathf.Abs(limitRot.y) <= shipBase.WeaponLimitElevation)
                Info.WeaponTransform.rotation = wantedRot;
        }
        else
            Info.WeaponTransform.rotation = wantedRot;
    }
}

public abstract class DreadnoughtWeaponBase : StateMng, TargetingModuleStateMng.ITargetable
{
    [Serializable]
    public struct _STAT_
    {
        public float maxVelocity;
        public float maxFireRate;
        public float damage;
        public float hitpoints;
        public float maxECCM_Capacity;
    }

    [Serializable]
    public struct _CURRENT_INFO_
    {
        public DateTimeOffset lastFired;
        public bool isFired;
        public float currentHitpoints;
        public float currentECCM_Capacity;
        public int currentChargeCount;
    }

    [Serializable]
    public struct _INFO_
    {
        [SerializeField] private Transform weaponTransform;
        [SerializeField] private AudioSource launchAudio;
        [SerializeField] private Collider weaponCollider;
        //[SerializeField] private DreadnoughtProjectileBase dreadnoughtProjectilePrefab;
        [SerializeField] private DreadnoughtProjectileBase[] dreadnoughtProjectiles;
        public Transform WeaponTransform => weaponTransform; 
        public AudioSource LaunchAudio => launchAudio; 
        public Collider WeaponCollider => weaponCollider;
        public DreadnoughtProjectileBase[] DreadnoughtProjectiles => dreadnoughtProjectiles;
        public int MaxChargeCount => dreadnoughtProjectiles.Length;
        //public DreadnoughtProjectileBase DreadnoughtProjectilePrefab => dreadnoughtProjectilePrefab;
    }

    [SerializeField] private ShipBase.IDENTIFY identify;
    public ShipBase.IDENTIFY Identify { get => identify; set { identify = value; } }
    public _STAT_ Stat;
    public _CURRENT_INFO_ CurrentInfo;
    public _INFO_ Info;

    public TargetingModuleStateMng TargetingModuleStateMng { get; protected set; }
    [SerializeField] protected ShipBase shipBase;

    [SerializeField] protected Image enemyIndicatorImage;
    [SerializeField] protected Text enemyLockOnStateText;
    [SerializeField] protected Text enemyDistanceText;
    [SerializeField] protected Transform indicator;
    public Image IndicatorImage { get { return enemyIndicatorImage; } }
    public Text LockStateText { get { return enemyLockOnStateText; } }
    public Text DistanceText { get { return enemyDistanceText; } }
    public Transform IndicatorTransform { get { return indicator; } }
    public Transform[] ParallelForTransform { get; protected set; }
    public Transform[] LockStateTextTransform { get; protected set; }
    public Transform[] DistanceTextTransform { get; protected set; }

    public ShipBase getShipBase { get { return shipBase; } }

    public Transform TargetableTransform { get { return Info.WeaponTransform; } }
    public float getCurrentHitpoints { get { return CurrentInfo.currentECCM_Capacity > 0 ? Mathf.Max(CurrentInfo.currentHitpoints, 1f) : 0f; } }
    public bool IsCapturable { get => !shipBase.CurrentInfo.isWarpin; }
    public TargetingModuleStateMng.TARGET_TYPE TargetType { get => TargetingModuleStateMng.TARGET_TYPE.module; }
    public float CurrentVelocity => shipBase.CurrentInfo.cur_velocity;

    protected void InitInstantiation()
    {
        Indicator indicator = Instantiate(this.indicator, EnemyManager.Instance.EnemyIndicatorParent.transform).GetComponent<Indicator>();
        enemyIndicatorImage = indicator.normalImage;
        enemyLockOnStateText = indicator.textList[0];
        enemyDistanceText = indicator.textList[1];
        indicator.mainCamera = GameMng.Instance.MainCamera;
        indicator.target = Info.WeaponTransform;
        this.indicator = indicator.transform;
    }
    public virtual void TakeDamage(object[] o_Params) { }
    public virtual void TakeDamage(ProjectileStateMng projectile) { }
    public virtual void TakeECM(ProjectileStateMng projectile) { }
}
