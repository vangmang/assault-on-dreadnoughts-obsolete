﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class AutoSelfDestruction : MonoBehaviour
{
    private enum OBJECT_TYPE
    {
        audio,
        particle
    }
    public GameObject target;
    [SerializeField] private OBJECT_TYPE objectType;
    private void Start()
    {
        // 부모가 바뀌면 폭8
        this.ObserveEveryValueChanged(_ => transform.parent).
            Where(p => p == null).
            Subscribe(_ =>
            {
                EnableDestruction();
            });
    }

    public void EnableDestruction()
    {
        IEnumerator invoke = null;
        switch (objectType)
        {
            case OBJECT_TYPE.audio:
                invoke = destructAudio(target.GetComponent<AudioSource>()); break;
            case OBJECT_TYPE.particle:
                invoke = destructParticle(target.GetComponent<ParticleSystem>()); break;
            default: break;
        }
        try {
            StartCoroutine(invoke);
        }
        catch (System.NullReferenceException) { }
    }

    private IEnumerator destructAudio(AudioSource audio)
    {
        yield return new WaitForSeconds(audio.clip.length);
        try
        {
            Destroy(audio.gameObject);
        }
        catch { }
    }

    private IEnumerator destructParticle(ParticleSystem particle)
    {
        yield return new WaitForSeconds(particle.main.duration);
        try
        {
            Destroy(particle.gameObject);
        }
        catch { }
    }


    private void OnDestroy()
    {
        
    }
}
