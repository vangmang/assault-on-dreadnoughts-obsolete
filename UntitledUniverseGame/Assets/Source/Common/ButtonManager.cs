﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class ButtonManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField] protected Image idleImage; // 투명하다.
    [SerializeField] protected Text btnText;
    [SerializeField] protected ButtonSetting buttonSetting;

    private void Start()
    {
        Init();
    }
    protected virtual void PlayEnterButtonSound()
    {
        buttonSetting.enterAudio.Play();
    }
    protected virtual void PlayPressButtonSound()
    {
        buttonSetting.pressedAudio.Play();
    }

    protected virtual void Init() { }
    public abstract void OnPointerDown(PointerEventData e);
    public abstract void OnPointerUp(PointerEventData e);
    public abstract void OnPointerEnter(PointerEventData e);
    public abstract void OnPointerExit(PointerEventData e);
    public virtual void OnPointerClick(PointerEventData e) { }
    protected virtual void ChangeColor(Color color)
    {
        btnText.color = color;
    }
}

public abstract class MainMenuButtonManager : ButtonManager
{
    [SerializeField] protected UIManager UI_Manager;
}

public abstract class GameSceneButtonManager : ButtonManager
{
    [SerializeField] protected GameMng gameMng;

    protected void invokeBack()
    {
        var temp = gameMng.PauseInfo.currentWindow;
        gameMng.PauseInfo.currentWindow.gameObject.SetActive(false);
        gameMng.PauseInfo.currentWindow = gameMng.PauseInfo.recentWindows.Pop();

        gameMng.PauseInfo.currentWindow.gameObject.SetActive(true);
        if (gameMng.PauseInfo.currentWindow.Equals(gameMng.PauseInfo.pauseWindow))
            gameMng.gameStateMng.Escape = gameMng.gameStateMng.escapePause;
        else
            gameMng.gameStateMng.Escape = invokeBack;
    }

}
