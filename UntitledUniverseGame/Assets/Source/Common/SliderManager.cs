﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;

public abstract class SliderManager : MonoBehaviour
{
    [SerializeField] protected Slider slider;

    public float getSliderValue { get { return slider.value; } }

    private void Start()
    {
        this.ObserveEveryValueChanged(_ => slider.value).
            Subscribe(_ =>
            {
                SetValue(slider.value);
            });
    }

    protected abstract void SetValue(float value);
}
