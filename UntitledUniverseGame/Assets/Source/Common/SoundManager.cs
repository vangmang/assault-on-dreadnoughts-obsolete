﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using System.Linq;

public sealed class SoundManager : MonoBehaviour
{
    private static SoundManager instance = null;
    public static SoundManager Instance => instance;
    public readonly List<SoundMngAssistant> AudioAssistants = new List<SoundMngAssistant>();

    public GameMng gameMng;
    [Serializable]
    public struct _AUDIOS_
    {
        public SoundMngAssistant backgroundAudio;
    }
    public _AUDIOS_ Audios;

    private void Awake()
    {
        instance = this;
    }

    private IEnumerator Start()
    {
        this.ObserveEveryValueChanged(_ => gameMng.PauseInfo.AudioSetting.backgroundVolume.value).
            Subscribe(x =>
            {
                var volume = x * gameMng.PauseInfo.AudioSetting.masterVolume.value;
                Audios.backgroundAudio.Audio.volume = Audios.backgroundAudio.isPaused ? volume * 0.5f : volume;
            });
        this.ObserveEveryValueChanged(_ => gameMng.PauseInfo.AudioSetting.masterVolume.value).
            Subscribe(x =>
            {
                var volume = gameMng.PauseInfo.AudioSetting.backgroundVolume.value * x;
                Audios.backgroundAudio.Audio.volume = Audios.backgroundAudio.isPaused ? volume * 0.5f : volume;
            });
        yield return new WaitForFixedUpdate();
        this.ObserveEveryValueChanged(_ => AudioAssistants.Count).
            Subscribe(_ =>
            {
                //Debug.Log(AudioAssistants.Count);
            });
        //Audios.ForEach(audio => Debug.Log());
    }

    public void RemoveAudio(SoundMngAssistant assistant)
    {
        if (AudioAssistants.Contains(assistant))
            AudioAssistants.Remove(assistant);
    }

    /// <summary>
    /// 게임 창에서 사용되는 오디오만 pause
    /// </summary>
    public void PauseAudios()
    {
        foreach (var assistant in AudioAssistants.Where(assistant => assistant.windowType == SoundMngAssistant.WINDOW_TYPE.game))
            assistant.Pause();
    }

    /// <summary>
    /// 게임 창에서 사용되는 오디오만 resume
    /// </summary>
    public void ResumeAudios()
    {
        foreach (var assistant in AudioAssistants.Where(assistant => assistant.windowType == SoundMngAssistant.WINDOW_TYPE.game))
            assistant.Resume();
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
