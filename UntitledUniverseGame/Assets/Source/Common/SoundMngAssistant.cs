﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class SoundMngAssistant : MonoBehaviour
{
    public enum WINDOW_TYPE
    {
        game,
        pause
    }

    [SerializeField] private new AudioSource audio;
    public AudioSource Audio => audio;
    public WINDOW_TYPE windowType;
    public bool isBackgroundMusic;
    public bool isPaused;
    private bool audioEnable = true;

    private void Start()
    {
        SoundManager.Instance.AudioAssistants.Add(this);
        this.ObserveEveryValueChanged(_ => audioEnable).
            Subscribe(_ =>
            {
                gameObject.SetActive(audioEnable);
            });
    }

    public void updateAudioEnable(float distance)
    {
        audioEnable = distance <= Audio.maxDistance;
    }

    public void Pause()
    {
        if (isBackgroundMusic)
        {
            if(!isPaused)
                audio.volume *= 0.5f;
        }
        else
        {
            try
            {
                audio.Pause();
            }
            catch { }
        }
        isPaused = true;
    }
    public void Resume()
    {
        if (isBackgroundMusic)
        {
            audio.volume *= 2f;
        }
        else
        {
            try
            {
                audio.UnPause();
            }
            catch { }
        }
        isPaused = false;
    }

    public void SetVolume(float volume)
    {
        audio.volume = volume;
    }
}
